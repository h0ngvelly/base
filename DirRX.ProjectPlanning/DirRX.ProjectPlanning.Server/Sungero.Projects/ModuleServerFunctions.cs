﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Company;

namespace DirRX.ProjectPlanning.Module.Projects.Server
{
  partial class ModuleFunctions
  {
    /// <summary>
    /// Получить подчиненных сотрудников для текущего сотрудника.
    /// </summary>
    /// <returns>Ид подчиненных сотрудников.</returns>
    public List<int> GetSubordinateEmployees()
    {
      var employeeIds = new List<int>();
      var currentEmployee = Sungero.Company.Employees.Current;
      var currentRecipientsIds = Sungero.Docflow.PublicFunctions.Module.GetCurrentRecipients(true);
      
      if(currentEmployee != null && currentRecipientsIds.Any())
      {
        var allBusinessUnits = Sungero.Company.BusinessUnits.GetAll();
        var allDepartments = Sungero.Company.Departments.GetAll();
        var allEmployees = Sungero.Company.Employees.GetAll();
        
        var businessUnits = allBusinessUnits.Where(x => x.CEO != null && currentRecipientsIds.Contains(x.CEO.Id));
        
        var subManagerIds = allDepartments.Where(x => x.Manager != null
                                                      && businessUnits.Contains(x.BusinessUnit)
                                                )
                                          .Select(x => x.Manager.Id);
        
        employeeIds.AddRange(subManagerIds);
        
        var subCeoIds = allBusinessUnits.Where(b => b.HeadCompany != null
                                                    && businessUnits.Contains(b.HeadCompany)
                                                    && b.CEO != null
                                                   )
                                        .Select(b => b.CEO.Id);
        
        employeeIds.AddRange(subCeoIds);
        
        var departments = allDepartments.Where(x => currentRecipientsIds.Contains(x.Manager.Id));
        var subEmployeeIds = allEmployees.Where(x => departments.Contains(x.Department)).Select(x => x.Id);
        
        employeeIds.AddRange(subEmployeeIds);
        
        //исключаем текущего сотрудника
        employeeIds = employeeIds.Where(x => x != currentEmployee.Id).ToList();
      }
          
      return employeeIds;
    }
    
  }
}