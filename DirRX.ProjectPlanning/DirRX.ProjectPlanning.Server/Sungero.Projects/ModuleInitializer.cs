﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace DirRX.ProjectPlanning.Module.Projects.Server
{
	public partial class ModuleInitializer
	{

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      base.Initializing(e);
      GrantRightsOnProjectFolders();
    }
    
    /// <summary>
    /// Выдача прав на папки проекта.
    /// </summary>
    public void GrantRightsOnProjectFolders()
    {
      var allUsers = Roles.AllUsers;
      DirRX.ProjectPlanning.Module.Projects.SpecialFolders.ProjectsPlansDirRX.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      DirRX.ProjectPlanning.Module.Projects.SpecialFolders.ProjectsPlansDirRX.AccessRights.Save();
      
      DirRX.ProjectPlanning.Module.Projects.SpecialFolders.ProjectTasksSungero.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      DirRX.ProjectPlanning.Module.Projects.SpecialFolders.ProjectTasksSungero.AccessRights.Save();
    }
  }
}
