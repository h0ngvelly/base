﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sungero.Core;
using Sungero.CoreEntities;

namespace DirRX.ProjectPlanning.Module.Projects.Server
{
  partial class ProjectTasksSungeroFolderHandlers
  {

    public virtual IQueryable<DirRX.ProjectPlanner.IProjectActivityTask> ProjectTasksSungeroDataQuery(IQueryable<DirRX.ProjectPlanner.IProjectActivityTask> query)
    {
      
      if(_filter == null)
        return query;
      
      var tasks = query;
      var currentEmployee = Sungero.Company.Employees.Current;
      IQueryable<DirRX.ProjectPlanner.IAssignment> assignments = null;
      
      if(_filter.ProjectSungero != null)
      {
        tasks = tasks.Where(x => x.ProjectPlan == _filter.ProjectSungero.ProjectPlanDirRX);
      }

      #region Исполнитель
      if (currentEmployee == null && (_filter.MeSungero || _filter.MySubordinatesSungero))
      {
        // HACK Balezin_AA: Платформа не дает вернуть пустой IQueryable, поэтому возвращаю
        // набор пустых данных таким способом
        return query.Where(x => false);
      }

      if(_filter.MeSungero)
      {
        assignments = DirRX.ProjectPlanner.Assignments.GetAll(x => x.Performer.Id == currentEmployee.Id);
      }
      else if(_filter.MySubordinatesSungero)
      {
        var subordinates = Functions.Module.GetSubordinateEmployees();
        assignments = DirRX.ProjectPlanner.Assignments.GetAll(x => subordinates.Contains(x.Performer.Id));
      }
      else if(_filter.SelectAnotherSungero)
      {
        assignments = DirRX.ProjectPlanner.Assignments.GetAll(x => x.Performer == _filter.EmployeeSungero);
      }

      if(assignments != null)
      {
        tasks = tasks.Where(x => assignments.Select(a => a.Task.Id).Contains(x.Id));
      }
      #endregion
      
      #region Состояние
      if(_filter.InWorkSungero)
      {
        tasks = tasks.Where(x => x.Status == DirRX.ProjectPlanner.ProjectActivityTask.Status.InProcess);
      }
      else if(_filter.ExpiredSungero)
      {
        //фильтрация по статусу
        tasks = tasks.Where(x => x.Status == DirRX.ProjectPlanner.ProjectActivityTask.Status.InProcess ||
                            x.Status == DirRX.ProjectPlanner.ProjectActivityTask.Status.UnderReview);
        //фильтрация по дате
        tasks = tasks.Where(x => x.MaxDeadline.HasValue && (x.MaxDeadline.Value.HasTime() ? x.MaxDeadline < Calendar.Now : x.MaxDeadline < Calendar.UserToday));
      }
      #endregion
      
      #region Период
      if(_filter.AllSungero)
      {
        if(_filter.Days30Sungero)
        {
          tasks = tasks.Where(x => x.Started >= Calendar.Today.AddDays(-30));
        }
        else if(_filter.Days90Sungero)
        {
          tasks = tasks.Where(x => x.Started >= Calendar.Today.AddDays(-90));
        }
        else if(_filter.Days180Sungero)
        {
          tasks = tasks.Where(x => x.Started >= Calendar.Today.AddDays(-180));
        }
      }
      #endregion
      
      return tasks;
    }
  }

	partial class ProjectsPlansDirRXFolderHandlers
	{

		public virtual IQueryable<DirRX.ProjectPlanner.IProjectPlanRX> ProjectsPlansDirRXDataQuery(IQueryable<DirRX.ProjectPlanner.IProjectPlanRX> query)
		{
			if (_filter == null)
				return query;
			
			// Фильтр по состоянию.
			if (_filter.Active || _filter.Closed || _filter.Closing || _filter.Initiation)
				query = query.Where(x => (_filter.Active && x.Stage == DirRX.ProjectPlanner.ProjectPlanRX.Stage.Execution) ||
				                    (_filter.Closed && x.Stage == DirRX.ProjectPlanner.ProjectPlanRX.Stage.Completed) ||
				                    (_filter.Closing && x.Stage == DirRX.ProjectPlanner.ProjectPlanRX.Stage.Completion) ||
				                    (_filter.Initiation && x.Stage == DirRX.ProjectPlanner.ProjectPlanRX.Stage.Initiation));

			var today = Calendar.UserToday;
			
			// Фильтр по дате начала проекта.
			var startDateBeginPeriod = _filter.StartDateRangeFrom ?? Calendar.SqlMinValue;
			var startDateEndPeriod = _filter.StartDateRangeTo ?? Calendar.SqlMaxValue;
			
			if (_filter.StartPeriodThisMonth)
			{
				startDateBeginPeriod = today.BeginningOfMonth();
				startDateEndPeriod = today.EndOfMonth();
			}
			
			if (_filter.StartPeriodThisMonth || (_filter.StartDateRangeFrom != null || _filter.StartDateRangeTo != null))
				query = query.Where(x => (x.StartDate.Between(startDateBeginPeriod, startDateEndPeriod) && !Equals(x.Stage, DirRX.ProjectPlanner.ProjectPlanRX.Stage.Completed)) ||
				                    (x.ActualStartDate.Between(startDateBeginPeriod, startDateEndPeriod) && Equals(x.Stage, DirRX.ProjectPlanner.ProjectPlanRX.Stage.Completed)));

			// Фильтр по дате окончания проекта.
			var finishDateBeginPeriod = _filter.FinishDateRangeFrom ?? Calendar.SqlMinValue;
			var finishDateEndPeriod = _filter.FinishDateRangeTo ?? Calendar.SqlMaxValue;
			
			if (_filter.FinishPeriodThisMonth)
			{
				finishDateBeginPeriod = today.BeginningOfMonth();
				finishDateEndPeriod = today.EndOfMonth();
			}
			
			if (_filter.FinishPeriodThisMonth || (_filter.FinishDateRangeFrom != null || _filter.FinishDateRangeTo != null))
				query = query.Where(x => (x.EndDate.Between(finishDateBeginPeriod, finishDateEndPeriod) && !Equals(x.Stage, DirRX.ProjectPlanner.ProjectPlanRX.Stage.Completed)) ||
				                    (x.ActualFinishDate.Between(finishDateBeginPeriod, finishDateEndPeriod) && Equals(x.Stage, DirRX.ProjectPlanner.ProjectPlanRX.Stage.Completed)));
			
			return query;
		}
	}

	partial class ProjectsHandlers
	{
	}
}