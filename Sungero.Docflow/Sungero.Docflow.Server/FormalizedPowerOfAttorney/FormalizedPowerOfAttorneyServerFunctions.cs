﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Docflow.FormalizedPowerOfAttorney;
using Sungero.FormalizeDocumentsParser.PowerOfAttorney.Model.PoAV2;
using Sungero.Parties;
using PoAServiceErrors = Sungero.PowerOfAttorneyCore.PublicConstants.Module.PowerOfAttorneyServiceErrors;
using XmlElementNames = Sungero.Docflow.Constants.FormalizedPowerOfAttorney.XmlElementNames;
using XmlFPoAInfoAttributeNames = Sungero.Docflow.Constants.FormalizedPowerOfAttorney.XmlFPoAInfoAttributeNames;
using XmlIssuedToAttributeNames = Sungero.Docflow.Constants.FormalizedPowerOfAttorney.XmlIssuedToAttributeNames;

namespace Sungero.Docflow.Server
{
  partial class FormalizedPowerOfAttorneyFunctions
  {
    
    #region Заявление на отзыв МЧД

    /// <summary>
    /// Получить заявление на отзыв эл. доверенности без учета прав доступа.
    /// </summary>
    /// <returns>Заявление на отзыв эл. доверенности.</returns>
    [Public, Remote]
    public virtual Sungero.Docflow.IPowerOfAttorneyRevocation GetRevocation()
    {
      IPowerOfAttorneyRevocation revocation = null;
      AccessRights.AllowRead(() =>
                             {
                               revocation = PowerOfAttorneyRevocations.GetAll().FirstOrDefault(r => Equals(r.FormalizedPowerOfAttorney, _obj));
                             });
      return revocation;
    }
    
    /// <summary>
    /// Создать заявление на отзыв эл. доверенности.
    /// </summary>
    /// <param name="reason">Причина отзыва доверенности.</param>
    /// <returns>Заявление на отзыв эл. доверенности.</returns>
    [Public, Remote]
    public virtual Sungero.Docflow.IPowerOfAttorneyRevocation CreateRevocation(string reason)
    {
      var revocation = PowerOfAttorneyRevocations.Create();
      Functions.PowerOfAttorneyRevocation.FillRevocationProperties(revocation, _obj, reason);

      if (!Functions.PowerOfAttorneyRevocation.GenerateRevocationBody(revocation))
        return null;
      
      revocation.Relations.Add(Docflow.PublicConstants.Module.SimpleRelationName, _obj);
      revocation.Save();
      Functions.PowerOfAttorneyRevocation.GenerateRevocationPdf(revocation);
      return revocation;
    }
    
    #endregion
    
    #region Генерация МЧД
    
    /// <summary>
    /// Сформировать тело эл. доверенности.
    /// </summary>
    /// <returns>True - если генерация завершилась успешно.</returns>
    [Public, Remote]
    public virtual bool GenerateFormalizedPowerOfAttorneyBody()
    {
      var unifiedRegistrationNumber = Guid.NewGuid();
      var xml = this.CreateFormalizedPowerOfAttorneyXml(unifiedRegistrationNumber);
      var isValidXml = this.ValidateGeneratedFormalizedPowerOfAttorneyXml(xml);
      
      if (isValidXml)
      {
        _obj.UnifiedRegistrationNumber = unifiedRegistrationNumber.ToString();
        this.AddNewVersionIfLastVersionApproved();
        Functions.OfficialDocument.WriteBytesToDocumentLastVersionBody(_obj, xml, Constants.FormalizedPowerOfAttorney.XmlExtension);
        _obj.LifeCycleState = Docflow.FormalizedPowerOfAttorney.LifeCycleState.Draft;
        _obj.FtsListState = null;
        _obj.FtsRejectReason = string.Empty;
        _obj.Save();
      }
      else
      {
        Logger.DebugFormat("Generate formalized power of attorney body validation error. Document id: {0}", _obj.Id);
        return false;
      }
      
      return true;
    }
    
    /// <summary>
    /// Создать тело эл. доверенности.
    /// </summary>
    /// <param name="unifiedRegistrationNumber">Единый регистрационный номер доверенности.</param>
    /// <returns>Тело эл. доверенности.</returns>
    public virtual Docflow.Structures.Module.IByteArray CreateFormalizedPowerOfAttorneyXml(Guid unifiedRegistrationNumber)
    {
      return this.CreateFormalizedPowerOfAttorneyXmlV2(unifiedRegistrationNumber);
    }
    
    /// <summary>
    /// Создать тело эл. доверенности версии 002.
    /// </summary>
    /// <param name="unifiedRegistrationNumber">Единый регистрационный номер доверенности.</param>
    /// <returns>Тело эл. доверенности версии 002.</returns>
    public virtual Docflow.Structures.Module.IByteArray CreateFormalizedPowerOfAttorneyXmlV2(Guid unifiedRegistrationNumber)
    {
      var poa = FormalizeDocumentsParser.PowerOfAttorney.PowerOfAttorneyBuilder.CreatePowerOfAttorney();
      poa.FileId = this.GetFileIdAttribute(unifiedRegistrationNumber);
      
      var generalInfo = this.GetGeneralInfo(unifiedRegistrationNumber);
      poa.GeneralInfo = generalInfo;
      
      var legalEntityPrincipal = this.GetLegalEntityPrincipal();
      poa.Principal = legalEntityPrincipal;
      
      if (_obj.AgentType == Docflow.PowerOfAttorneyBase.AgentType.Employee)
      {
        var individualRepresentative = this.GetIndividualAgent(_obj.IssuedTo.Person);
        poa.Agent = individualRepresentative;
      }
      if (_obj.AgentType == Docflow.PowerOfAttorneyBase.AgentType.Person)
      {
        var individualRepresentative = this.GetIndividualAgent(Sungero.Parties.People.As(_obj.IssuedToParty));
        poa.Agent = individualRepresentative;
      }
      else if (_obj.AgentType == Docflow.PowerOfAttorneyBase.AgentType.LegalEntity)
      {
        var legalEntityRepresentative = this.GetLegalEntityAgent();
        poa.Agent = legalEntityRepresentative;
      }
      else if (_obj.AgentType == Docflow.PowerOfAttorneyBase.AgentType.Entrepreneur)
      {
        var entrepreneurRepresentative = this.GetEntrepreneurAgent();
        poa.Agent = entrepreneurRepresentative;
      }
      
      var permissions = this.GetAgentPowers();
      poa.PermissionsInfos = permissions;
      
      var xml = Sungero.FormalizeDocumentsParser.Extension.GetPowerOfAttorneyXml(poa);
      return Docflow.Structures.Module.ByteArray.Create(xml);
    }
    
    /// <summary>
    /// Получить значение атрибута "ИдФайл".
    /// </summary>
    /// <param name="unifiedRegistrationNumber">Единый регистрационный номер доверенности.</param>
    /// <returns>Значение атрибута "ИдФайл".</returns>
    public virtual string GetFileIdAttribute(Guid unifiedRegistrationNumber)
    {
      return string.Format("{0}_{1}_{2}",
                           Constants.FormalizedPowerOfAttorney.XmlGeneralInfoAttributeValues.FileIdPrefix,
                           Calendar.UserNow.ToString("yyyyMMdd"),
                           unifiedRegistrationNumber);
    }
    
    /// <summary>
    /// Получить основные сведения доверенности.
    /// </summary>
    /// <param name="unifiedRegistrationNumber">Единый регистрационный номер доверенности.</param>
    /// <returns>Сведения доверенности.</returns>
    private GeneralInfo GetGeneralInfo(Guid unifiedRegistrationNumber)
    {
      var generalInfo = FormalizeDocumentsParser.PowerOfAttorney.PowerOfAttorneyBuilder.CreateGeneralInfo();
      
      generalInfo.Number = unifiedRegistrationNumber.ToString();
      
      if (_obj.ValidFrom.HasValue)
        generalInfo.ValidFrom = _obj.ValidFrom.Value;
      
      if (_obj.ValidTill.HasValue)
        generalInfo.ValidTill = _obj.ValidTill.Value;
      
      generalInfo.Retrust = Constants.FormalizedPowerOfAttorney.XmlGeneralInfoAttributeValues.WithoutRetrust;
      generalInfo.JointPowers = Constants.FormalizedPowerOfAttorney.XmlGeneralInfoAttributeValues.IndividualPowers;
      generalInfo.InternalNumber = _obj.RegistrationNumber;
      
      if (_obj.RegistrationDate.HasValue)
        generalInfo.InternalRegistrationDate = _obj.RegistrationDate.Value;
      
      generalInfo.Revocable = Constants.FormalizedPowerOfAttorney.XmlGeneralInfoAttributeValues.RevokeAvailable;
      generalInfo.SourceSystemInfo = Constants.FormalizedPowerOfAttorney.XmlGeneralInfoAttributeValues.SourceSystemInfo;
      
      return generalInfo;
    }
    
    /// <summary>
    /// Получить доверителя юридическое лицо.
    /// </summary>
    /// <returns>Доверитель юридическое лицо.</returns>
    private LegalEntityPrincipal GetLegalEntityPrincipal()
    {
      var legalEntityPrincipal = FormalizeDocumentsParser.PowerOfAttorney.PowerOfAttorneyBuilder.CreateLegalEntityPrincipal();
      
      // Заполнение элемента "Сведения о российском юридическом лице (СвРосОрг)".
      if (_obj.BusinessUnit != null)
      {
        legalEntityPrincipal.Name = _obj.BusinessUnit.LegalName;
        legalEntityPrincipal.TIN = _obj.BusinessUnit.TIN;
        legalEntityPrincipal.PSRN = _obj.BusinessUnit.PSRN;
        legalEntityPrincipal.TRRC = _obj.BusinessUnit.TRRC;
        legalEntityPrincipal.Address = _obj.BusinessUnit.LegalAddress;
      }
      
      // Заполнение элемента "Сведения о лице, действующем от имени юридического лица без доверенности (ЛицоБезДов)".
      if (_obj.BusinessUnit.CEO != null && _obj.BusinessUnit.CEO.Person != null)
      {
        legalEntityPrincipal.HeadTIN = _obj.BusinessUnit.CEO.Person.TIN;
        legalEntityPrincipal.HeadINILA = Parties.PublicFunctions.Person.GetFormattedInila(_obj.BusinessUnit.CEO.Person);
        
        if (_obj.BusinessUnit.CEO.Person.DateOfBirth.HasValue)
          legalEntityPrincipal.HeadDateOfBirth = _obj.BusinessUnit.CEO.Person.DateOfBirth.Value;
        
        if (_obj.BusinessUnit.CEO.JobTitle != null)
          legalEntityPrincipal.HeadJobTitle = _obj.BusinessUnit.CEO.JobTitle.Name;
      }
      
      if (_obj.OurSigningReason != null)
        legalEntityPrincipal.HeadSigningReason = _obj.OurSigningReason.Name;

      // TODO Сделать признак наличия гражданства для иностранцев.
      legalEntityPrincipal.HeadCitizenshipSign = Constants.FormalizedPowerOfAttorney.XmlIndividualInfoAttributeValues.RussianFederationCitizen;
      
      // Заполнение элемента "Сведения о физическом лице, подписывающем доверенность от имени доверителя (Подписант)".
      if (_obj.OurSignatory != null)
      {
        legalEntityPrincipal.Signer.FirstName = _obj.OurSignatory.Person.FirstName;
        legalEntityPrincipal.Signer.LastName = _obj.OurSignatory.Person.LastName;
        legalEntityPrincipal.Signer.MiddleName = _obj.OurSignatory.Person.MiddleName;
      }
      
      return legalEntityPrincipal;
    }

    private IndividualAgent GetIndividualAgent(Sungero.Parties.IPerson person)
    {
      var agent = FormalizeDocumentsParser.PowerOfAttorney.PowerOfAttorneyBuilder.CreateIndividualAgent();
      agent.TIN = person.TIN;
      agent.INILA = Parties.PublicFunctions.Person.GetFormattedInila(person);
      agent.FullName.FirstName = person.FirstName;
      agent.FullName.LastName = person.LastName;
      agent.FullName.MiddleName = person.MiddleName;
      
      agent.PersonalData.DateOfBirth = person.DateOfBirth.Value;
      agent.PersonalData.CitizenshipFlag = this.GetCitizenshipFlag(person.Citizenship);
      agent.PersonalData.Citizenship = agent.PersonalData.CitizenshipFlag == CitizenshipFlag.Other ? person.Citizenship.Code : null;
      return agent;
    }
    
    private CitizenshipFlag GetCitizenshipFlag(Sungero.Commons.ICountry country)
    {
      if (country == null)
        return CitizenshipFlag.None;
      
      if (country.Code == Constants.FormalizedPowerOfAttorney.RussianFederationCountryCode)
        return CitizenshipFlag.Russia;

      return CitizenshipFlag.Other;
    }
    
    private LegalEntityAgent GetLegalEntityAgent()
    {
      var legalEntityAgent = FormalizeDocumentsParser.PowerOfAttorney.PowerOfAttorneyBuilder.CreateLegalEntityAgent();
      
      if (_obj.IssuedToParty != null)
      {
        legalEntityAgent.OrganizationName = _obj.IssuedToParty.Name;
        legalEntityAgent.OGRN = _obj.IssuedToParty.PSRN;
        legalEntityAgent.KPP = CompanyBases.As(_obj.IssuedToParty).TRRC;
        legalEntityAgent.TIN = _obj.IssuedToParty.TIN;
        
        if (_obj.IssuedToParty.LegalAddress != null && _obj.IssuedToParty.Region?.Code != null)
        {
          legalEntityAgent.Address = _obj.IssuedToParty.LegalAddress;
          legalEntityAgent.Region = _obj.IssuedToParty.Region?.Code;
        }
      }
      
      if (_obj.Representative != null)
      {
        var agentRepresentative = legalEntityAgent.Representative;
        agentRepresentative.FullName.FirstName = _obj.Representative.FirstName;
        agentRepresentative.FullName.MiddleName = _obj.Representative.MiddleName;
        agentRepresentative.FullName.LastName = _obj.Representative.LastName;
        agentRepresentative.TIN = _obj.Representative.TIN;
        agentRepresentative.INILA = Parties.PublicFunctions.Person.GetFormattedInila(_obj.Representative);
        
        agentRepresentative.PersonalData.DateOfBirth = _obj.Representative.DateOfBirth.Value;
        agentRepresentative.PersonalData.Phone = _obj.Representative.Phones;
        agentRepresentative.PersonalData.CitizenshipFlag = this.GetCitizenshipFlag(_obj.Representative.Citizenship);
        agentRepresentative.PersonalData.Citizenship = agentRepresentative.PersonalData.CitizenshipFlag == CitizenshipFlag.Other ? _obj.Representative.Citizenship?.Code : null;
      }
      
      return legalEntityAgent;
    }
    
    private EntrepreneurAgent GetEntrepreneurAgent()
    {
      var agent = FormalizeDocumentsParser.PowerOfAttorney.PowerOfAttorneyBuilder.CreateEntrepreneurAgent();
      
      if (_obj.IssuedToParty != null)
      {
        agent.Name = _obj.IssuedToParty.Name;
        agent.TIN = _obj.IssuedToParty.TIN;
        agent.OGRNIP = _obj.IssuedToParty.PSRN;
      }
      
      if (_obj.Representative != null)
      {
        agent.FullName.FirstName = _obj.Representative.FirstName;
        agent.FullName.MiddleName = _obj.Representative.MiddleName;
        agent.FullName.LastName = _obj.Representative.LastName;
        agent.INILA = Parties.PublicFunctions.Person.GetFormattedInila(_obj.Representative);
        
        agent.PersonalData.DateOfBirth = _obj.Representative.DateOfBirth.Value;
        agent.PersonalData.Phone = _obj.Representative.Phones;
        agent.PersonalData.CitizenshipFlag = this.GetCitizenshipFlag(_obj.Representative.Citizenship);
        agent.PersonalData.Citizenship = agent.PersonalData.CitizenshipFlag == CitizenshipFlag.Other ? _obj.Representative.Citizenship?.Code : null;
      }
      
      return agent;
    }
    
    /// <summary>
    /// Получить полномочия представителя (представителей).
    /// </summary>
    /// <returns>Полномочия представителя (представителей).</returns>
    public virtual string[] GetAgentPowers()
    {
      if (_obj.Powers == null)
        return new string[0];
      
      return _obj.Powers
        .Split('\r', '\n')
        .Select(p => p.Trim())
        .Where(p => p != string.Empty)
        .ToArray();
    }

    /// <summary>
    /// Проверить сформированную xml доверенности.
    /// </summary>
    /// <param name="xml">Тело эл. доверенности.</param>
    /// <returns>True - если проверка xml прошла успешно.</returns>
    [Public]
    public virtual bool ValidateGeneratedFormalizedPowerOfAttorneyXml(Docflow.Structures.Module.IByteArray xml)
    {
      if (xml == null || xml.Bytes == null || xml.Bytes.Length == 0)
        return false;
      
      var validationResult = FormalizeDocumentsParser.Extension.ValidatePowerOfAttorneyXml(xml.Bytes);
      var isValidXml = !validationResult.Any();
      if (!isValidXml)
      {
        Logger.ErrorFormat("ValidateGeneratedFormalizedPowerOfAttorneyXml. Validation error. Document id: {0}", _obj.Id);
        foreach (var vr in validationResult)
          Logger.Error(vr);
      }
      
      return isValidXml;
    }
    
    /// <summary>
    /// Создать новую версию, если последняя утверждена.
    /// </summary>
    [Public]
    public virtual void AddNewVersionIfLastVersionApproved()
    {
      if (!_obj.HasVersions)
        return;
      
      if (_obj.LastVersionApproved == true)
        _obj.Versions.AddNew();
    }
    
    #endregion
    
    #region Импорт МЧД из xml
    
    /// <summary>
    /// Загрузить тело эл. доверенности из XML и импортировать внешнюю подпись.
    /// </summary>
    /// <param name="xml">Структура с XML.</param>
    /// <param name="signature">Структура с подписью.</param>
    [Remote, Public]
    public virtual void ImportFormalizedPowerOfAttorneyFromXmlAndSign(Docflow.Structures.Module.IByteArray xml,
                                                                      Docflow.Structures.Module.IByteArray signature)
    {
      this.ValidateFormalizedPowerOfAttorneyXml(xml);
      
      signature = this.ConvertSignatureFromBase64(signature);
      this.VerifyExternalSignature(xml, signature);
      
      this.FillFormalizedPowerOfAttorney(xml);
      
      Functions.OfficialDocument.WriteBytesToDocumentLastVersionBody(_obj, xml, Constants.FormalizedPowerOfAttorney.XmlExtension);
      
      // Устанавливаем статус до сохранения, чтобы можно было найти дубли эл. доверенности.
      Functions.FormalizedPowerOfAttorney.SetLifeCycleAndFtsListStates(_obj);
      
      // Сохранение необходимо для импорта подписи.
      _obj.Save();
      
      // Сохранение записи об импорте xml-файла в историю.
      var importFromXmlOperationText = Constants.FormalizedPowerOfAttorney.Operation.ImportFromXml;
      var importFromXmlComment = Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.ImportFromXmlHistoryComment;
      _obj.History.Write(new Enumeration(importFromXmlOperationText), null, importFromXmlComment, _obj.LastVersion.Number);
      
      this.ImportSignature(xml, signature);
      this.CheckSignature();
    }
    
    /// <summary>
    /// Декодировать подпись из base64.
    /// </summary>
    /// <param name="signature">Подпись.</param>
    /// <returns>Декодированная подпись.</returns>
    [Public]
    public virtual Docflow.Structures.Module.IByteArray ConvertSignatureFromBase64(Docflow.Structures.Module.IByteArray signature)
    {
      var signatureInfo = ExternalSignatures.GetSignatureInfo(signature.Bytes);
      // Если подпись передали в закодированном виде, попытаться раскодировать.
      if (signatureInfo.SignatureFormat == SignatureFormat.Hash)
      {
        try
        {
          var byteString = System.Text.Encoding.UTF8.GetString(signature.Bytes);
          var signatureBytes = Convert.FromBase64String(byteString);
          signature = Docflow.Structures.Module.ByteArray.Create(signatureBytes);
        }
        catch
        {
          Logger.Error("Import formalized power of attorney. Failed to import signature: cannot decode given signature.");
          throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.SignatureImportFailed);
        }
      }
      
      return signature;
    }
    
    /// <summary>
    /// Проверить подпись на достоверность.
    /// </summary>
    /// <param name="xml">Подписанные данные.</param>
    /// <param name="signature">Подпись.</param>
    [Public]
    public virtual void VerifyExternalSignature(Docflow.Structures.Module.IByteArray xml, Docflow.Structures.Module.IByteArray signature)
    {
      using (var xmlStream = new System.IO.MemoryStream(xml.Bytes))
      {
        var signatureInfo = ExternalSignatures.Verify(signature.Bytes, xmlStream);
        if (signatureInfo.Errors.Any())
        {
          Logger.ErrorFormat("Import formalized power of attorney. Failed to import signature: {0}", string.Join("\n", signatureInfo.Errors.Select(x => x.Message)));
          throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.SignatureImportFailed);
        }
      }
    }
    
    /// <summary>
    /// Заполнить свойства эл. доверенности.
    /// </summary>
    /// <param name="xml">Тело эл. доверенности.</param>
    [Public]
    public virtual void FillFormalizedPowerOfAttorney(Docflow.Structures.Module.IByteArray xml)
    {
      System.Xml.Linq.XDocument xdoc;
      using (var memoryStream = new System.IO.MemoryStream(xml.Bytes))
        xdoc = System.Xml.Linq.XDocument.Load(memoryStream);
      
      var poaInfo = this.TryGetPoAInfoElement(xdoc);
      if (poaInfo == null)
      {
        Logger.Error("Import formalized power of attorney. Failed to parse given XML as formalized power of attorney.");
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.XmlLoadFailed);
      }
      
      this.FillUnifiedRegistrationNumberFromXml(xdoc,
                                                poaInfo,
                                                XmlFPoAInfoAttributeNames.UnifiedRegistrationNumber);
      
      this.FillValidDatesFromXml(xdoc,
                                 poaInfo,
                                 XmlFPoAInfoAttributeNames.ValidFrom,
                                 XmlFPoAInfoAttributeNames.ValidTill);
      
      // Получить регистрационные данные из xml и попытаться пронумеровать документ.
      // Если в xml нет даты регистрации, но есть номер, взять текущую дату в качестве даты регистрации.
      string number = this.GetAttributeValueByName(poaInfo, XmlFPoAInfoAttributeNames.RegistrationNumber);
      DateTime? date = this.GetDateFromXml(poaInfo, XmlFPoAInfoAttributeNames.RegistrationDate) ?? Calendar.Today;
      this.FillRegistrationData(number, date);
      this.FillIssuedToFromXml(xdoc);
      
      this.FillDocumentName(xdoc);
    }
    
    /// <summary>
    /// Получить XML-элемент с информацией об эл. доверенности.
    /// </summary>
    /// <param name="xdoc">XML-документ.</param>
    /// <returns>XML-элемент с информацией о доверенности.</returns>
    [Public]
    public virtual System.Xml.Linq.XElement TryGetPoAInfoElement(System.Xml.Linq.XDocument xdoc)
    {
      var poaFormat = this.GetPoAFormatVersionFromXml(xdoc);
      switch (poaFormat)
      {
        case "001":
          return xdoc.Element(XmlElementNames.PowerOfAttorney)
            ?.Element(XmlElementNames.Document)
            ?.Element(XmlElementNames.PowerOfAttorneyInfo);
        case "002":
        default:
          return xdoc.Element(XmlElementNames.PowerOfAttorney)
            ?.Element(XmlElementNames.Document)
            ?.Element(XmlElementNames.PowerOfAttorneyVersion2)
            ?.Element(XmlElementNames.PowerOfAttorneyInfo);
      }
    }
    
    /// <summary>
    /// Получить XML-элемент с информацией об эл. доверенности.
    /// </summary>
    /// <param name="xdoc">XML-документ.</param>
    /// <param name="poaElementName">Имя элемента, содержащего доверенность.</param>
    /// <param name="documentElementName">Имя элемента, содержащего документ.</param>
    /// <param name="poaInfoElementName">Имя элемента, содержащего информацию о доверенности.</param>
    /// <returns>XML-элемент с информацией о доверенности.</returns>
    [Public, Obsolete("Используйте метод TryGetPoAInfoElement(XDocument)")]
    public virtual System.Xml.Linq.XElement TryGetPoAInfoElement(System.Xml.Linq.XDocument xdoc,
                                                                 string poaElementName,
                                                                 string documentElementName,
                                                                 string poaInfoElementName)
    {
      try
      {
        return xdoc.Element(poaElementName).Element(documentElementName).Element(poaInfoElementName);
      }
      catch (Exception ex)
      {
        Logger.ErrorFormat("Import formalized power of attorney. Failed to parse given XML as formalized power of attorney: {0}",
                           ex.Message);
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.XmlLoadFailed);
      }
    }
    
    /// <summary>
    /// Заполнить единый рег. номер эл. доверенности из xml-файла.
    /// </summary>
    /// <param name="xdoc">Тело доверенности в xml-формате.</param>
    /// <param name="powerOfAttorneyInfo">Xml-элемент с информацией об эл. доверенности.</param>
    /// <param name="poaUnifiedRegNumberAttributeName">Имя атрибута, содержащего единый рег.номер доверенности.</param>
    [Public]
    public virtual void FillUnifiedRegistrationNumberFromXml(System.Xml.Linq.XDocument xdoc,
                                                             System.Xml.Linq.XElement powerOfAttorneyInfo,
                                                             string poaUnifiedRegNumberAttributeName)
    {
      var unifiedRegNumber = this.GetAttributeValueByName(powerOfAttorneyInfo, poaUnifiedRegNumberAttributeName);
      
      Guid guid;
      if (!Guid.TryParse(unifiedRegNumber, out guid))
      {
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.XmlLoadFailed,
                                          FormalizedPowerOfAttorneys.Resources.ErrorValidateUnifiedRegistrationNumber);
      }
      
      _obj.UnifiedRegistrationNumber = guid.ToString();
    }
    
    /// <summary>
    /// Заполнить дату начала и окончания действия эл. доверенности из xml-файла.
    /// </summary>
    /// <param name="xdoc">Тело доверенности в xml-формате.</param>
    /// <param name="powerOfAttorneyInfo">Xml-элемент с информацией об эл. доверенности.</param>
    /// <param name="poaValidFromAttributeName">Имя атрибута, содержащего дату начала действия доверенности.</param>
    /// <param name="poaValidTillAttributeName">Имя атрибута, содержащего дату окончания действия доверенности.</param>
    [Public]
    public virtual void FillValidDatesFromXml(System.Xml.Linq.XDocument xdoc,
                                              System.Xml.Linq.XElement powerOfAttorneyInfo,
                                              string poaValidFromAttributeName,
                                              string poaValidTillAttributeName)
    {
      DateTime? validFrom;
      DateTime? validTill;
      try
      {
        validFrom = this.GetDateFromXml(powerOfAttorneyInfo, poaValidFromAttributeName);
        validTill = this.GetDateFromXml(powerOfAttorneyInfo, poaValidTillAttributeName);
      }
      catch (Exception ex)
      {
        Logger.Error("Import formalized power of attorney. Failed to parse validity dates from xml.", ex);
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.XmlLoadFailed);
      }
      if (validFrom == null || validTill == null)
      {
        Logger.Error("Import formalized power of attorney. Failed to parse validity dates from xml.");
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.XmlLoadFailed);
      }
      _obj.ValidFrom = validFrom;
      _obj.ValidTill = validTill;
    }
    
    /// <summary>
    /// Заполнить рег. данные эл. доверенности в зависимости от настроек вида документа.
    /// </summary>
    /// <param name="number">Регистрационный номер.</param>
    /// <param name="date">Дата регистрации.</param>
    /// <remarks>Если вид документа ненумеруемый, данные не будут заполнены.</remarks>
    [Public]
    public virtual void FillRegistrationData(string number, DateTime? date)
    {
      if (string.IsNullOrEmpty(number) || !date.HasValue)
        return;
      
      // Проверить настройки RX на возможность нумерации документа.
      if (_obj.DocumentKind == null || _obj.DocumentKind.NumberingType == Docflow.DocumentKind.NumberingType.NotNumerable)
        return;
      
      if (_obj.DocumentKind.NumberingType == Docflow.DocumentKind.NumberingType.Numerable)
      {
        var matchingRegistersIds = Functions.OfficialDocument.GetDocumentRegistersIdsByDocument(_obj, Docflow.RegistrationSetting.SettingType.Numeration);
        if (matchingRegistersIds.Count == 1)
        {
          var register = DocumentRegisters.Get(matchingRegistersIds.First());
          Functions.OfficialDocument.RegisterDocument(_obj, register, date, number, false, false);
          return;
        }
      }
      if (_obj.AccessRights.CanRegister())
      {
        _obj.RegistrationDate = date;
        _obj.RegistrationNumber = number;
      }
      else
      {
        var registrationDataString = FormalizedPowerOfAttorneys.Resources.FormalizedPowerOfAttorneyFormat(_obj.DocumentKind.ShortName,
                                                                                                          number,
                                                                                                          date.Value.Date.ToString("d"));
        _obj.Note = registrationDataString + Environment.NewLine + _obj.Note;
      }
      return;
    }
    
    /// <summary>
    /// Заполнить поле Кому эл. доверенности из xml-файла.
    /// </summary>
    /// <param name="xdoc">Тело доверенности в xml-формате.</param>
    [Public]
    public virtual void FillIssuedToFromXml(System.Xml.Linq.XDocument xdoc)
    {
      // Не перезаполнять Кому.
      if (_obj.IssuedTo != null)
        return;
      
      // Не заполнять Кому, если тип представителя не Сотрудник.
      if (_obj.AgentType != Docflow.FormalizedPowerOfAttorney.AgentType.Employee)
        return;
      
      // Получить ИНН, СНИЛС и ФИО из xml.
      var issuedToInfoFromXml = this.GetIssuedToInfoFromXml(xdoc);
      
      // Попытаться заполнить Кому по ИНН.
      if (!string.IsNullOrWhiteSpace(issuedToInfoFromXml.TIN))
      {
        var employees = Company.PublicFunctions.Employee.Remote.GetEmployeesByTIN(issuedToInfoFromXml.TIN);
        if (employees.Count() == 1)
        {
          _obj.IssuedTo = employees.FirstOrDefault();
          return;
        }
      }
      
      // Попытаться заполнить Кому по СНИЛС.
      if (!string.IsNullOrWhiteSpace(issuedToInfoFromXml.INILA))
      {
        var employees = Company.PublicFunctions.Employee.Remote.GetEmployeesByINILA(issuedToInfoFromXml.INILA);
        if (employees.Count() == 1)
        {
          _obj.IssuedTo = employees.FirstOrDefault();
          return;
        }
      }
      
      // Попытаться заполнить Кому по ФИО.
      if (!string.IsNullOrWhiteSpace(issuedToInfoFromXml.FullName))
      {
        if (_obj.IssuedTo == null)
          _obj.IssuedTo = Company.PublicFunctions.Employee.Remote.GetEmployeeByName(issuedToInfoFromXml.FullName);
      }
    }
    
    /// <summary>
    /// Заполнить имя эл. доверенности.
    /// </summary>
    /// <param name="xdoc">Тело доверенности в xml-формате.</param>
    [Public]
    public virtual void FillDocumentName(System.Xml.Linq.XDocument xdoc)
    {
      // Заполнить пустое имя документа из сокращенного имени вида документа.
      if (string.IsNullOrWhiteSpace(_obj.Name) && _obj.DocumentKind != null && _obj.DocumentKind.GenerateDocumentName != true)
        _obj.Name = _obj.DocumentKind.ShortName;
    }
    
    /// <summary>
    /// Импортировать подпись.
    /// </summary>
    /// <param name="xml">Структура с подписанными данными.</param>
    /// <param name="signature">Структура с подписью.</param>
    /// <remarks>В случае если подпись без даты, которая в Sungero обязательна, будет выполнена попытка проставить подпись
    /// хоть как-нибудь. Подпись после этого будет отображаться как невалидная, но она хотя бы будет.
    /// Валидная подпись останется только в сервисе.</remarks>
    [Public]
    public virtual void ImportSignature(Docflow.Structures.Module.IByteArray xml, Docflow.Structures.Module.IByteArray signature)
    {
      var signatureBytes = signature.Bytes;

      // Получить подписавшего из сертификата.
      var certificateInfo = Docflow.PublicFunctions.Module.GetSignatureCertificateInfo(signatureBytes);
      var signatoryName = Docflow.PublicFunctions.Module.GetCertificateSignatoryName(certificateInfo.SubjectInfo);
      
      // Импортировать подпись.
      Signatures.Import(_obj, SignatureType.Approval, signatoryName, signatureBytes, _obj.LastVersion);
    }
    
    /// <summary>
    /// Проверить, что документ подписан. Если нет, сгенерировать исключение.
    /// </summary>
    [Public]
    public virtual void CheckSignature()
    {
      Sungero.Domain.Shared.ISignature importedSignature;
      importedSignature = Signatures.Get(_obj.LastVersion)
        .Where(s => s.IsExternal == true && s.SignCertificate != null)
        .OrderByDescending(x => x.Id)
        .FirstOrDefault();
      
      if (importedSignature == null)
      {
        Logger.DebugFormat("Can't find signature on document with version id: '{0}'", _obj.LastVersion.Id);
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.SignatureImportFailed);
      }
    }
    
    /// <summary>
    /// Получить дату из информации об эл. доверенности из xml-файла.
    /// </summary>
    /// <param name="element">Элемент с датой.</param>
    /// <param name="attributeName">Наименование атрибута для даты.</param>
    /// <returns>Дата.</returns>
    [Public]
    public virtual DateTime? GetDateFromXml(System.Xml.Linq.XElement element, string attributeName)
    {
      var dateValue = this.GetAttributeValueByName(element, attributeName);
      if (string.IsNullOrEmpty(dateValue))
        return null;
      
      DateTime date;
      if (Calendar.TryParseDate(dateValue, out date))
        return date;
      
      return Convert.ToDateTime(dateValue);
    }
    
    /// <summary>
    /// Получить из xml информацию об уполномоченном представителе.
    /// </summary>
    /// <param name="xdoc">Тело доверенности в xml-формате.</param>
    /// <returns>Структура с информацией.</returns>
    [Public]
    public virtual Structures.FormalizedPowerOfAttorney.IIssuedToInfo GetIssuedToInfoFromXml(System.Xml.Linq.XDocument xdoc)
    {
      var result = Structures.FormalizedPowerOfAttorney.IssuedToInfo.Create();
      
      // Получить элементы, связанные с уполномоченным представителем.
      var representativeElements = this.GetRepresentativeElements(xdoc);
      
      // Не искать по сотрудникам, если в xml нет узлов или больше одного узла с уполномоченным представителем, который является физ. лицом.
      if (representativeElements == null || !representativeElements.Any() || representativeElements.Count() > 1)
        return result;
      
      var representativeElement = representativeElements.FirstOrDefault();
      if (representativeElement == null)
        return result;
      
      // Получить ИНН, СНИЛС и ФИО уполномоченного представителя.
      var individualElement = representativeElement.Element(XmlElementNames.Individual);
      if (individualElement == null)
        return result;
      
      var tin = this.GetAttributeValueByName(individualElement, XmlIssuedToAttributeNames.TIN);
      var inila = this.GetAttributeValueByName(individualElement, XmlIssuedToAttributeNames.INILA);
      var fullName = this.GetIssuedToFullNameFromXml(individualElement);
      
      return Structures.FormalizedPowerOfAttorney.IssuedToInfo.Create(fullName, tin, inila);
    }
    
    /// <summary>
    /// Получить XML-элемент c информацией об уполномоченном представителе.
    /// </summary>
    /// <param name="xdoc">Тело доверенности в xml-формате.</param>
    /// <returns>XML-элемент с информацией об уполномоченном представителе.</returns>
    [Public]
    public virtual List<System.Xml.Linq.XElement> GetRepresentativeElements(System.Xml.Linq.XDocument xdoc)
    {
      var poaFormat = this.GetPoAFormatVersionFromXml(xdoc);
      // Получить элементы, связанные с уполномоченным представителем.
      switch (poaFormat)
      {
        case "001":
          return xdoc?.Element(XmlElementNames.PowerOfAttorney)
            ?.Element(XmlElementNames.Document)
            ?.Element(XmlElementNames.AuthorizedRepresentative)
            ?.Elements(XmlElementNames.Representative)
            ?.ToList();
        case "002":
        default:
          return xdoc?.Element(XmlElementNames.PowerOfAttorney)
            ?.Element(XmlElementNames.Document)
            ?.Element(XmlElementNames.PowerOfAttorneyVersion2)
            ?.Elements(XmlElementNames.AuthorizedRepresentative)
            ?.ToList();
      }
    }
    
    /// <summary>
    /// Получить версию формата эл. доверенности из xml-файла.
    /// </summary>
    /// <param name="xdoc">Тело доверенности в xml-формате.</param>
    /// <returns>Версия формата эл. доверенности.</returns>
    [Public]
    public virtual string GetPoAFormatVersionFromXml(System.Xml.Linq.XDocument xdoc)
    {
      var versionFormatElement = xdoc?.Element(XmlElementNames.PowerOfAttorney);
      
      try
      {
        return this.GetAttributeValueByName(versionFormatElement, Constants.FormalizedPowerOfAttorney.XmlFPoAFormatVersionAttributeName);
      }
      catch (Exception ex)
      {
        Logger.ErrorFormat("Import formalized power of attorney. Failed to parse given XML as formalized power of attorney: {0}",
                           ex.Message);
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.XmlLoadFailed);
      }
    }
    
    /// <summary>
    /// Получить имя того, кому выдана эл. доверенность из xml-файла.
    /// </summary>
    /// <param name="individualElement">Элемент xml с информацией о полномочном представителе.</param>
    /// <returns>ФИО.</returns>
    [Public]
    public virtual string GetIssuedToFullNameFromXml(System.Xml.Linq.XElement individualElement)
    {
      var individualNameElement = individualElement.Element(XmlIssuedToAttributeNames.IndividualName);
      if (individualNameElement == null)
        return string.Empty;
      
      // Собрать полные ФИО из фамилии, имени и отчества.
      var parts = new List<string>();
      var surname = this.GetAttributeValueByName(individualNameElement, XmlIssuedToAttributeNames.LastName);
      if (!string.IsNullOrWhiteSpace(surname))
        parts.Add(surname);
      var name = this.GetAttributeValueByName(individualNameElement, XmlIssuedToAttributeNames.FirstName);
      if (!string.IsNullOrWhiteSpace(name))
        parts.Add(name);
      var patronymic = this.GetAttributeValueByName(individualNameElement, XmlIssuedToAttributeNames.MiddleName);
      if (!string.IsNullOrWhiteSpace(patronymic))
        parts.Add(patronymic);
      
      var fullName = string.Join(" ", parts);
      return fullName;
    }
    
    /// <summary>
    /// Получить значение атрибута по имени.
    /// </summary>
    /// <param name="element">Элемент, которому принадлежит атрибут.</param>
    /// <param name="attributeName">Имя атрибута.</param>
    /// <returns>Значение или пустая строка, если атрибут не найден.</returns>
    [Public]
    public virtual string GetAttributeValueByName(System.Xml.Linq.XElement element, string attributeName)
    {
      var attribute = element?.Attribute(attributeName);
      return attribute == null ? string.Empty : attribute.Value;
    }
    
    #endregion
    
    #region Регистрация МЧД
    
    /// <summary>
    /// Зарегистрировать эл. доверенность в ФНС.
    /// </summary>
    /// <returns>Результат отправки: ИД операции регистрации в сервисе доверенностей или ошибка.</returns>
    [Public, Remote]
    public virtual PowerOfAttorneyCore.Structures.Module.IResponseResult RegisterFormalizedPowerOfAttorneyWithService()
    {
      // Отправка запроса на регистрацию.
      var powerOfAttorneyBytes = Functions.Module.GetBinaryData(_obj.LastVersion.Body);
      var signature = Functions.OfficialDocument.GetSignatureFromOurSignatory(_obj, _obj.LastVersion.Id);
      var signatureBytes = signature?.GetDataSignature();
      var sendingResult = PowerOfAttorneyCore.PublicFunctions.Module.SendPowerOfAttorneyForRegistration(_obj.BusinessUnit,
                                                                                                        powerOfAttorneyBytes,
                                                                                                        signatureBytes);
      if (!string.IsNullOrWhiteSpace(sendingResult.ErrorCode) || !string.IsNullOrWhiteSpace(sendingResult.ErrorType))
      {
        this.HandleRegistrationError(sendingResult.ErrorCode);
        return sendingResult;
      }

      // Успешная отправка на регистрацию.
      _obj.LifeCycleState = Docflow.FormalizedPowerOfAttorney.LifeCycleState.Draft;
      _obj.FtsListState = Docflow.FormalizedPowerOfAttorney.FtsListState.OnRegistration;
      _obj.Save();
      
      var queueItem = PowerOfAttorneyQueueItems.Create();
      queueItem.OperationType = Docflow.PowerOfAttorneyQueueItem.OperationType.Registration;
      queueItem.DocumentId = _obj.Id;
      queueItem.OperationId = sendingResult.OperationId;
      queueItem.Save();
      
      sendingResult.QueueItem = queueItem;

      // Запуск мониторинга регистрации доверенности в сервисе доверенностей.
      var getFPoAStateHandler = AsyncHandlers.SetFPoARegistrationState.Create();
      getFPoAStateHandler.QueueItemId = queueItem.Id;
      getFPoAStateHandler.ExecuteAsync(FormalizedPowerOfAttorneys.Resources.FormalizedPowerOfAttorneySentForRegistrationSuccessfully,
                                       FormalizedPowerOfAttorneys.Resources.FormalizedPowerOfAttorneyRegistrationCompleted);
      return sendingResult;
    }
    
    /// <summary>
    /// Заполнить Состояние, статус В реестре ФНС и сообщение об ошибке по коду ошибки.
    /// </summary>
    /// <param name="errorCode">Код ошибки.</param>
    [Public]
    public virtual void HandleRegistrationError(string errorCode)
    {
      _obj.LifeCycleState = Sungero.Docflow.FormalizedPowerOfAttorney.LifeCycleState.Draft;
      _obj.FtsListState = Sungero.Docflow.FormalizedPowerOfAttorney.FtsListState.Rejected;
      
      var reasonsDictionary = this.GetErrorCodeAndReasonMapping();
      _obj.FtsRejectReason = reasonsDictionary
        .Where(x => string.Equals(x.Key, errorCode, StringComparison.InvariantCultureIgnoreCase))
        .Select(x => x.Value)
        .FirstOrDefault();
      if (string.IsNullOrEmpty(_obj.FtsRejectReason))
        _obj.FtsRejectReason = Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.DefaultErrorMessage;
      
      _obj.Save();
      
      Logger.Error($"HandleRegistrationError. Power of attorney registration error = {errorCode} (PoA id = {_obj.Id}).");
    }
    
    public virtual System.Collections.Generic.Dictionary<string, string> GetErrorCodeAndReasonMapping()
    {
      var result = new Dictionary<string, string>();
      
      result.Add(Constants.FormalizedPowerOfAttorney.FPoARegistrationErrors.ExternalSystemIsUnavailableError,
                 Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.ExternalSystemIsUnavailableErrorMessage);
      result.Add(Constants.FormalizedPowerOfAttorney.FPoARegistrationErrors.InvalidCertificateError,
                 Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.DifferentSignatureErrorMessage);
      result.Add(Constants.FormalizedPowerOfAttorney.FPoARegistrationErrors.RepeatedRegistrationError,
                 Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.ConflictDifferentSignatureErrorMessage);
      result.Add(Constants.FormalizedPowerOfAttorney.FPoARegistrationErrors.DifferentSignatureError,
                 Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.DifferentSignatureErrorMessage);
      result.Add(Constants.FormalizedPowerOfAttorney.FPoARegistrationErrors.UnsupportedPoA,
                 Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.UnsupportedPoAErrorMessage);
      
      return result;
    }

    #endregion

    #region Поиск дублей

    /// <summary>
    /// Получить дубли эл. доверенности.
    /// </summary>
    /// <returns>Дубли эл. доверенности.</returns>
    [Public, Remote(IsPure = true)]
    public virtual List<IFormalizedPowerOfAttorney> GetFormalizedPowerOfAttorneyDuplicates()
    {
      var duplicates = new List<IFormalizedPowerOfAttorney>();
      if (_obj.IssuedTo == null ||
          _obj.BusinessUnit == null ||
          string.IsNullOrEmpty(_obj.UnifiedRegistrationNumber))
      {
        return duplicates;
      }

      AccessRights.AllowRead(
        () =>
        {
          duplicates = FormalizedPowerOfAttorneys
            .GetAll()
            .Where(f => !Equals(f, _obj) && f.LifeCycleState == LifeCycleState.Active &&
                   f.UnifiedRegistrationNumber == _obj.UnifiedRegistrationNumber &&
                   Equals(f.IssuedTo, _obj.IssuedTo) &&
                   Equals(f.BusinessUnit, _obj.BusinessUnit))
            .ToList();
        });
      return duplicates;
    }
    
    #endregion
    
    #region Печатная форма
    
    /// <summary>
    /// Сгенерировать PDF из тела доверенности.
    /// </summary>
    [Public, Remote(IsPure = true)]
    public virtual void GenerateFormalizedPowerOfAttorneyPdf()
    {
      this.ConvertToPdfAndAddSignatureMark(_obj.LastVersion.Id);
      
      PublicFunctions.Module.LogPdfConverting("Signature mark. Added interactively", _obj, _obj.LastVersion);
    }
    
    /// <summary>
    /// Преобразовать документ в PDF с наложением отметки об ЭП.
    /// </summary>
    /// <returns>Результат преобразования.</returns>
    /// <remarks>Перед преобразованием валидируются документ и подпись на версии.</remarks>
    [Remote]
    public override Structures.OfficialDocument.СonversionToPdfResult ConvertToPdfWithSignatureMark()
    {
      var versionId = _obj.LastVersion.Id;
      var info = this.ValidateDocumentBeforeConvertion(versionId);
      if (info.HasErrors)
        return info;
      
      info = this.ConvertToPdfAndAddSignatureMark(versionId);
      info.ErrorTitle = OfficialDocuments.Resources.ConvertionErrorTitleBase;
      
      return info;
    }
    
    /// <summary>
    /// Преобразовать документ в PDF и поставить отметку об ЭП, если есть утверждающая подпись.
    /// </summary>
    /// <param name="versionId">ИД версии документа.</param>
    /// <returns>Результат преобразования в PDF.</returns>
    [Remote]
    public override Structures.OfficialDocument.СonversionToPdfResult ConvertToPdfAndAddSignatureMark(int versionId)
    {
      var signatureMark = string.Empty;
      
      var signature = Functions.OfficialDocument.GetSignatureFromOurSignatory(_obj, versionId);
      if (signature != null)
        signatureMark = Functions.Module.GetSignatureMarkAsHtml(_obj, signature);
      
      return this.GeneratePublicBodyWithSignatureMark(versionId, signatureMark);
    }
    
    /// <summary>
    /// Получить электронную подпись для простановки отметки.
    /// </summary>
    /// <param name="versionId">Номер версии.</param>
    /// <returns>Электронная подпись.</returns>
    [Public]
    public override Sungero.Domain.Shared.ISignature GetSignatureForMark(int versionId)
    {
      return this.GetSignatureFromOurSignatory(versionId);
    }
    
    /// <summary>
    /// Получить тело и расширение версии для преобразования в PDF с отметкой об ЭП.
    /// </summary>
    /// <param name="version">Версия для генерации.</param>
    /// <param name="isSignatureMark">Признак отметки об ЭП. True - отметка об ЭП, False - отметка о поступлении.</param>
    /// <returns>Тело версии документа и расширение.</returns>
    /// <remarks>Для преобразования в PDF эл. доверенности необходимо сначала получить ее в виде html.</remarks>
    [Public]
    public override Structures.OfficialDocument.IVersionBody GetBodyToConvertToPdf(Sungero.Content.IElectronicDocumentVersions version, bool isSignatureMark)
    {
      var result = Structures.OfficialDocument.VersionBody.Create();
      if (version == null)
        return result;
      
      var html = this.GetFormalizedPowerOfAttorneyAsHtml(version);
      if (string.IsNullOrWhiteSpace(html))
        return result;

      result.Body = System.Text.Encoding.UTF8.GetBytes(html);
      result.Extension = Constants.FormalizedPowerOfAttorney.HtmlExtension;
      return result;
    }
    
    /// <summary>
    /// Получить эл. доверенность в виде html.
    /// </summary>
    /// <param name="version">Версия, на основании которой формируется html.</param>
    /// <returns>Эл. доверенность в виде html.</returns>
    [Public]
    public virtual string GetFormalizedPowerOfAttorneyAsHtml(Sungero.Content.IElectronicDocumentVersions version)
    {
      if (version == null)
        return string.Empty;
      
      // Получить модель эл. доверенности из xml.
      using (var body = new System.IO.MemoryStream())
      {
        // Выключить error-логирование при доступе к зашифрованным бинарным данным.
        AccessRights.SuppressSecurityEvents(() => version.Body.Read().CopyTo(body));
        var powerOfAttorney = FormalizeDocumentsParser.Extension.GetPowerOfAttorney(body.ToArray());
        return powerOfAttorney != null
          ? FormalizeDocumentsParser.PowerOfAttorney.PoAV2HtmlProducer.ProducePoAHtml(powerOfAttorney)
          : string.Empty;
      }
    }
    
    /// <summary>
    /// Определить, поддерживается ли преобразование в PDF для переданного расширения.
    /// </summary>
    /// <param name="extension">Расширение.</param>
    /// <returns>True, если поддерживается, иначе False.</returns>
    /// <remarks>МЧД имеют расширение XML, которое всегда поддерживается.</remarks>
    [Public]
    public override bool CheckPdfConvertibilityByExtension(string extension)
    {
      return true;
    }

    #endregion
    
    #region Проверка состояния в сервисе
    
    /// <summary>
    /// Проверить состояние эл. доверенности в ФНС.
    /// </summary>
    /// <returns>Результат проверки.</returns>
    [Public, Remote]
    public virtual string CheckFormalizedPowerOfAttorneyState()
    {
      // Если доверенность в процессе регистрации или отзыва - не выполнять запрос в сервис.
      var registrationInProcess = this.RegistrationInProcess();
      if (registrationInProcess)
        return FormalizedPowerOfAttorneys.Resources.FPoARegistrationInProcess;
      
      var revocationInProcess = this.RevocationInProcess();
      if (revocationInProcess)
        return FormalizedPowerOfAttorneys.Resources.FPoARevocationInProcess;
      
      // Отправка запроса на валидацию.
      var agent = this.CreateAgent();
      var powerOfAttorneyXml = Docflow.PublicFunctions.Module.GetBinaryData(_obj.LastVersion.Body);
      var signature = Docflow.PublicFunctions.OfficialDocument.GetSignatureFromOurSignatory(_obj, _obj.LastVersion.Id);
      var signatureBytes = signature?.GetDataSignature();
      var validationState = PowerOfAttorneyCore.PublicFunctions.Module.CheckPowerOfAttorneyState(_obj.BusinessUnit, _obj.UnifiedRegistrationNumber,
                                                                                                 agent, powerOfAttorneyXml, signatureBytes);
      
      if (validationState.Errors.Any(x => string.Equals(x.Type, PoAServiceErrors.ConnectionError, StringComparison.InvariantCultureIgnoreCase)))
        return PowerOfAttorneyCore.Resources.PowerOfAttorneyNoConnection;
      
      if (string.IsNullOrEmpty(validationState.Result))
        return Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FPoAStateCheckFailed;
      
      if (validationState.Result == Constants.FormalizedPowerOfAttorney.FPoAState.Valid)
      {
        // Ответ не получен за таймаут или сразу вернулся ответ, что данные не актуальны.
        // При переповторе данные могут успеть актуализироваться.
        if (validationState.Errors.Any(x => string.Equals(x.Code, PoAServiceErrors.StateIsOutdated, StringComparison.InvariantCultureIgnoreCase)))
          return Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FPoAStateCheckFailed;
        
        if (validationState.Errors.Any(x => string.Equals(x.Code, PoAServiceErrors.PoANotFound, StringComparison.InvariantCultureIgnoreCase)))
          return Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FPoANotFound;
        
        Functions.FormalizedPowerOfAttorney.SetLifeCycleAndFtsListStates(_obj, LifeCycleState.Active, FtsListState.Registered);
        _obj.Save();
        return FormalizedPowerOfAttorneys.Resources.FPoAStateHasBeenUpdated;
      }
      
      if (validationState.Result == Constants.FormalizedPowerOfAttorney.FPoAState.Invalid)
      {
        // Для отозванных дополнительно запросить причину и дату подписания отзыва.
        if (validationState.Errors.Any(x => string.Equals(x.Code, Constants.FormalizedPowerOfAttorney.FPoAState.Revoked, StringComparison.InvariantCultureIgnoreCase)))
        {
          if (!PublicFunctions.FormalizedPowerOfAttorney.SetRevokedState(_obj))
            return Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FPoASetRevokedStateFailed;
          
          PublicFunctions.FormalizedPowerOfAttorney.Remote.CreateSetSignatureSettingsValidTillAsyncHandler(_obj, _obj.ValidTill.Value);
          return FormalizedPowerOfAttorneys.Resources.FPoAStateHasBeenUpdated;
        }
        
        if (validationState.Errors.Any(x => string.Equals(x.Code, Constants.FormalizedPowerOfAttorney.FPoAState.Expired, StringComparison.InvariantCultureIgnoreCase)))
        {
          Functions.FormalizedPowerOfAttorney.SetLifeCycleAndFtsListStates(_obj, LifeCycleState.Obsolete, FtsListState.Registered);
          _obj.Save();
          return FormalizedPowerOfAttorneys.Resources.FPoAStateHasBeenUpdated;
        }
        
        if (validationState.Errors.Any(x => string.Equals(x.Code, PoAServiceErrors.PoaIsNotValidYet, StringComparison.InvariantCultureIgnoreCase)))
          return Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FPoANotFound;
        
        return Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FPoAAttributesNotMatchXml;
      }
      
      return Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FPoAStateCheckFailed;
    }
    
    /// <summary>
    /// Проверить, находится ли эл. доверенность в процессе регистрации.
    /// </summary>
    /// <returns>True - если в процессе регистрации, иначе - false.</returns>
    public virtual bool RegistrationInProcess()
    {
      if (_obj.FtsListState == Docflow.FormalizedPowerOfAttorney.FtsListState.OnRegistration)
      {
        var registrationQueueItem = PowerOfAttorneyQueueItems.GetAll()
          .Where(q => q.DocumentId == _obj.Id && q.OperationType == Docflow.PowerOfAttorneyQueueItem.OperationType.Registration)
          .FirstOrDefault();
        
        return registrationQueueItem != null;
      }
      
      return false;
    }
    
    /// <summary>
    /// Проверить, находится ли эл. доверенность в процессе отзыва.
    /// </summary>
    /// <returns>True - если в процессе отзыва, иначе - false.</returns>
    public virtual bool RevocationInProcess()
    {
      var revocation = PowerOfAttorneyRevocations.GetAll().Where(r => Equals(r.FormalizedPowerOfAttorney, _obj)).FirstOrDefault();
      if (revocation != null)
      {
        var revocationQueueItem = PowerOfAttorneyQueueItems.GetAll()
          .Where(q => q.DocumentId == revocation.Id && q.OperationType == Docflow.PowerOfAttorneyQueueItem.OperationType.Revocation)
          .FirstOrDefault();
        
        return revocationQueueItem != null;
      }
      
      return false;
    }
    
    /// <summary>
    /// Создать асинхронное событие установки "Действует по" во всех правах подписи,
    /// где в качестве документа-основания указана эл. доверенность.
    /// </summary>
    /// <param name="validTill">Дата, по которую действует эл. доверенность.</param>
    /// <remarks>Выполняется асинхронно.</remarks>
    [Public, Remote]
    public virtual void CreateSetSignatureSettingsValidTillAsyncHandler(DateTime validTill)
    {
      var signatureSettingIds = Docflow.SignatureSettings.GetAll().Where(x => Equals(x.Document, _obj)).Select(x => x.Id);
      foreach (var settingId in signatureSettingIds)
      {
        var asyncSetSignatureSettingValidTillHandler = Docflow.AsyncHandlers.SetSignatureSettingValidTill.Create();
        asyncSetSignatureSettingValidTillHandler.SignatureSettingId = settingId;
        asyncSetSignatureSettingValidTillHandler.ValidTill = validTill;
        asyncSetSignatureSettingValidTillHandler.ExecuteAsync();
      }
    }
    
    /// <summary>
    /// Зарегистрировать операцию валидации доверенности на сервисе.
    /// </summary>
    /// <param name="serviceConnection">Подключение к сервису доверенностей.</param>
    /// <param name="queueItem">Элемент очереди синхронизации эл. доверенностей.</param>
    /// <returns>True - если нужно продолжить дальнейшую обработку элемента очереди.</returns>
    [Public]
    public virtual bool EnqueueValidation(Sungero.PowerOfAttorneyCore.IPowerOfAttorneyServiceConnection serviceConnection, IPowerOfAttorneyQueueItem queueItem)
    {
      queueItem = PowerOfAttorneyQueueItems.GetAll().FirstOrDefault(x => Equals(x, queueItem));
      if (queueItem == null)
      {
        Logger.DebugFormat("EnqueueValidation: Queue item is null. Formalized power of attorney id {0}.", _obj.Id);
        return false;
      }
      
      if (!_obj.HasVersions)
      {
        Logger.DebugFormat("EnqueueValidation: Formalized power of attorney with id {0} has no version.", _obj.Id);
        return false;
      }
      
      var contentBytes = Docflow.PublicFunctions.Module.GetBinaryData(_obj.LastVersion.Body);
      
      if (contentBytes == null || contentBytes.Length == 0)
      {
        Logger.DebugFormat("EnqueueValidation: Document body is empty. Formalized power of attorney id {0}.", _obj.Id);
        return false;
      }

      var signature = Docflow.PublicFunctions.OfficialDocument.GetSignatureFromOurSignatory(_obj, _obj.LastVersion.Id);
      var signatureBytes = signature?.GetDataSignature();
      
      if (signatureBytes == null)
      {
        Logger.DebugFormat("EnqueueValidation: Signature is empty. Formalized power of attorney id {0}.", _obj.Id);
        return false;
      }
      
      var agent = this.CreateAgent();
      var sendingResult = PowerOfAttorneyCore.PublicFunctions.Module.EnqueuePoAValidation(serviceConnection, _obj.BusinessUnit, _obj.UnifiedRegistrationNumber, agent, contentBytes, signatureBytes);
      
      // Проверить, что queueItem ещё существует.
      queueItem = PowerOfAttorneyQueueItems.GetAll().FirstOrDefault(x => Equals(x, queueItem));
      if (queueItem == null)
      {
        Logger.DebugFormat("EnqueueValidation: Queue item is null. Formalized power of attorney id {0}.", _obj.Id);
        return false;
      }
      
      if (!string.IsNullOrEmpty(sendingResult.OperationId))
      {
        Logger.DebugFormat("EnqueueValidation: Operation id {0} successfully received. Formalized power of attorney id {1}.", sendingResult.OperationId, _obj.Id);
        queueItem.OperationId = sendingResult.OperationId;
        queueItem.Save();
      }
      
      return true;
    }
    
    /// <summary>
    /// Сформировать представителя в зависимости от типа.
    /// </summary>
    /// <returns>Представитель.</returns>
    public virtual PowerOfAttorneyCore.Structures.Module.IAgent CreateAgent()
    {
      var agent = PowerOfAttorneyCore.Structures.Module.Agent.Create();
      var representative = People.Null;
      
      if ((_obj.AgentType == Sungero.Docflow.FormalizedPowerOfAttorney.AgentType.Person ||
           _obj.AgentType == Sungero.Docflow.FormalizedPowerOfAttorney.AgentType.Employee) &&
          _obj.IssuedToParty != null)
      {
        representative = People.As(_obj.IssuedToParty);
        agent.Name = representative?.FirstName;
        agent.Middlename = representative?.MiddleName;
        agent.Surname = representative?.LastName;
        agent.TIN = representative?.TIN;
        agent.INILA = representative?.INILA;
      }
      else if (_obj.AgentType == Sungero.Docflow.FormalizedPowerOfAttorney.AgentType.Entrepreneur &&
               _obj.Representative != null && _obj.IssuedToParty != null)
      {
        representative = People.As(_obj.Representative);
        var entrepreneur = CompanyBases.As(_obj.IssuedToParty);
        agent.Name = representative?.FirstName;
        agent.Middlename = representative?.MiddleName;
        agent.Surname = representative?.LastName;
        agent.TIN = entrepreneur?.TIN;
      }
      else if (_obj.AgentType == Sungero.Docflow.FormalizedPowerOfAttorney.AgentType.LegalEntity &&
               _obj.Representative != null && _obj.IssuedToParty != null)
      {
        representative = People.As(_obj.Representative);
        var legalEntity = CompanyBases.As(_obj.IssuedToParty);
        agent.Name = representative?.FirstName;
        agent.Middlename = representative?.MiddleName;
        agent.Surname = representative?.LastName;
        agent.INILA = representative?.INILA;
        agent.TINUl = legalEntity?.TIN;
        agent.TRRC = legalEntity?.TRRC;
      }
      else
      {
        Logger.ErrorFormat("CreateAgent. Power of attorney validation error: AgentType is incorrect.");
        agent = null;
      }
      
      return agent;
    }

    /// <summary>
    /// Обновить статус валидации доверенности на сервисе.
    /// </summary>
    /// <param name="serviceConnection">Подключение к сервису доверенностей.</param>
    /// <param name="queueItem">Элемент очереди синхронизации эл. доверенностей.</param>
    /// <returns>True - если нужно продолжить дальнейшую обработку элемента очереди.</returns>
    [Public]
    public virtual bool UpdateValidationServiceStatus(Sungero.PowerOfAttorneyCore.IPowerOfAttorneyServiceConnection serviceConnection, IPowerOfAttorneyQueueItem queueItem)
    {
      if (queueItem == null)
      {
        Logger.DebugFormat("UpdateValidationServiceStatus: Queue item is null (PoA id = {0}).", _obj.Id);
        return false;
      }
      
      var validationState = PowerOfAttorneyCore.PublicFunctions.Module.GetPoAValidationState(serviceConnection, queueItem.OperationId);
      
      if (validationState.OperationStatus == Constants.FormalizedPowerOfAttorney.FPoARegistrationStatus.Error)
      {
        var notFoundError = validationState.Errors.FirstOrDefault(x => string.Equals(x.Code,
                                                                                     Constants.FormalizedPowerOfAttorney.FPoAState.PoANotFoundError,
                                                                                     StringComparison.InvariantCultureIgnoreCase));
        if (notFoundError?.Code != null)
        {
          queueItem.RejectCode = notFoundError.Code;
          queueItem.Save();
          Logger.DebugFormat("UpdateValidationServiceStatus: Formalized power of attorney with id {0} not found in FTS register.", _obj.Id);
          return true;
        }
        
        Logger.DebugFormat("UpdateValidationServiceStatus: Formalized power of attorney with id {0} validation error.", _obj.Id);
        return false;
      }
      
      if (validationState.OperationStatus == Constants.FormalizedPowerOfAttorney.FPoARegistrationStatus.Done)
      {
        if (validationState.Result == Constants.FormalizedPowerOfAttorney.FPoAState.Valid)
        {
          Logger.DebugFormat("UpdateValidationServiceStatus: Formalized power of attorney with id {0} is valid, no processing required.", _obj.Id);
          queueItem.FormalizedPoAServiceStatus = Sungero.Docflow.PowerOfAttorneyQueueItem.FormalizedPoAServiceStatus.Valid;
          queueItem.Save();
          return true;
        }
        else if (validationState.Result == Constants.FormalizedPowerOfAttorney.FPoAState.Invalid)
        {
          var revokedError = validationState.Errors.FirstOrDefault(x => string.Equals(x.Code,
                                                                                      Constants.FormalizedPowerOfAttorney.FPoAState.Revoked,
                                                                                      StringComparison.InvariantCultureIgnoreCase));
          if (revokedError?.Code != null)
          {
            queueItem.RejectCode = revokedError.Code;
            queueItem.FormalizedPoAServiceStatus = Sungero.Docflow.PowerOfAttorneyQueueItem.FormalizedPoAServiceStatus.Invalid;
            queueItem.Save();
            Logger.DebugFormat("UpdateValidationServiceStatus: Formalized power of attorney with id {0} was revoked in FTS register, trying to update validation service status.", _obj.Id);
            return true;
          }
          
          Logger.DebugFormat("UpdateValidationServiceStatus: Formalized power of attorney with id {0} is invalid.", _obj.Id);
          queueItem.FormalizedPoAServiceStatus = Sungero.Docflow.PowerOfAttorneyQueueItem.FormalizedPoAServiceStatus.Invalid;
          queueItem.Save();
          return false;
        }
      }
      
      return true;
    }
    
    /// <summary>
    /// Установить эл. доверенность в отозванное состояние.
    /// </summary>
    /// <returns>True - если доверенность успешно перешла в отозванное состояние.</returns>
    [Public]
    public virtual bool SetRevokedState()
    {
      var serviceConnection = Sungero.PowerOfAttorneyCore.PublicFunctions.Module.GetPowerOfAttorneyServiceConnection(_obj.BusinessUnit);
      return this.SetRevokedState(serviceConnection);
    }
    
    /// <summary>
    /// Установить эл. доверенность в отозванное состояние.
    /// </summary>
    /// <param name="serviceConnection">Подключение к сервису доверенностей.</param>
    /// <returns>True - если доверенность успешно перешла в отозванное состояние.</returns>
    [Public]
    public virtual bool SetRevokedState(Sungero.PowerOfAttorneyCore.IPowerOfAttorneyServiceConnection serviceConnection)
    {
      var revocationInfo = PowerOfAttorneyCore.PublicFunctions.Module.Remote.GetPowerOfAttorneyRevocationInfo(serviceConnection, _obj.UnifiedRegistrationNumber);
      
      if (revocationInfo == null)
      {
        Logger.DebugFormat("SetRevokedState. Cannot obtain revocation data from service, trying to set power of attorney with id {0} to revoked state.", _obj.Id);
        return this.SetRevokedState(string.Empty, Calendar.UserToday);
      }
      
      Logger.DebugFormat("SetRevokedState. Revocation data for power of attorney with id {0}: reason {1}, date {2}.", _obj.Id, revocationInfo.Reason, revocationInfo.Date);
      return this.SetRevokedState(revocationInfo.Reason, revocationInfo.Date);
    }
    
    /// <summary>
    /// Установить эл. доверенность в отозванное состояние.
    /// </summary>
    /// <param name="reason">Причина отзыва.</param>
    /// <param name="revocationDate">Дата отзыва.</param>
    /// <returns>True - если доверенность успешно перешла в отозванное состояние.</returns>
    [Public]
    public virtual bool SetRevokedState(string reason, DateTime revocationDate)
    {
      Logger.DebugFormat("SetRevokedState. Business unit id = {0}. Unified registration number = {1}.", _obj.BusinessUnit?.Id,  _obj.UnifiedRegistrationNumber);
      
      try
      {
        if (_obj.LifeCycleState != Sungero.Docflow.FormalizedPowerOfAttorney.LifeCycleState.Obsolete)
          _obj.LifeCycleState = Sungero.Docflow.FormalizedPowerOfAttorney.LifeCycleState.Obsolete;
        if (_obj.FtsListState != Sungero.Docflow.FormalizedPowerOfAttorney.FtsListState.Revoked)
          _obj.FtsListState = Sungero.Docflow.FormalizedPowerOfAttorney.FtsListState.Revoked;
        // Нельзя установить дату меньше, чем "Действует с".
        if (_obj.ValidTill != revocationDate && _obj.ValidTill > revocationDate)
          _obj.ValidTill = _obj.ValidFrom > revocationDate ? _obj.ValidFrom : revocationDate;
        
        if (!string.IsNullOrEmpty(reason) && (string.IsNullOrEmpty(_obj.Note) || !_obj.Note.Contains(reason)))
        {
          var reasonPrefix = string.IsNullOrWhiteSpace(_obj.Note) ? string.Empty : Environment.NewLine;
          _obj.Note += Sungero.Docflow.FormalizedPowerOfAttorneys.Resources.FormalizedPowerOfAttorneyRevocationReasonFormat(reasonPrefix, reason);
        }
        if (_obj.State.IsChanged)
          _obj.Save();
      }
      catch (Exception ex)
      {
        Logger.ErrorFormat("SetRevokedState. Failed to set revoked state (PoA id = {0}).", ex, _obj.Id);
        return false;
      }
      
      return true;
    }
    
    /// <summary>
    /// Установить состояние эл. доверенности.
    /// </summary>
    /// <param name="lifeCycleState">Состояние жизненного цикла.</param>
    /// <param name="ftsListState">Состояние в реестре ФНС.</param>
    /// <returns>True - если доверенность успешно перешла в состояние.</returns>
    [Public]
    public virtual bool TrySetLifeCycleAndFtsListStates(Enumeration? lifeCycleState, Enumeration? ftsListState)
    {
      try
      {
        Functions.FormalizedPowerOfAttorney.SetLifeCycleAndFtsListStates(_obj, lifeCycleState, ftsListState);
        _obj.Save();
      }
      catch (Exception ex)
      {
        Logger.ErrorFormat("TrySetLifeCycleStateAndFtsListState. Failed to set LifeCycleState and FtsListState (PoA id = {0}).", ex, _obj.Id);
        return false;
      }
      
      return true;
    }
    
    #endregion

    #region Валидации доверенности
    
    /// <summary>
    /// Проверить эл. доверенность перед отправкой запроса к сервису доверенностей.
    /// </summary>
    /// <returns>Сообщение об ошибке или пустая строка, если ошибок нет.</returns>
    [Public, Remote]
    public virtual string ValidateFormalizedPoABeforeSending()
    {
      var validationError = this.ValidateBodyAndSignature();

      if (!string.IsNullOrEmpty(validationError))
        return validationError;

      if (!Sungero.PowerOfAttorneyCore.PublicFunctions.Module.HasPowerOfAttorneyServiceConnection(_obj.BusinessUnit))
        return Sungero.PowerOfAttorneyCore.Resources.ServiceConnectionNotConfigured;
      
      // Валидация xml по схеме.
      try
      {
        var body = Docflow.PublicFunctions.Module.GetBinaryData(_obj.LastVersion.Body);
        var xml = Docflow.Structures.Module.ByteArray.Create(body);
        this.ValidateFormalizedPowerOfAttorneyXml(xml);
      }
      catch
      {
        return FormalizedPowerOfAttorneys.Resources.XmlLoadFailed;
      }
      
      return string.Empty;
    }
    
    /// <summary>
    /// Проверить валидность xml-файла эл. доверенности.
    /// </summary>
    /// <param name="xml">Тело эл. доверенности.</param>
    [Public]
    public virtual void ValidateFormalizedPowerOfAttorneyXml(Docflow.Structures.Module.IByteArray xml)
    {
      var memoryStream = new System.IO.MemoryStream(xml.Bytes);
      System.Xml.Linq.XDocument xdoc;
      try
      {
        xdoc = System.Xml.Linq.XDocument.Load(memoryStream);
      }
      catch (Exception e)
      {
        Logger.ErrorFormat("Import formalized power of attorney. Failed to load XML: {0}", e.Message);
        throw AppliedCodeException.Create(FormalizedPowerOfAttorneys.Resources.XmlLoadFailed);
      }
      finally
      {
        xdoc = null;
        memoryStream.Close();
      }
    }
    
    /// <summary>
    /// Проверить, отключена ли валидация рег.номера.
    /// </summary>
    /// <returns>Для МЧД всегда отключена.</returns>
    public override bool IsNumberValidationDisabled()
    {
      return true;
    }
    
    #endregion
    
    #region История смены состояний
    
    public override System.Collections.Generic.IEnumerable<Sungero.Docflow.Structures.OfficialDocument.HistoryOperation> StatusChangeHistoryOperations(Sungero.Content.DocumentHistoryEventArgs e)
    {
      foreach (var operation in base.StatusChangeHistoryOperations(e))
      {
        yield return operation;
      }
      
      if (_obj.FtsListState != _obj.State.Properties.FtsListState.OriginalValue)
      {
        if (_obj.FtsListState != null)
          yield return Sungero.Docflow.Structures.OfficialDocument.HistoryOperation.Create(
            Constants.FormalizedPowerOfAttorney.Operation.FtsStateChange,
            Sungero.Docflow.FormalizedPowerOfAttorneys.Info.Properties.FtsListState.GetLocalizedValue(_obj.FtsListState));
        else
          yield return Sungero.Docflow.Structures.OfficialDocument.HistoryOperation.Create(
            Constants.FormalizedPowerOfAttorney.Operation.FtsStateClear, null);
      }
    }
    
    #endregion
    
    /// <summary>
    /// Создать простую задачу с уведомлением по отзыву электронной доверенности.
    /// </summary>
    [Public]
    public virtual void SendNoticeForRevokedFormalizedPoA()
    {
      // Получение параметров для задачи с уведомлением.
      var subject = FormalizedPowerOfAttorneys.Resources.TitleForNoticeFormat(_obj.Name);
      var performers = this.GetRevokedPoANotificationReceivers();
      
      // Проверка на корректность параметров.
      if (performers.Count == 0)
      {
        Logger.DebugFormat("SendNoticeForRevokedFormalizedPoA. No users to receive notification (PoA id = {0}).", _obj.Id);
        return;
      }
      
      try
      {
        var task = Workflow.SimpleTasks.CreateWithNotices(subject, performers, new[] { _obj });
        task.ActiveText = _obj.Note;
        task.Start();
        Logger.DebugFormat("SendNoticeForRevokedFormalizedPoA. Notice of revocation was sent successfully (task id = {0}, recipient id = {1}).", task.Id, string.Join<int>(", ", performers.Select(u => u.Id)));
      }
      catch (Exception ex)
      {
        Logger.ErrorFormat("SendNoticeForRevokedFormalizedPoA. Error sending notice of revocation: {0}.", ex);
      }
    }
    
    /// <summary>
    /// Получить список адресатов уведомления об отзыве  электронной доверенности.
    /// </summary>
    /// <returns> Список адресатов.</returns>
    public virtual List<IUser> GetRevokedPoANotificationReceivers()
    {
      var issuedTo = _obj.IssuedTo;
      var preparedBy = _obj.PreparedBy;
      var issuedToManager = Company.Employees.Null;
      if (issuedTo != null)
        issuedToManager = PublicFunctions.Module.Remote.GetManager(issuedTo);
      
      var performers = new List<IUser>();
      
      if (issuedTo != null && issuedTo.Status == Sungero.Company.Employee.Status.Active)
      {
        var needNotice = Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(issuedTo).MyRevokedFormalizedPoANotification;
        if (needNotice == true)
          performers.Add(issuedTo);
      }
      
      if (preparedBy != null && preparedBy.Status == Sungero.Company.Employee.Status.Active)
      {
        var needNotice = Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(preparedBy).MyRevokedFormalizedPoANotification;
        if (needNotice == true)
          performers.Add(preparedBy);
      }
      
      if (issuedToManager != null && issuedToManager.Status == Sungero.Company.Employee.Status.Active)
      {
        var needNotice = Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(issuedToManager).MySubordinatesRevokedFormalizedPoANotification;
        if (needNotice == true)
          performers.Add(issuedToManager);
      }

      return performers.Distinct().ToList();
    }
    
    /// <summary>
    /// Заполнить подписывающего в карточке документа.
    /// </summary>
    /// <param name="employee">Сотрудник.</param>
    [Remote]
    public override void SetDocumentSignatory(Company.IEmployee employee)
    {
      if (_obj.OurSignatory != null)
        return;
      base.SetDocumentSignatory(employee);
    }
    
    /// <summary>
    /// Заполнить основание в карточке документа.
    /// </summary>
    /// <param name="employee">Сотрудник.</param>
    /// <param name="e">Аргументы события подписания.</param>
    /// <param name="changedSignatory">Признак смены подписывающего.</param>
    public override void SetOurSigningReason(Company.IEmployee employee, Sungero.Domain.BeforeSigningEventArgs e, bool changedSignatory)
    {
      if (!Equals(_obj.OurSignatory, employee))
        return;
      base.SetOurSigningReason(employee, e, changedSignatory);
    }
    
    /// <summary>
    /// Заполнить Единый рег. № из эл. доверенности в подпись.
    /// </summary>
    /// <param name="employee">Сотрудник.</param>
    /// <param name="signature">Подпись.</param>
    /// <param name="certificate">Сертификат для подписания.</param>
    public override void SetUnifiedRegistrationNumber(Company.IEmployee employee, Sungero.Domain.Shared.ISignature signature, ICertificate certificate)
    {
      if (signature.SignCertificate == null)
        return;

      var changedSignatory = !Equals(_obj.OurSignatory, employee);
      var signingReason = this.GetSuitableOurSigningReason(employee, certificate, changedSignatory);
      this.SetUnifiedRegistrationNumber(signingReason, signature, certificate);
    }
    
    /// <summary>
    /// Проверить блокировку электронной доверенности.
    /// </summary>
    /// <returns>True - заблокирована, иначе - false.</returns>
    [Public]
    public virtual bool FormalizedPowerOfAttorneyIsLocked()
    {
      if (Locks.GetLockInfo(_obj).IsLocked)
      {
        Logger.DebugFormat("ProcessPowerOfAttorneyQueueItem: Formalized power of attorney with id {0} is locked.", _obj.Id);
        return true;
      }
      return false;
    }
    
  }
}