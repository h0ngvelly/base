﻿using System;
using Sungero.Core;

namespace DirRX.TeamsCommonAPI.Constants
{
  public static class Module
  {
    public const string ProjectPlanGuid = "f2894e4d-a950-497f-a311-1fd7e9665d28";
    public const string AgileBoardsGuid = "c00e3e1f-f948-431b-b211-4924cb2ed84d";  
    public const int SizePersonalPhotoThumbnail = 36;
    public const int MaxRecepintCount = 25;
    
    public const string IncorrectGuidFormat = "IncorrectGuidFormat";
    public const string TypeByGuidNotFound = "TypeByGuidNotFound";
    public const string EntityNotFound = "EntityNotFound";
  }
}