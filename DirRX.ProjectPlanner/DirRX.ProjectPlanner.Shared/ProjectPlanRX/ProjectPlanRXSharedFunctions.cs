﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using DirRX.ProjectPlanner.ProjectPlanRX;

namespace DirRX.ProjectPlanner.Shared
{
  partial class ProjectPlanRXFunctions
  {
		/// <summary>
		/// Установить доступность свойств в зависимости от наличия этапов по проекту.
		/// </summary>
		public void SetPropertiesAvailability()
		{
			// TODO: используется серверная функция для получения списка проектов.
			var haveActivities = ProjectPlanner.PublicFunctions.ProjectActivity.Remote.GetActivities(_obj).Any();
			_obj.State.Properties.StartDate.IsEnabled = !haveActivities;
			_obj.State.Properties.EndDate.IsEnabled = !haveActivities;
			_obj.State.Properties.BaselineWork.IsEnabled = !haveActivities;
			_obj.State.Properties.BaselineWorkType.IsEnabled = !haveActivities;
			_obj.State.Properties.PlannedCosts.IsEnabled = false;
			_obj.State.Properties.FactualCosts.IsEnabled = false;
			
			foreach(var property in _obj.State.Properties.ResponsibleNotices.Properties)
      {
        property.IsEnabled = _obj.AccessRights.CanManageOrDelegate();
      }
      
      foreach(var property in _obj.State.Properties.PlanDateNotices.Properties)
      {
        property.IsEnabled = _obj.AccessRights.CanManageOrDelegate();
      }
      
      foreach(var property in _obj.State.Properties.OtherNotices.Properties)
      {
        property.IsEnabled = _obj.AccessRights.CanManageOrDelegate();
      }
      
      //Добавить условие наличия соответствующих ролей.
      var noticeAddittionalColumnsVisibility = _obj.ProjectId.HasValue && _obj.ProjectId.Value != 0;
      
      _obj.State.Properties.ResponsibleNotices.Properties.EventType.IsEnabled = false;
      _obj.State.Properties.ResponsibleNotices.Properties.ProjectAdmin.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.ResponsibleNotices.Properties.CustomerInternal.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.ResponsibleNotices.Properties.Participant.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.ResponsibleNotices.Properties.MgmntTeam.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.ResponsibleNotices.Properties.Observers.IsVisible = noticeAddittionalColumnsVisibility;
      
      _obj.State.Properties.PlanDateNotices.Properties.EventType.IsEnabled = false;
      _obj.State.Properties.PlanDateNotices.Properties.ProjectAdmin.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.PlanDateNotices.Properties.CustomerInternal.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.PlanDateNotices.Properties.Participant.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.PlanDateNotices.Properties.MgmntTeam.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.PlanDateNotices.Properties.Observers.IsVisible = noticeAddittionalColumnsVisibility;
      
      _obj.State.Properties.OtherNotices.Properties.EventType.IsEnabled = false;
      _obj.State.Properties.OtherNotices.Properties.ProjectAdmin.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.OtherNotices.Properties.CustomerInternal.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.OtherNotices.Properties.Participant.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.OtherNotices.Properties.MgmntTeam.IsVisible = noticeAddittionalColumnsVisibility;
      _obj.State.Properties.OtherNotices.Properties.Observers.IsVisible = noticeAddittionalColumnsVisibility;
		}
  }
}