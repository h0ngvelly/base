﻿using System;
using Sungero.Core;

namespace DirRX.ProjectPlanner.Constants
{
  public static class ProjectPlanRX
  {

    /// <summary>
    /// Формат документа MS Project.
    /// </summary>
    public const string MSProjectFormat = "xml";
    
    /// <summary>
    /// Наименование параметра "Сайт клиента"
    /// </summary>
    public const string WebSiteParam = "WebSite";
  }
}