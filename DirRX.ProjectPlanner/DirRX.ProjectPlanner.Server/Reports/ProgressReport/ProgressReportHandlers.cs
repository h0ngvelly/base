﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using DirRX.ProjectPlanning;

namespace DirRX.ProjectPlanner
{
  partial class ProgressReportServerHandlers
  {

    public override void AfterExecute(Sungero.Reporting.Server.AfterExecuteEventArgs e)
    {
      Sungero.Docflow.PublicFunctions.Module.DeleteReportData(Constants.ProgressReport.MilestonesTableName, ProgressReport.ReportSessionId);
      Sungero.Docflow.PublicFunctions.Module.DeleteReportData(Constants.ProgressReport.OverdueStagesTableName, ProgressReport.ReportSessionId);
      Sungero.Docflow.PublicFunctions.Module.DeleteReportData(Constants.ProgressReport.CompletedStagesTableName, ProgressReport.ReportSessionId);
      Sungero.Docflow.PublicFunctions.Module.DeleteReportData(Constants.ProgressReport.CurrentStagesTableName, ProgressReport.ReportSessionId);
    }

    public override void BeforeExecute(Sungero.Reporting.Server.BeforeExecuteEventArgs e)
    {
      IProjectDocument charter = null;
      var reportSessionId = System.Guid.NewGuid().ToString();
      var plan = ProgressReport.ProjectPlanRX;
      var activities = ProjectActivities.GetAll(x => x.ProjectPlan.Id == plan.Id);
      var project = DirRX.ProjectPlanner.Functions.Module.GetLinkedProject(plan);
      var startDatePlan = plan.StartDate;
      var endDatePlan = plan.EndDate;
      var startDate = ProgressReport.StartDate;
      var currentDate = ProgressReport.CurrentDate;
      var endDate = ProgressReport.EndDate;
      
      #region Заполнение параметров отчета
      ProgressReport.ReportSessionId = reportSessionId;
      ProgressReport.ProjectPlanRXLink = Hyperlinks.Get(plan);
      ProgressReport.IsHourCost = plan.BaselineWorkType == ProjectPlanRX.BaselineWorkType.Hours;
      ProgressReport.Workload = activities.Where(x => x.TypeActivity != ProjectActivity.TypeActivity.Section).Sum(x => x.BaselineWork) ?? 0;
      ProgressReport.BudgetCurrent = plan.FactualCosts;
      ProgressReport.BudgetApproved = activities.Where(x => x.TypeActivity != ProjectActivity.TypeActivity.Section && x.StartDate < currentDate.Value.Date)
                                                .Sum(x => x.PlannedCosts) ?? 0;
      ProgressReport.CountTable = Constants.ProgressReport.CountTableFirst;
      ProgressReport.DurationFact = Constants.ProgressReport.DurationFactFirst;
      ProgressReport.HasAgileProject = false;
      
      if(startDatePlan != null && endDatePlan != null)
      {
        ProgressReport.DurationApproved = Functions.Module.GetWorkiningDaysInPeriod(startDatePlan.Value, endDatePlan.Value);
      }
      
      if(plan.ActualStartDate.HasValue)
      {
        var actualFinishDate = plan.ActualFinishDate == null ? Calendar.Now: plan.ActualFinishDate.Value;
        ProgressReport.DurationFact = Functions.Module.GetWorkiningDaysInPeriod(plan.ActualStartDate.Value, actualFinishDate);
      }
      
      if(project != null)
      {
        ProgressReport.ProjectId = project.Id;
        var externalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(Sungero.Docflow.Server.DocumentKind.ClassTypeGuid,
                                                                                  Sungero.Projects.PublicConstants.Module.Initialize.RegulationsKind);
        
        if(externalLink != null)
        {
          var documentKindId = externalLink.EntityId;
          charter = ProjectDocuments.GetAll(x => x.Project == project && x.DocumentKind.Id == documentKindId).FirstOrDefault();
          
          if(charter != null)
          {
            ProgressReport.ProjectCharter = charter.Name;
            ProgressReport.ProjectCharterLink = Hyperlinks.Get(charter);
          }
        }
        
        if(project.Folder != null)
        {
          ProgressReport.ProjectFolder = project.Folder.Name;
          ProgressReport.ProjectFolderLink = Hyperlinks.Get(project.Folder);
        }
        
        if(ProgressReport.IsIncludeAgile == true)
        {
          using (var command = SQL.GetCurrentConnection().CreateCommand())
          {
            command.CommandText = Queries.ProgressReport.SelectBoardWithProject;
            SQL.AddParameter(command, "@projectId", project.Id, System.Data.DbType.Int32);
            var boardId = command.ExecuteScalar();
            ProgressReport.HasAgileProject = boardId != null;
          }
        }
      }
      #endregion
      
      #region Заполнение отчета по плану проекта
      var milestones = GetAllStagesWithType(ProjectActivity.TypeActivity.Milestone, plan).Distinct().ToList();
      
      var overdueStages = GetAllStagesWithType(ProjectActivity.TypeActivity.Task, plan)
        .Where(x => x.ExecutionPercent != 100 && x.EndDate.HasValue && x.EndDate.Value.AddDays(-1) < currentDate)
        .Distinct()
        .ToList();
      
      var completedStages = GetAllStagesWithType(ProjectActivity.TypeActivity.Task, plan)
        .Where(x => x.ExecutionPercent == 100 && x.EndDate.HasValue && x.EndDate.Value.AddDays(-1) >= startDate)
        .Distinct()
        .ToList();

      var currentStages = GetAllStagesWithType(ProjectActivity.TypeActivity.Task, plan)
        .Where(x => x.ExecutionPercent != 100 && x.EndDate.HasValue && x.EndDate.Value.AddDays(-1) > currentDate && x.StartDate <= endDate)
        .Distinct()
        .ToList();

      var allStagesReport = milestones.Union(overdueStages).Union(completedStages).Union(currentStages);
      var allStagesReportWithWbs = FillWbs(allStagesReport, ProgressReport.ProjectPlanRX);

      this.FillTableMilestones(milestones, allStagesReportWithWbs, reportSessionId);
      this.FillTableOverdueStages(overdueStages, allStagesReportWithWbs,  reportSessionId);
      this.FillTableCompletedStages(completedStages, allStagesReportWithWbs, reportSessionId);
      this.FillTableCurrentStages(currentStages, allStagesReportWithWbs, reportSessionId);
      #endregion
    }
    
    /// <summary>
    /// Заполнение вех плана.
    /// </summary>
    /// <param name="reportSessionId">ИД сессии.</param>
    private void FillTableMilestones(List<IProjectActivity> milestones, Dictionary<int, string> allStagesReportWithWbs, string reportSessionId)
    {
      var tableData = new List<Structures.ProgressReport.IMilestoneTableLine>();
      var startDate = ProgressReport.StartDate;
      var currentDate = ProgressReport.CurrentDate;
      var endDate = ProgressReport.EndDate;
      var plan = ProgressReport.ProjectPlanRX;
      var statusDone = Reports.Resources.ProgressReport.MilestoneStatusDone;
      var statusNotDone = Reports.Resources.ProgressReport.MilestoneStatusNotDone;
      
      var rowsMilestone = milestones.Select(m =>
                                            Structures.ProgressReport.MilestoneTableLine.Create(
                                              reportSessionId,
                                              m.Id,
                                              allStagesReportWithWbs[m.Id],
                                              m.Name,
                                              m.EndDate,
                                              m.ExecutionPercent == 100 ? statusDone : statusNotDone,
                                              m.Responsible?.Name,
                                              m.NumberVersion.Value)
                                           )
        .Cast<Structures.ProgressReport.IMilestoneTableLine>();
      
      tableData.AddRange(rowsMilestone);
      
      Sungero.Docflow.PublicFunctions.Module.WriteStructuresToTable(Constants.ProgressReport.MilestonesTableName, tableData);
    }
    
    /// <summary>
    /// Заполнение просроченных этапов.
    /// </summary>
    /// <param name="overdueStages">Просроченные этапы.</param>
    /// <param name="allStagesReportWithWbs">Этапы с ИСР.</param>
    /// <param name="reportSessionId">ИД сессии.</param>
    private void FillTableOverdueStages(List<IProjectActivity> overdueStages, Dictionary<int, string> allStagesReportWithWbs,  string reportSessionId)
    {
      var tableData = new List<Structures.ProgressReport.IStageTableLine>();
      
      var rowsOverdueStage = overdueStages.Select(stage =>
                                                  Structures.ProgressReport.StageTableLine.Create(
                                                    reportSessionId,
                                                    stage.Id,
                                                    allStagesReportWithWbs[stage.Id],
                                                    stage.Name,
                                                    stage.StartDate.Value,
                                                    stage.EndDate.Value.AddDays(-1),
                                                    stage.ExecutionPercent.Value,
                                                    stage.Responsible?.Name,
                                                    stage.NumberVersion.Value)
                                                 )
        .Cast<Structures.ProgressReport.IStageTableLine>();
      
      tableData.AddRange(rowsOverdueStage);
      
      Sungero.Docflow.PublicFunctions.Module.WriteStructuresToTable(Constants.ProgressReport.OverdueStagesTableName, tableData);
    }
    
    /// <summary>
    /// Заполнение выполненных эапов.
    /// </summary>
    /// <param name="completedStages">Выполненные этапы.</param>
    /// <param name="allStagesReportWithWbs">Этапы с ИСР.</param>
    /// <param name="reportSessionId">ИД сессии.</param>
    private void FillTableCompletedStages(List<IProjectActivity> completedStages, Dictionary<int, string> allStagesReportWithWbs, string reportSessionId)
    {
      var tableData = new List<Structures.ProgressReport.IStageTableLine>();
      
      var rowsCompletedStage = completedStages.Select(stage =>
                                                      Structures.ProgressReport.StageTableLine.Create(
                                                        reportSessionId,
                                                        stage.Id,
                                                        allStagesReportWithWbs[stage.Id],
                                                        stage.Name,
                                                        stage.StartDate.Value,
                                                        stage.EndDate.Value.AddDays(-1),
                                                        stage.ExecutionPercent.Value,
                                                        stage.Responsible?.Name,
                                                        stage.NumberVersion.Value)
                                                     )
        .Cast<Structures.ProgressReport.IStageTableLine>();
      
      tableData.AddRange(rowsCompletedStage);
      
      Sungero.Docflow.PublicFunctions.Module.WriteStructuresToTable(Constants.ProgressReport.CompletedStagesTableName, tableData);
    }
    
    /// <summary>
    /// Заполнение текущих и ближайших этапов.
    /// </summary>
    /// <param name="currentStages">Текущие и ближайшие этапы.</param>
    /// <param name="allStagesReportWithWbs">Этапы с ИСР.</param>
    /// <param name="reportSessionId">ИД сессии.</param>
    private void FillTableCurrentStages(List<IProjectActivity> currentStages, Dictionary<int, string> allStagesReportWithWbs, string reportSessionId)
    {
      var tableData = new List<Structures.ProgressReport.IStageTableLine>();
      
      var rowsCurrentStage = currentStages.Select(stage =>
                                                  Structures.ProgressReport.StageTableLine.Create(
                                                    reportSessionId,
                                                    stage.Id,
                                                    allStagesReportWithWbs[stage.Id],
                                                    stage.Name,
                                                    stage.StartDate.Value,
                                                    stage.EndDate.Value.AddDays(-1),
                                                    stage.ExecutionPercent.Value,
                                                    stage.Responsible?.Name,
                                                    stage.NumberVersion.Value)
                                                 )
        .Cast<Structures.ProgressReport.IStageTableLine>();
      
      tableData.AddRange(rowsCurrentStage);

      Sungero.Docflow.PublicFunctions.Module.WriteStructuresToTable(Constants.ProgressReport.CurrentStagesTableName, tableData);
    }
    
    /// <summary>
    /// Заполнение ИСР у этапов.
    /// </summary>
    /// <param name="activitiesInReport">Этапы.</param>
    /// <param name="plan">План.</param>
    /// <returns>Словарь ИД этапов с ИСР.</returns>
    private static Dictionary<int, string> FillWbs(IEnumerable<IProjectActivity> activitiesInReport, IProjectPlanRX plan)
    {
      Stack<int> currentIsr = new Stack<int>();
      var activitiesResult = new Dictionary<int, string>();
      IProjectActivity lastParent = null;
      var listParent = new List<IProjectActivity>();
      
      //если список этапов пустой, возвращаем пустой словарь
      if(!activitiesInReport.Any())
      {
        return activitiesResult;
      }
      
      var allActivities = ProjectActivities.GetAll(x => x.ProjectPlan.Id == plan.Id).OrderBy(x => x.SortIndex).Distinct().ToList();
      var firstActivity = allActivities.First();
      listParent.Add(firstActivity.LeadingActivity);
      currentIsr.Push(1);
      
      activitiesResult.Add(firstActivity.Id, CastWbs(currentIsr));
      
      for(int i = 1; i < allActivities.Count; i++)
      {

        if (listParent.Contains(allActivities[i].LeadingActivity))
        {
          var indexLastParent = listParent.IndexOf(lastParent);
          var indexCurrentParent = listParent.IndexOf(allActivities[i].LeadingActivity);
          
          for(int j = 0; j < indexLastParent - indexCurrentParent; j++)
          {
            currentIsr.Pop();
            listParent.Remove(listParent.Last());
          }
          
          lastParent = allActivities[i].LeadingActivity;
        }
        
        if (allActivities[i].LeadingActivity != lastParent && !listParent.Contains(allActivities[i].LeadingActivity))
        {
          lastParent = allActivities[i].LeadingActivity;
          listParent.Add(lastParent);
          currentIsr.Push(0);
        }

        currentIsr.Push(currentIsr.Pop() + 1);
        
        if(activitiesInReport.Contains(allActivities[i]))
        {
          activitiesResult.Add(allActivities[i].Id, CastWbs(currentIsr));
        }
      }
      
      return activitiesResult;
    }
    
    /// <summary>
    /// Привести ИСР в стэке к строке.
    /// </summary>
    /// <param name="s">Стек.</param>
    /// <returns>ИСР.</returns>
    private static string CastWbs(Stack<int> s)
    {
      return String.Join(".", s.Reverse());
    }
    
    /// <summary>
    /// Получить все активити в плане с определенным типом.
    /// </summary>
    /// <param name="type">Тип активити.</param>
    /// <param name="plan">План проекта.</param>
    /// <returns>Активити.</returns>
    private static IQueryable<IProjectActivity> GetAllStagesWithType(Enumeration type, IProjectPlanRX plan)
    {
      return ProjectActivities.GetAll(x => x.ProjectPlan == plan && x.TypeActivity == type);
    }

  }
}