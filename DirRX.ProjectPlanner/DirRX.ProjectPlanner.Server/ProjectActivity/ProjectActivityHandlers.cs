﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using DirRX.ProjectPlanner.ProjectActivity;

namespace DirRX.ProjectPlanner
{
  partial class ProjectActivityFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      return query.Where(q => q.ProjectPlan != null);
    }
  }


  partial class ProjectActivityServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      _obj.IsCreatedFromCopy = false;
    }

    public override void BeforeDelete(Sungero.Domain.BeforeDeleteEventArgs e)
    {
      var linkedTasks = TeamsCommonAPI.TeamsTasks.GetAll().ToList().Where(t => t.Attachments.Any(a => a.Id == _obj.Id));
      
      foreach (var task in linkedTasks)
      {
        TeamsCommonAPI.TeamsTasks.Delete(task);
      }
      
    }

    public override void AfterSave(Sungero.Domain.AfterSaveEventArgs e)
    {
      var plan = _obj.ProjectPlan;
      var responsible = _obj.Responsible;
      
      if(responsible != null && plan != null && plan.Project != null)
      {
          var asyncHandler = ProjectPlanner.AsyncHandlers.GrantRightsToProject.Create();
          asyncHandler.EmployeeId = responsible.Id;
          asyncHandler.ProjectId =  plan.Project.Id;
          asyncHandler.ExecuteAsync();   
      }
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      //Для того, что бы проинициализировать св-во, но не смешивать с другими активити.
      _obj.NumberVersion = -10;
    }
  }

}