﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using DirRX.ProjectPlanner.ProjectPlanRX;
using DirRX.TeamsCommonAPI.NotifyEventType;
using DirRX.TeamsCommonAPI.TeamsNoticesSettings;
using DirRX.TeamsCommonAPI;
using DirRX.TeamsCommonAPI.TeamsNoticesSettingsRecipients;
using DirRX.TeamsCommonAPI.NotifyConditionItem;

namespace DirRX.ProjectPlanner
{
  partial class ProjectPlanRXPlanDateNoticesObserversPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesObserversFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXPlanDateNoticesMgmntTeamPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesMgmntTeamFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXPlanDateNoticesParticipantPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesParticipantFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXPlanDateNoticesCustomerInternalPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesCustomerInternalFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXPlanDateNoticesProjectAdminPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesProjectAdminFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXPlanDateNoticesActivityRepPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesActivityRepFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXPlanDateNoticesRepSectionPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesRepSectionFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXPlanDateNoticesProjectLeadPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PlanDateNoticesProjectLeadFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => 
                         (_obj.EventType.EventType != EventType.ActOverdated 
                          && _obj.EventType.EventType != EventType.MilestOverdated 
                          && _obj.EventType.EventType != EventType.PlanOverdated 
                          && _obj.EventType.EventType != EventType.SectOverdated
                          && _obj.EventType.EventType != EventType.ActCannotStart
                         ) || q.Condition != Condition.Early
                        );
    }
  }

  partial class ProjectPlanRXOtherNoticesObserversPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesObserversFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXOtherNoticesMgmntTeamPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesMgmntTeamFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXOtherNoticesParticipantPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesParticipantFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXOtherNoticesCustomerInternalPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesCustomerInternalFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXOtherNoticesProjectAdminPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesProjectAdminFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXOtherNoticesActivityRepPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesActivityRepFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXOtherNoticesRepSectionPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesRepSectionFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXOtherNoticesProjectLeadPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> OtherNoticesProjectLeadFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesObserversPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesObserversFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesMgmntTeamPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesMgmntTeamFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesParticipantPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesParticipantFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesCustomerInternalPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesCustomerInternalFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesProjectAdminPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesProjectAdminFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesActivityRepPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesActivityRepFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesRepSectionPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesRepSectionFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }

  partial class ProjectPlanRXResponsibleNoticesProjectLeadPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesProjectLeadFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.Condition != Condition.Early);
    }
  }


  partial class ProjectPlanRXResponsibleNoticesEventTypePropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ResponsibleNoticesEventTypeFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(q => q.SectionIdentifier == DirRX.TeamsCommonAPI.NotifyEventType.SolutionIdentifier.ProjectPlanning);
    }
  }

  partial class ProjectPlanRXDocumentKindPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> DocumentKindFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(x => x.DocumentType.DocumentTypeGuid == Server.ProjectPlanRX.ClassTypeGuid.ToString());
    }
  }

  partial class ProjectPlanRXFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      if (_filter == null)
        return query;
      
      // Фильтр по состоянию.
      if (_filter.Active || _filter.Closed || _filter.Closing || _filter.Initiation)
        query = query.Where(x => (_filter.Active && x.Stage == Stage.Execution) ||
                            (_filter.Closed && x.Stage == Stage.Completed) ||
                            (_filter.Closing && x.Stage == Stage.Completion) ||
                            (_filter.Initiation && x.Stage == Stage.Initiation));

      var today = Calendar.UserToday;
      
      // Фильтр по дате начала проекта.
      var startDateBeginPeriod = _filter.StartDateRangeFrom ?? Calendar.SqlMinValue;
      var startDateEndPeriod = _filter.StartDateRangeTo ?? Calendar.SqlMaxValue;
      
      if (_filter.StartPeriodThisMonth)
      {
        startDateBeginPeriod = today.BeginningOfMonth();
        startDateEndPeriod = today.EndOfMonth();
      }
      
      if (_filter.StartPeriodThisMonth || (_filter.StartDateRangeFrom != null || _filter.StartDateRangeTo != null))
        query = query.Where(x => (x.StartDate.Between(startDateBeginPeriod, startDateEndPeriod) && !Equals(x.Stage, Stage.Completed)) ||
                            (x.ActualStartDate.Between(startDateBeginPeriod, startDateEndPeriod) && Equals(x.Stage, Stage.Completed)));

      // Фильтр по дате окончания проекта.
      var finishDateBeginPeriod = _filter.FinishDateRangeFrom ?? Calendar.SqlMinValue;
      var finishDateEndPeriod = _filter.FinishDateRangeTo ?? Calendar.SqlMaxValue;
      
      if (_filter.FinishPeriodThisMonth)
      {
        finishDateBeginPeriod = today.BeginningOfMonth();
        finishDateEndPeriod = today.EndOfMonth();
      }
      
      if (_filter.FinishPeriodThisMonth || (_filter.FinishDateRangeFrom != null || _filter.FinishDateRangeTo != null))
        query = query.Where(x => (x.EndDate.Between(finishDateBeginPeriod, finishDateEndPeriod) && !Equals(x.Stage, Stage.Completed)) ||
                            (x.ActualFinishDate.Between(finishDateBeginPeriod, finishDateEndPeriod) && Equals(x.Stage, Stage.Completed)));
      
      return query;
    }
  }

  partial class ProjectPlanRXCreatingFromServerHandler
  {

    public override void CreatingFrom(Sungero.Domain.CreatingFromEventArgs e)
    {
      e.Without(_info.Properties.ActualStartDate);
      e.Without(_info.Properties.ActualFinishDate);
      e.Without(_info.Properties.ExecutionPercent);
      e.Without(_info.Properties.Note);
      e.Params.Add("IsCopy", true);
      e.Params.Add("CopiedSourceEntityId", _source.Id);
    }
  }

  partial class ProjectPlanRXServerHandlers
  {

    public override void BeforeDelete(Sungero.Domain.BeforeDeleteEventArgs e)
    {
      base.BeforeDelete(e);
    }

    public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
    {
      base.BeforeSaveHistory(e);
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.Stage = Stage.Initiation;
      
      _obj.Modified = Calendar.Now;
      
      _obj.BaselineWorkType = ProjectPlanRX.BaselineWorkType.Hours;
      
      _obj.StartDate = Calendar.Now;
      
      var defaultOrFirstActiveDocKind = Sungero.Docflow.DocumentKinds
        .GetAll(x => x.DocumentType.DocumentTypeGuid == Server.ProjectPlanRX.ClassTypeGuid.ToString() &&
          x.Status == Sungero.Docflow.DocumentKind.Status.Active)
        .OrderByDescending(x => x.IsDefault.HasValue && x.IsDefault.Value)
        .FirstOrDefault();
      
      if (defaultOrFirstActiveDocKind == null)
      {
        throw new ArgumentException(DirRX.ProjectPlanner.ProjectPlanRXes.Resources.CannotFindDocKind);
      }
      
      _obj.DocumentKind = defaultOrFirstActiveDocKind;
      
      var isCopy = false;
      e.Params.TryGetValue("IsCopy", out isCopy);
      _obj.IsCopy = isCopy;
      
      if (isCopy)
      {
        var entityId = 0;
        e.Params.TryGetValue("CopiedSourceEntityId", out entityId);
        
        if (entityId != 0)
        {
          var sourcePlan = ProjectPlanRXes.Get(entityId);
          foreach (var setting in sourcePlan.PlanDateNotices.Select(s => s.LinkedSetting).Concat(sourcePlan.ResponsibleNotices.Select(s => s.LinkedSetting)).Concat(sourcePlan.OtherNotices.Select(s => s.LinkedSetting)))
          {
            var newSetting = TeamsNoticesSettingses.Copy(setting);
            newSetting.ConnectedEntityId = _obj.Id;
            newSetting.Save();
            
          }
        }
      }
      else
      {
        Functions.ProjectPlanRX.RecreateDefaultSettingsWithoutSave(_obj);
      }
      
      _obj.EnableReviewAssignmentFlag = true;
    }

    public override void Deleting(Sungero.Domain.DeletingEventArgs e)
    {
      var linkedProject = DirRX.ProjectPlanning.Projects.GetAll(x => ProjectPlanRXes.Equals(_obj, x.ProjectPlanDirRX)).FirstOrDefault();
      
      var recipient = linkedProject != null ? linkedProject.Administrator : _obj.Author;
      // Изъять права на этапы проекта.
      ProjectPlanner.ProjectActivities.AccessRights.RevokeAll(recipient);
      ProjectPlanner.ProjectActivities.AccessRights.Save();
      _obj.ResponsibleNotices.Clear();
    }

    public override void AfterSave(Sungero.Domain.AfterSaveEventArgs e)
    {
      if (!e.Params.Contains(Constants.Module.DontUpdateModified) && e.Params.Contains(Sungero.Docflow.PublicConstants.OfficialDocument.GrantAccessRightsToProjectDocument))
      {
        Sungero.Projects.Jobs.GrantAccessRightsToProjectDocuments.Enqueue();
        e.Params.Remove(Sungero.Docflow.PublicConstants.OfficialDocument.GrantAccessRightsToProjectDocument);
      }
      
      if (!e.Params.Contains(Constants.Module.DontUpdateModified))
        Sungero.Projects.Jobs.GrantAccessRightsToProjectFolders.Enqueue();
      
      
      var linkedProject = DirRX.ProjectPlanning.Projects.GetAll(x => ProjectPlanRXes.Equals(_obj, x.ProjectPlanDirRX)).FirstOrDefault();
      
      if (linkedProject != null)
      {
        if (!linkedProject.Folder.Items.Contains(_obj))
          linkedProject.Folder.Items.Add(_obj);
      }
      
      if(_obj.ProjectId.HasValue)
      {
        var project = DirRX.ProjectPlanning.Projects.Get(_obj.ProjectId.Value);
        if (project != null)
        {
          if (!ProjectPlanRXes.Equals(project.ProjectPlanDirRX, _obj))
            project.ProjectPlanDirRX = _obj;
        }
      }
      
      var diffs = NotifyDiffs.GetAll(d => d.ConnectedPlanId == _obj.Id).ToList();
      
      foreach (var diff in diffs.Where(d => d != null))
      {
        DirRX.TeamsCommonAPI.ITeamsNoticesSettings setting = null;
        
        var respSetting = _obj.ResponsibleNotices.Where(s => s != null && s.EventType != null & s.EventType.Id == diff.EventTypeId).FirstOrDefault();
        
        if (respSetting != null)
        {
          setting = respSetting.LinkedSetting;
        }
        else
        {
          var planSetting = _obj.PlanDateNotices.Where(s => s != null && s.EventType != null & s.EventType.Id == diff.EventTypeId).FirstOrDefault();
          
          if (planSetting != null)
          {
            setting = planSetting.LinkedSetting;
          }
          else
          {
            var otherSetting = _obj.OtherNotices.Where(s => s != null && s.EventType != null & s.EventType.Id == diff.EventTypeId).FirstOrDefault();
            
            if (otherSetting != null)
            {
              setting = otherSetting.LinkedSetting;
            }
          }
        }
        
        if (setting != null)
        {
          var connectedActivity = ProjectActivities.GetAll(a => a.Id == diff.ConnectedActId).FirstOrDefault();
          var connectedPlan = ProjectPlanRXes.GetAll(p => p.Id == diff.ConnectedPlanId).FirstOrDefault();
          setting = ConfigureRecipients(setting, connectedActivity, connectedPlan);
          
          DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting, connectedPlan, connectedActivity, diff));
        }
        NotifyDiffs.Delete(diff);
      }
    }
    
    public DirRX.TeamsCommonAPI.ITeamsNoticesSettings ConfigureRecipients(
      DirRX.TeamsCommonAPI.ITeamsNoticesSettings settings,
      DirRX.ProjectPlanner.IProjectActivity activity = null,
      DirRX.ProjectPlanner.IProjectPlanRX plan = null)
    {
      foreach (var recipient in settings.Recipients)
      {
        var resultList = new List<int>();
        
        if (recipient.RecipientType == RecipientType.Author && plan?.Author != null)
        {
          resultList.Add(plan.Author.Id);
          recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.ProjectLead && plan != null && plan.ProjectId.HasValue)
        {
          var connectedProject = DirRX.ProjectPlanning.Projects.GetAll(p => p.Id == plan.ProjectId.Value).FirstOrDefault();
          if (connectedProject != null && connectedProject.Manager != null)
          {
            resultList.Add(connectedProject.Manager.Id);
            recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
          }
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.ProjectAdmin && plan != null && plan.ProjectId.HasValue)
        {
          var connectedProject = DirRX.ProjectPlanning.Projects.GetAll(p => p.Id == plan.ProjectId.Value).FirstOrDefault();
          if (connectedProject != null && connectedProject.Administrator != null)
          {
            resultList.Add(connectedProject.Administrator.Id);
            recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
          }
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.Participant && plan != null && plan.ProjectId != null)
        {
          var connectedProject = DirRX.ProjectPlanning.Projects.GetAll(p => p.Id == plan.ProjectId.Value).FirstOrDefault();
          if (connectedProject != null)
          {
            recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(connectedProject.TeamMembers.Where(m => m.Group == Sungero.Projects.ProjectTeamMembers.Group.Change).Select(m => m.Member.Id).ToList());
          }
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.Observers && plan != null && plan.ProjectId != null)
        {
          var connectedProject = DirRX.ProjectPlanning.Projects.GetAll(p => p.Id == plan.ProjectId.Value).FirstOrDefault();
          if (connectedProject != null)
          {
            recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(connectedProject.TeamMembers.Where(m => m.Group == Sungero.Projects.ProjectTeamMembers.Group.Read).Select(m => m.Member.Id).ToList());
          }
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.MgmntTeam && plan != null && plan.ProjectId != null)
        {
          var connectedProject = DirRX.ProjectPlanning.Projects.GetAll(p => p.Id == plan.ProjectId.Value).FirstOrDefault();
          if (connectedProject != null)
          {
            recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(connectedProject.TeamMembers.Where(m => m.Group == Sungero.Projects.ProjectTeamMembers.Group.Management).Select(m => m.Member.Id).ToList());
          }
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.CustomerInterna && plan != null && plan.ProjectId != null)
        {
          var connectedProject = DirRX.ProjectPlanning.Projects.GetAll(p => p.Id == plan.ProjectId.Value).FirstOrDefault();
          if (connectedProject != null && connectedProject.InternalCustomer != null)
          {
            resultList.Add(connectedProject.InternalCustomer.Id);
            recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
          }
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.ActivityRep && activity != null && activity.Responsible != null)
        {
          resultList.Add(activity.Responsible.Id);
          recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
          continue;
        }
        
        if (recipient.RecipientType == RecipientType.RepSection && activity != null && activity.Responsible != null)
        {
          if (activity.LeadingActivity != null)
          {
            var currentActivity = activity.LeadingActivity;
            //циклически пробегаемся по всем ведущим этапам, ищем первую вложенную секцию (раздел).
            while (currentActivity.TypeActivity != DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Section)
            {
              //если ведущий этап не найден - следующие условия не отработают, и ответственному за этап уведомление не придёт.
              if (currentActivity.LeadingActivity == null)
              {
                break;
              }
              currentActivity = currentActivity.LeadingActivity;
            }
            
            if (currentActivity.TypeActivity == DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Task && currentActivity.Responsible != null)
            {
              resultList.Add(currentActivity.Responsible.Id);
              recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
            }
          }
          else
          {
            resultList.Add(activity.Responsible.Id);
            recipient.Recipient = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
          }
          
          continue;
        }
          
      }
      
      settings.Save();
      return settings;
    }
    
    
    private RNDNoticesUtils.Structures.NoticeItem GetNoticeItem(DirRX.TeamsCommonAPI.ITeamsNoticesSettings settingItem, object projectPlan = null, object projectActivity = null, DirRX.ProjectPlanner.INotifyDiff diff = null)
    {
      var setting = DirRX.TeamsCommonAPI.TeamsNoticesSettingses.As(settingItem);
      var attachments = new List<Sungero.Domain.Entity>();
      
      Sungero.Domain.Entity mainEntity = null;
      
      string firstTextParam = null;
      string secondaryTextParam = null;
      
      if (projectActivity != null)
      {
        attachments.Add((Sungero.Domain.Entity) projectActivity);
        
        if (mainEntity == null)
        {
          mainEntity = (Sungero.Domain.Entity)projectActivity;
        }
        
        firstTextParam = Hyperlinks.Get((Sungero.Domain.Entity)projectActivity);
      }
      
      if (projectPlan != null)
      {
        attachments.Add((Sungero.Domain.Entity) projectPlan);
        mainEntity = (Sungero.Domain.Entity)projectPlan;
        
        if (firstTextParam == null)
        {
          firstTextParam = Hyperlinks.Get((Sungero.Domain.Entity)projectPlan);
        }
        else
        {
          secondaryTextParam = Hyperlinks.Get((Sungero.Domain.Entity)projectPlan);
        }
      }
      
      var clientWebsite = DirRX.ProjectPlanner.PublicFunctions.Module.Remote.GetWebSite();
      
      var noticeItem =  new RNDNoticesUtils.Structures.NoticeItem(setting, attachments, clientWebsite, mainEntity, firstTextParam, secondaryTextParam);
      
      noticeItem.Diffs.Add(new RNDNoticesUtils.Structures.NoticeDiffItem() 
                           {
                             newValue = diff.NewValue,
                             oldValue = diff.PreviousValue
                           });
      
      noticeItem.TaskDeadLine = Calendar.Today;
      noticeItem.SolutionGuid = "f2894e4d-a950-497f-a311-1fd7e9665d28";
      
      return noticeItem;
    }
    
    private void UpdateLinkedProject(DirRX.ProjectPlanning.IProject linkedProject)
    {
      if (linkedProject == null)
      {
        return;
      }
      
      if (!linkedProject.AccessRights.CanUpdate())
      {
        Logger.Error(DirRX.ProjectPlanner.ProjectPlanRXes.Resources.NotEnoughRightsToUpdateProjectFormat(linkedProject.Name));
        return;
      }
      
      if (Locks.GetLockInfo(linkedProject).IsLockedByOther)
      {
        Logger.Error(DirRX.ProjectPlanner.ProjectPlanRXes.Resources.ProjectIsLockedByOtherFormat(linkedProject.Name));
        return;
      }
      
      if(_obj.State.Properties.StartDate.IsChanged && _obj.StartDate != null)
      {
        linkedProject.StartDate = _obj.StartDate;
      }
      
      if(_obj.State.Properties.EndDate.IsChanged && _obj.EndDate != null)
      {
        linkedProject.EndDate = _obj.EndDate;
      }
      
      if(_obj.State.Properties.ActualStartDate.IsChanged && _obj.ActualStartDate != null)
      {
        linkedProject.ActualStartDate = _obj.ActualStartDate;
      }
      
      if(_obj.State.Properties.ActualFinishDate.IsChanged && _obj.ActualFinishDate != null)
      {
        linkedProject.ActualFinishDate = _obj.ActualFinishDate;
      }
    }

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      if (!_obj.AccessRights.CanUpdate())
      {
        e.AddError(ProjectPlanRXes.Resources.NoRightToUpdateProject);
        return;
      }
      
      //TODO Urmanov Нагруженная часть, можно отказаться при отказе от связи с проектом
      var linkedProject = Functions.Module.GetLinkedProject(_obj);
      
      UpdateLinkedProject(linkedProject);
      
      
      // TODO Zamerov: сравнивать надо с ресурсом в локали тенанта. BUG: 35010
      if (Equals(_obj.Name, Sungero.Projects.Resources.ProjectArhiveFolderName))
        e.AddError(ProjectPlanRXes.Resources.PropertyReservedFormat(_obj.Info.Properties.Name.LocalizedName, Sungero.Projects.Resources.ProjectArhiveFolderName));

      if (!e.Params.Contains(Constants.Module.DontUpdateModified))
        _obj.Modified = Calendar.Now; 
    }
  }
  
  partial class ProjectPlanRXTeamMembersMemberPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> TeamMembersMemberFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return query.Where(x => x.Sid != Sungero.Domain.Shared.SystemRoleSid.Administrators &&
                         x.Sid != Sungero.Domain.Shared.SystemRoleSid.Auditors &&
                         x.Sid != Sungero.Domain.Shared.SystemRoleSid.ConfigurationManagers &&
                         x.Sid != Sungero.Domain.Shared.SystemRoleSid.ServiceUsers &&
                         x.Sid != Sungero.Domain.Shared.SystemRoleSid.SoloUsers &&
                         x.Sid != Sungero.Domain.Shared.SystemRoleSid.DeliveryUsersSid);
    }
  }

}