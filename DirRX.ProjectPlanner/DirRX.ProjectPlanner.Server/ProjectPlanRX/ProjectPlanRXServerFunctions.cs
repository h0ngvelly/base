﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using DirRX.ProjectPlanner.ProjectPlanRX;
using DirRX.TeamsCommonAPI.NotifyEventType;
using DirRX.TeamsCommonAPI.TeamsNoticesSettings;
using DirRX.TeamsCommonAPI;

namespace DirRX.ProjectPlanner.Server
{
  partial class ProjectPlanRXFunctions
  {	
    
    /// <summary>
    /// Удаляет старые настройки уведомлений плана проекта и создает новые с дефолтными значениями,
    /// без сохранения сущности.
    /// </summary>
    /// <param name="projectPlan">План проекта.</param>
    [Remote]
    public static void RecreateDefaultSettingsWithoutSave(IProjectPlanRX projectPlan)
    {
      if (!projectPlan.AccessRights.CanUpdate())
      {
        return;
      }
      
      AccessRights.AllowRead(() =>
         {
           projectPlan.OtherNotices.Clear();
           projectPlan.PlanDateNotices.Clear();
           projectPlan.ResponsibleNotices.Clear();
           
           DirRX.ProjectPlanner.Functions.Module.CreateDefaultNoticeSettings(projectPlan.Id);
           
           var settings = DirRX.TeamsCommonAPI.TeamsNoticesSettingses.GetAll(s => s.ConnectedEntityId == projectPlan.Id && s.SolutionIdentifier == DirRX.TeamsCommonAPI.TeamsNoticesSettings.SolutionIdentifier.ProjectPlanner);
           
           var respSettings = settings.Where(s => s.EventType.SectionIdentifier == SectionIdentifier.RespAssign);
           var planSettings = settings.Where(s => s.EventType.SectionIdentifier == SectionIdentifier.PlanDate);
           var otherSettings = settings.Where(s => s.EventType.SectionIdentifier == SectionIdentifier.Other);
           
           foreach (var newSetting in respSettings)
           {
             var newSettingRef = projectPlan.ResponsibleNotices.AddNew();
             newSettingRef.LinkedSetting = newSetting;
           }
           
           foreach (var newSetting in planSettings)
           {
             var newSettingRef = projectPlan.PlanDateNotices.AddNew();
             newSettingRef.LinkedSetting = newSetting;
           }
           
           foreach (var newSetting in otherSettings)
           {
             var newSettingRef = projectPlan.OtherNotices.AddNew();
             newSettingRef.LinkedSetting = newSetting;
           }
           
           projectPlan.EnableReviewAssignmentFlag = true;
         }
      );
    }
		
		[Remote]
		public static IProjectPlanRX GetProjectPlan(int projectId)
		{
			return DirRX.ProjectPlanner.ProjectPlanRXes.Get(projectId);
		}
		
		/// <summary>
		/// Возвращает информацию о блокировке проекта.
		/// </summary>
		/// <returns>Информация о блокировке проекта.</returns>
		/// <remarks>
		/// Если проект заблокирован, то возвращается информация о блокировке.
		/// Если же не заблокирован, то пустая строка.
		/// </remarks>
		[Remote]
		public string GetLockInfo()
		{
			var lockInfo = Locks.GetLockInfo(_obj);
			return lockInfo.IsLockedByOther && !string.IsNullOrEmpty(lockInfo.LockedMessage) ? lockInfo.LockedMessage.ToString() : string.Empty;
		}
		
		/// <summary>
		/// Возвращает список проектов c этапами.
		/// </summary>
		/// <returns>Список проектов с этапами.</returns>
		[Remote]
		public static List<IProjectPlanRX> GetProjectsWithActivities()
		{
			var result = new List<IProjectPlanRX>();
			foreach (var item in ProjectPlanRXes.GetAll())
			{
				if (ProjectPlanner.PublicFunctions.ProjectActivity.Remote.GetActivities(item).Any())
					result.Add(item);
			}
			return result;
		}
		
		/// <summary>
		/// Копирует этапы из одного проекта в другой.
		/// </summary>
		/// <param name="sourceProject">Исходный проект.</param>
		/// <param name="targetProject">Проект, в который копируются этапы.</param>
		/// <param name="newDate">Новая дата начала.</param>
		[Remote]
		public static void CopyActivitiesFormSourceProjectToTargetProject(IProjectPlanRX sourceProject, IProjectPlanRX targetProject, DateTime newDate)
		{
			// Разница времени.
			TimeSpan diff = newDate - sourceProject.StartDate.Value;
			// Соответствие между этапами исходного проекта и этапами проекта, в который копируются этапы.
			var idActivities = new List<Structures.ProjectPlanRX.IDActivities>();
			// Скопировать этапы.
			var sourceActivities = ProjectPlanner.PublicFunctions.ProjectActivity.Remote.GetActivities(sourceProject);
			// Список скопированных этапов.
			var targetActivities = new List<ProjectPlanner.IProjectActivity>();
			foreach (var activity in sourceActivities)
			{
				var newActivity = ProjectPlanner.ProjectActivities.Copy(activity);
				// Кеш соответствия ID.
				idActivities.Add(Structures.ProjectPlanRX.IDActivities.Create(activity.Id, newActivity));
				newActivity.StartDate = activity.StartDate.Value + diff;
				newActivity.EndDate = activity.EndDate.Value + diff;
				newActivity.ProjectPlan = targetProject;
				// Очистить ведущий этап и проценты выполнения.
				newActivity.LeadingActivity = null;
				newActivity.ExecutionPercent = null;
				// Очистить предшественников.
				newActivity.Predecessors.Clear();
				newActivity.Save();
				targetActivities.Add(newActivity);
			}
			foreach (var sourceActivity in sourceActivities)
			{
				var targetActivity = idActivities.First(x => x.IDSource == sourceActivity.Id).TargetActitvity;
				var isChanged = false;
				// Заполнить ведущий этап для скопированных этапов.
				if (sourceActivity.LeadingActivity != null)
				{
					targetActivity.LeadingActivity = idActivities.First(y => y.IDSource == sourceActivity.LeadingActivity.Id).TargetActitvity;
					isChanged = true;
				}
				// Заполнить предшественников для скопированных этапов.
				foreach (var predecessor in sourceActivity.Predecessors)
				{
					var newPredecessor = targetActivity.Predecessors.AddNew();
					newPredecessor.Activity = idActivities.First(y => y.IDSource == predecessor.Activity.Id).TargetActitvity;
					isChanged = true;
				}
				if (isChanged)
					targetActivity.Save();
			}
		}
		
		/// <summary>
		/// Создать проект.
		/// </summary>
		/// <returns>Проект.</returns>
		[Public, Remote]
		public static IProjectPlanRX CreateProject()
		{
			return ProjectPlanRXes.Create();
		}
		
		/// <summary>
		/// Создать план проекта.
		/// </summary>
		/// <returns>План проекта.</returns>
		[Public, Remote]
		public static IProjectPlanRX CreateProjectPlan()
		{
			return DirRX.ProjectPlanner.ProjectPlanRXes.Create();
		}
	}
}