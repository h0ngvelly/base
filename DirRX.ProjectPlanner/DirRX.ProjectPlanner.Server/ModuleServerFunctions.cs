﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.Domain;
using Sungero.Domain.Shared;
using Sungero.Company;
using Sungero.CoreEntities;
using DirRX.ProjectPlanner;
using DirRX.ProjectPlanning;
using DirRX.TeamsCommonAPI;
using DirRX.TeamsCommonAPI.NotifyConditionItem;
using DirRX.TeamsCommonAPI.NotifyEventType;
using DirRX.TeamsCommonAPI.TeamsNoticesSettingsRecipients;

namespace DirRX.ProjectPlanner.Server
{
  public class ModuleFunctions
  {
    
    /// <summary>
    /// Получить ссылку на тикет в отчете.
    /// </summary>
    /// <param name="boardId">Ид доски.</param>
    /// <param name="ticketId">Ид тикета.</param>
    /// <returns>Ссылка на тикет.</returns>
    [Public]
    public string GetLinkTicket(int boardId, int ticketId)
    {
      string result = null;
      
      try
      {
        var getter = new ConfigWrapperV1_0_0.WebClientAddressGetter();
        var uriAgile = getter.GetContentFullAddress(new Uri(Constants.Module.AgileClientAddress, UriKind.Relative));
        var builder = new UriBuilder(uriAgile)
        {
          Query = string.Format("boardId={0}&ticketId={1}", boardId, ticketId)
        };
        
        result = builder.Uri.ToString();
      }
      catch(System.UriFormatException ex)
      {
        Logger.Error(Resources.ErrorUriTicketFormat(ticketId, boardId), ex);
      }
      
      return result;
    }
    
    /// <summary>
    /// Получить ссылку на этап в отчете.
    /// </summary>
    /// <param name="planId">Ид плана.</param>
    /// <param name="activityId">Ид этапа.</param>
    /// <param name="activityId">Номер версии плана.</param>
    /// <returns>Ссылка на этап.</returns>
    [Public]
    public string GetLinkActivity(int planId, int activityId, int numberVersion)
    {
      string result = null;
      
      try
      {
        var webSite = Functions.Module.GetWebSite();
        var getter = new ConfigWrapperV1_0_0.WebClientAddressGetter();
        var uriProjectPlan = getter.GetContentFullAddress(new Uri(Constants.Module.DefaultClientAddress, UriKind.Relative));
        var builder = new UriBuilder(uriProjectPlan)
        {
          Query = string.Format("projectId={0}&readonly={1}&numberVersion={2}&activityId={3}", planId, true, numberVersion, activityId)
        };
        
        result = builder.Uri.ToString();
      }
      catch(System.UriFormatException ex)
      {
        Logger.Error(Resources.ErrorUriActivityFormat(activityId, planId), ex);
      }
      
      return result;
    }
    

    private static void AddNewRecipientTypeToSettings(ITeamsNoticesSettings rule, Enumeration recipientType, INotifyConditionItem notifyCondition)
    {
       var newRecipient = rule.Recipients.AddNew();
       newRecipient.RecipientType = recipientType;
       newRecipient.NotifyCondition = notifyCondition;
    }
    
    /// <summary>
    /// Создать правило уведомлений по-умолчанию.
    /// </summary>
    /// <param name="eventType">Тип уведомления.</param>
    /// <param name="name">Отображаемое имя уведомления.</param>
    /// <param name="projectLead">Тип уведомления для руководителя проекта/плана.</param>
    /// <param name="repSection">Тип уведомления для ответственного за раздел.</param>
    /// <param name="activityRep">Тип уведомления для ответственного за этап/веху.</param>
    /// <param name="projectAdmin">Тип уведомления для админа проекта.</param>
    /// <param name="customerInternal">Тип уведомления для внутреннего заказчика.</param>
    /// <param name="participant">Тип уведомления для участников проекта.</param>
    /// <param name="mgmntTeam">Тип уведомления для команды управления проекта.</param>
    /// <param name="observers">Тип уведомления для наблюдателей.</param>
    private static void SetDefaultRule(int entityId, INotifyEventType type, string name, INotifyConditionItem projectLead, INotifyConditionItem repSection, INotifyConditionItem activityRep, INotifyConditionItem projectAdmin, INotifyConditionItem customerInternal, INotifyConditionItem participant, INotifyConditionItem mgmntTeam, INotifyConditionItem observers)
    {
      ITeamsNoticesSettings settings = null;
      
      var existingRule = TeamsNoticesSettingses.GetAll(s => s.ConnectedEntityId == entityId && s.EventType == type).FirstOrDefault();
      
      if (existingRule != null)
      {
        settings = existingRule;
      }
      else
      {
        settings = TeamsNoticesSettingses.Create();
      }
      
      settings.Recipients.Clear();
      
      settings.Name = name;
     
      AddNewRecipientTypeToSettings(settings, RecipientType.ProjectLead, projectLead);
      AddNewRecipientTypeToSettings(settings, RecipientType.RepSection, repSection);
      AddNewRecipientTypeToSettings(settings, RecipientType.ActivityRep, activityRep);
      AddNewRecipientTypeToSettings(settings, RecipientType.ProjectAdmin, projectAdmin);
      AddNewRecipientTypeToSettings(settings, RecipientType.CustomerInterna, customerInternal);
      AddNewRecipientTypeToSettings(settings, RecipientType.Participant, participant);
      AddNewRecipientTypeToSettings(settings, RecipientType.MgmntTeam, mgmntTeam);
      AddNewRecipientTypeToSettings(settings, RecipientType.Observers, observers);
      settings.EventType = type;
      settings.ConnectedEntityId = entityId;
      settings.SolutionIdentifier = DirRX.TeamsCommonAPI.TeamsNoticesSettings.SolutionIdentifier.ProjectPlanner;
      settings.Save();
    }
    
    public static void CreateDefaultNoticeSettings(int entityId)
    {
      var existsSettings = TeamsNoticesSettingses.GetAll(s => s.ConnectedEntityId == entityId).ToList();
      
      var notifyConditions = NotifyConditionItems.GetAll().ToList();
      var never = notifyConditions.Where(c => c.Condition == Condition.Never).FirstOrDefault();
      var standart = notifyConditions.Where(c => c.Condition == Condition.StandartNotice).FirstOrDefault();
      var task = notifyConditions.Where(c => c.Condition == Condition.Task).FirstOrDefault();
      var early = notifyConditions.Where(c => c.Condition == Condition.Early).FirstOrDefault();
      var collective = notifyConditions.Where(c => c.Condition == Condition.Consolidated).FirstOrDefault();

      
      //Перевызов не страшен, так как идут проверки, что условия еще не созданы
      //Однако этот вызов гарантирует, что все нужные справочники будут созданы при нарушении порядка инициализации
      DirRX.TeamsCommonAPI.PublicInitializationFunctions.Module.CreateDefaultEventTypes();
      var eventTypes = NotifyEventTypes.GetAll().ToList();
      
      #region Уведомления о правах доступа и зонах ответственности
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.PlanRespAssign),
        DirRX.ProjectPlanner.Resources.PplanRespAssigned,
        projectLead:collective,
        repSection:never,
        activityRep:never,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.SectRespAssign),
        DirRX.ProjectPlanner.Resources.PplanSectionRespAssigned,
        projectLead:never,
        repSection:collective,
        activityRep:never,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.ActRespAssign),
        DirRX.ProjectPlanner.Resources.PplanActivityRespAssigned,
        projectLead:never,
        repSection:never,
        activityRep:collective,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.MilesRespAssign),
        DirRX.ProjectPlanner.Resources.PplanMilestoneRespAssigned,
        projectLead:never,
        repSection:never,
        activityRep:collective,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.PlanParticipAdd),
        DirRX.ProjectPlanner.Resources.PlanParticipAdd,
        projectLead:never,
        repSection:never,
        activityRep:never,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      #endregion
      #region Уведомления по плану проекта
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.PlanMustStarted),
        DirRX.ProjectPlanner.Resources.PlanMustStarted,
        projectLead:collective,
        repSection:never,
        activityRep:never,
        projectAdmin:collective,
        customerInternal:collective,
        participant:never,
        mgmntTeam:collective,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.PlanMustEnded),
        DirRX.ProjectPlanner.Resources.PlanMustEnded,
        projectLead:collective,
        repSection:never,
        activityRep:never,
        projectAdmin:collective,
        customerInternal:collective,
        participant:never,
        mgmntTeam:collective,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.PlanOverdated),
        DirRX.ProjectPlanner.Resources.PlanOverdated,
        projectLead:standart,
        repSection:never,
        activityRep:never,
        projectAdmin:standart,
        customerInternal:standart,
        participant:never,
        mgmntTeam:standart,
        observers:never
       );
      
      #endregion
      #region Уведомления по разделам плана проекта
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.SectMustStarted),
        DirRX.ProjectPlanner.Resources.SectMustStarted,
        projectLead:collective,
        repSection:collective,
        activityRep:never,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.SectMustEnded),
        DirRX.ProjectPlanner.Resources.SectMustEnded,
        projectLead:collective,
        repSection:collective,
        activityRep:never,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.SectOverdated),
        DirRX.ProjectPlanner.Resources.SectOverdated,
        projectLead:standart,
        repSection:standart,
        activityRep:never,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      #endregion
      #region Уведомления по этапам и вехам плана проекта
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.ActMustStarted),
        "Этап плана проекта должен быть начат",
        projectLead:collective,
        repSection:collective,
        activityRep:collective,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.ActCannotStart),
        DirRX.ProjectPlanner.Resources.ActCannotStart,
        projectLead:collective,
        repSection:collective,
        activityRep:collective,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.ActMustEnded),
        DirRX.ProjectPlanner.Resources.ActMustEnded,
        projectLead:early,
        repSection:early,
        activityRep:early,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.ActOverdated),
        DirRX.ProjectPlanner.Resources.ActOverdated,
        projectLead:standart,
        repSection:standart,
        activityRep:standart,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.MilestMustReach),
        DirRX.ProjectPlanner.Resources.MilestMustReach,
        projectLead:early,
        repSection:early,
        activityRep:early,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.MilestOverdated),
        DirRX.ProjectPlanner.Resources.MilestOverdated,
        projectLead:standart,
        repSection:standart,
        activityRep:standart,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.ActStatusChangd),
        DirRX.ProjectPlanner.Resources.ActStatusChangd,
        projectLead:collective,
        repSection:collective,
        activityRep:never,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      SetDefaultRule(
        entityId,
        eventTypes.FirstOrDefault(t => t.EventType == EventType.StartedActChd),
        DirRX.ProjectPlanner.Resources.StartedActChd,
        projectLead:never,
        repSection:collective,
        activityRep:collective,
        projectAdmin:never,
        customerInternal:never,
        participant:never,
        mgmntTeam:never,
        observers:never
       );
      
      #endregion
    }
    

    public void UpdateLeadActivity(DirRX.ProjectPlanner.Structures.Module.IActivityDto sourceActivity,
      List<DirRX.ProjectPlanner.Structures.Module.ModelActivity> updatedActivityIds,
      int lastActivityId)
    {
      if (sourceActivity.LeadActivityId.HasValue && sourceActivity.LeadActivityId.Value > lastActivityId)
      {
        var leadActivityId = updatedActivityIds.FirstOrDefault(x => x.ModelActivitylId == sourceActivity.LeadActivityId.Value);
        if (leadActivityId == null)
        {
          throw new Exception(DirRX.ProjectPlanner.Resources.FailToFindUpdatedIdExceptionTextFormat(sourceActivity.LeadActivityId.Value));
        }
        
        sourceActivity.LeadActivityId = new Nullable<int>(leadActivityId.ID);
      }
    }
    
    /// <summary>
    /// Проверяет успешное прохождение инициализации.
    /// </summary>
    /// <returns>Ссылка на веб-клиент.</returns>
    /// <exception cref="Exception">Если проверка не пройдена.</exception>
    [Public(WebApiRequestType = RequestType.Get)]
    public string Check()
    {
      try
      {
        this.ThrowExceptionIfResourceLinksTableNotExist();
        
        return DirRX.ProjectPlanner.PublicFunctions.Module.Remote.GetWebSite();
      }
      catch (Exception ex)
      {
        throw new Exception(Resources.HealthCheckExceptionText, ex);
      }
    }
    
    private void ThrowExceptionIfResourceLinksTableNotExist()
    {
      using (var command = SQL.GetCurrentConnection().CreateCommand())
      {
        command.CommandText = Queries.Module.CheckResourceLinksTable;
        if ((int)command.ExecuteScalar() == 0)
        {
          throw new Exception(Resources.ResourceLinksTableNotFoundExceptionText);
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    [Public(WebApiRequestType = RequestType.Post)]
    public static Structures.Module.IProjectPlanDto UpdatePlanData(int projectPlanId, int numberVersion)
    {
      try
      {
        var projectPlanDto = new Structures.Module.ProjectPlanDto();
        
        AccessRights.AllowRead(() =>
                               {
                                 projectPlanDto.ResourceData = HandleUpdateResourcesDataDto(projectPlanId, numberVersion);
                                 projectPlanDto.BaselineWorkType = GetPlanBaselineWorkType(projectPlanId);
                               });
        
        projectPlanDto.Tasks = GetRxTasks(projectPlanId, numberVersion);
        projectPlanDto.AccessDeniedTasks = GetAccessDeniedTasks(projectPlanId, numberVersion);
        return projectPlanDto;
      }
      catch (Exception ex)
      {
        throw new Exception(DirRX.ProjectPlanner.Resources.UpdatePlanDataExceptionFormat(projectPlanId, numberVersion), ex);
      }
    }
    
    /// <summary>
    /// Проверить существует ли целевая версия в плане.
    /// </summary>
    /// <returns></returns>
    [Public(WebApiRequestType = RequestType.Get)]
    public Structures.Module.ICheckPlanExistsResult CheckPlanExists(int projectPlanId, int numberVersion)
    {
      try
      {
        var projectPlan = ProjectPlanRXes.GetAll(x => x.Id == projectPlanId).FirstOrDefault();
        var planVersionExist = projectPlan != null && projectPlan.Versions.Any(x => x.Number.HasValue ? x.Number.Value == numberVersion : false);
        return new Structures.Module.CheckPlanExistsResult {IsExists = planVersionExist};
      }
      catch (Exception ex)
      {
        throw new Exception(DirRX.ProjectPlanner.Resources.CheckPlanExistsExeptionFormat(projectPlanId, numberVersion), ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    [Public(WebApiRequestType = RequestType.Post)]
    public Structures.Module.ICapacityResponseDto GetCapacityDto(List<int> resourceIds, double startDate, double endDate, int planId, int planVersion)
    {
      try
      {
        var startDateFromTimeStamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(startDate);
        var endDateFromTimeStamp = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(endDate);
        return new Structures.Module.CapacityResponseDto()
        {
          Capacity = GetCapacity(resourceIds, startDateFromTimeStamp, endDateFromTimeStamp, planId, planVersion),
          WorkingTimeCalendar = GetWorkingTimeCalendars(resourceIds, startDateFromTimeStamp, endDateFromTimeStamp)
        };
      }
      catch (Exception ex)
      {
        throw new Exception(DirRX.ProjectPlanner.Resources.GetCapacityExceptionFormat(resourceIds, startDate, endDate, ex));
      }
    }
    
    /// <summary>
    /// Найти или создать ресурс сотрудника.
    /// </summary>
    /// <param name="employeeId"></param>
    /// <returns></returns>
    [Public(WebApiRequestType = RequestType.Get)]
    public Structures.ProjectsResource.IProjectResourceDto GetOrCreateResource(int employeeId)
    {
      try
      {
        return this.HandleGetOrCreateResource(employeeId);
      }
      catch (Exception ex)
      {
        throw new Exception(DirRX.ProjectPlanner.Resources.ExecutionRequestExeptionFormat(ex, employeeId));
      }
    }

    /// <summary>
    /// 
    /// </summary>
    private static void AddEmployeeResources(List<int> projectPlanResourceIds, Dictionary<int, Structures.Module.IUser> users, Dictionary<int, Structures.Module.IResource> resources)
    {
      var employeeResources = ProjectsResources.GetAll(x => x.Type.ServiceName == Constants.Module.ResourceTypes.Users && projectPlanResourceIds.Contains(x.Id));
      foreach (var employeeResource in employeeResources)
      {
        if (!users.ContainsKey(employeeResource.Employee.Id))
        {
          users.Add(
          employeeResource.Employee.Id,
          new Structures.Module.User()
                  {
                    Id = employeeResource.Employee.Id,
                    Name = employeeResource.Employee.Person?.Name ?? employeeResource.Employee.Name
                  }
         );
        }
        
        if (!resources.ContainsKey(employeeResource.Id))
        {
          resources.Add(
          employeeResource.Id,
          new Structures.Module.Resource()
                      {
                        Id = employeeResource.Id,
                        EntityTypeId = employeeResource.Type.Id,
                        EntityId = employeeResource.Employee.Id,
                        UnitLabel = employeeResource.Type.MeasureUnit
                      }
         );
        }
      }
      
    }
    
    /// <summary>
    /// 
    /// </summary>
    private static void FillUsersFromResponsibles(List<DirRX.ProjectPlanner.IProjectActivity> activities, Dictionary<int, Structures.Module.IUser> users)
    {
      foreach(var activity in activities.Where(a => a.Responsible != null))
      {
        if (users.ContainsKey(activity.Responsible.Id))
        {
          continue;
        }
        
        users.Add(
          activity.Responsible.Id,
          new Structures.Module.User
                  {
                    Id = activity.Responsible.Id,
                    Name = activity.Responsible.Person.Name
                  }
         );
      }
    }
    
    /// <summary>
    /// 
    /// </summary>
    private static List<Structures.Module.IMaterialResource> GetMaterialResources()
    {
      //HACK В данной версии материальные ресурсы не используются.
      return new List<Structures.Module.IMaterialResource>();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// Может ли быть пустой???
    private static List<Structures.Module.IResourceTypes> GetResourcesTypes()
    {
      var resourceTypes = new List<Structures.Module.IResourceTypes>();
      foreach(var type in ProjectResourceTypes.GetAll())
      {
        resourceTypes.Add(new Structures.Module.ResourceTypes()
                          {
                            Id = type.Id,
                            Name = type.Name,
                            SectionName = type.ServiceName
                          });
      }
      return resourceTypes;
    }
    
    /// <summary>
    /// 
    /// </summary>
    private static Structures.Module.IUser TryGetProjectManager(DirRX.ProjectPlanner.IProjectPlanRX project)
    {
      var linkedProject = DirRX.ProjectPlanning.Projects.GetAll(x => ProjectPlanRXes.Equals(project, x.ProjectPlanDirRX)).FirstOrDefault();
      if (linkedProject != null && linkedProject.Manager != null)
      {
        return new Structures.Module.User()
        {
          Id = linkedProject.Manager.Id,
          Name = linkedProject.Manager.Person.Name
        };
      }
      return null;
    }
    
    private Structures.ProjectsResource.IProjectResourceDto HandleGetOrCreateResource(int employeeId)
    {
      //Если ресурс есть, то просто возвращаем его
      var resourceByEmployeeId = DirRX.ProjectPlanner.ProjectsResources.GetAll(x => x.Employee != null && x.Employee.Id == employeeId).FirstOrDefault();
      if (resourceByEmployeeId != null)
      {
        return new Structures.ProjectsResource.ProjectResourceDto()
        {
          ResourceId = resourceByEmployeeId.Id,
          ResourceName = resourceByEmployeeId.Name
        };
      }
      //Если ресурса нет, то создаем новый
      //получаем сотрудника
      var employee = Employees.GetAll(x => x.Id == employeeId).FirstOrDefault();
      if (employee == null){
        throw new Exception(DirRX.ProjectPlanner.Resources.EmployeeIdNotFoundExeptionFormat(employeeId));
      }
      //получаем тип ресурса
      var userResourceType = DirRX.ProjectPlanner.ProjectResourceTypes.GetAll(x => x.ServiceName == Constants.Module.ResourceTypes.Users).FirstOrDefault();
      if (userResourceType == null)
      {
        throw new Exception(DirRX.ProjectPlanner.Resources.NullReferenceResourceTypeExceptionFormat(Constants.Module.ResourceTypes.Users));
      }
      //Создаем ресурс
      var newResource = DirRX.ProjectPlanner.ProjectsResources.Create();
      newResource.Type = userResourceType;
      newResource.Employee = employee;
      newResource.Name = newResource.Employee.Name;
      newResource.Save();
      //Возвращаем новый ресурс
      return new Structures.ProjectsResource.ProjectResourceDto()
      {
        ResourceId = newResource.Id,
        ResourceName = newResource.Name
      };
    }
    
    private static List<int> GetPlanOldTasksQuery(int activityId)
    {
      var oldTaskIds = new List<int>();
      using (var command = SQL.GetCurrentConnection().CreateCommand())
      {
        try
        {
          Logger.Debug("Execute get activity old tasks query");
          command.CommandText = string.Format(Queries.Module.GetPlanOldTasks, activityId);
          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              try 
              {
                oldTaskIds.Add((int)reader[0]);
              }
              catch (Exception ex) 
              {
                Logger.ErrorFormat("Не найдена задача с ID = {0}, Exception: {1} Trace: {2}", reader[0], ex.ToString(), ex.StackTrace.ToString());
              }
            }
          }
        }
        catch (Exception ex) 
        {
          Logger.ErrorFormat("Ошибка выполнения запроса. Exception: {0}, Trace: {1}", ex.ToString(), ex.StackTrace.ToString());
        }
      }
      return oldTaskIds;
    }
    
    /// <summary>
    /// Получить количество недоступных задач по каждому активити.
    /// </summary>
    /// <param name="projectPlanId">Ид плана.</param>
    /// <param name="numVersion">Номер версии плана.</param>
    /// <returns>Количество недоступных задач по каждому активити.</returns>
    private static List<Structures.Module.IAccessDeniedTasks> GetAccessDeniedTasks(int projectPlanId, int numVersion)
    {
      var accessDeniedTasksByActivity = new List<Structures.Module.IAccessDeniedTasks>();
      IQueryable<IProjectActivityTask> allActivityTasks = null;
      var projectActivities = ProjectActivities.GetAll(x => x.NumberVersion.Value == numVersion && x.ProjectPlan.Id == projectPlanId);
      var activityTasks = ProjectActivityTasks.GetAll(t => t.ProjectActivity != null && projectActivities.Select(pa => pa.Id).Contains(t.ProjectActivity.Id));
      
      AccessRights.AllowRead(() =>
                             {
                               allActivityTasks = ProjectActivityTasks.GetAll(t => t.ProjectActivity != null &&
                                                                              projectActivities.Select(pa => pa.Id).Contains(t.ProjectActivity.Id)
                                                                             );
                             });
      
      // nhibernate не дает сгруппировать. Приходится материализовывать
      var accessDeniedTasks = allActivityTasks.Where(x => !activityTasks.Contains(x)).ToList();
      accessDeniedTasksByActivity = accessDeniedTasks.GroupBy(x => x.ProjectActivity.Id)
                                                       .Select(x => Structures.Module.AccessDeniedTasks.Create(x.Key, x.Count()))
                                                       .ToList();

      return accessDeniedTasksByActivity;
    }
    
    /// <summary>
    /// 
    /// </summary>
    private static List<Structures.Module.ITask> GetRxTasks(int projectPlanId, int numVersion)
    {
      var rxTasks = new List<Structures.Module.ITask>();
      var projectActivities = ProjectActivities.GetAll(x => x.NumberVersion.Value == numVersion && x.ProjectPlan.Id == projectPlanId);
      var activityTasks = ProjectActivityTasks.GetAll(t => t.ProjectActivity != null && projectActivities.Select(pa => pa.Id).Contains(t.ProjectActivity.Id));
      
      foreach (var task in activityTasks)
      {
        if (!task.MaxDeadline.HasValue)
        {
          throw new Exception(DirRX.ProjectPlanner.Resources.TaskMaxDeadLineExeptionFormat(task.Id));
        }
        // Zheleznov_AV HACK клиент сейчас не умеет обрабатывать отмененные задачи.
        // Надо синхронизировать модель статусов на киленте и на сервере.
        if (task.Status == Sungero.Workflow.Task.Status.Aborted)
        {
          continue;
        }
        var taskStatus = "Unfinished";
        if (task.Status == Sungero.Workflow.Task.Status.Completed)
        {
          taskStatus = "Submitted";
        }
        rxTasks.Add(new Structures.Module.Task()
                    {
                      ActivityId = task.ProjectActivity.Id,
                      Deadline = task.MaxDeadline.Value,
                      HyperLink = Hyperlinks.Get(task),
                      Id = task.Id,
                      TaskStatus = taskStatus,
                      DisplayValue = task.DisplayValue
                    });
      }
      
      return rxTasks;
    }
    
    /// <summary>
    /// Получение открытых из карточки планов.
    /// </summary>
    [Remote]
    public static List<IOpensProjectPlansFromCard> GetOpensProjectPlansFromCard(IProjectPlanRX pp, DirRX.ProjectPlanning.IProject linkedProject)
    {
      return OpensProjectPlansFromCards.GetAll(x => ProjectPlanRXes.Equals(x.PrjectPlan, pp) || (linkedProject != null && DirRX.ProjectPlanning.Projects.Equals(x.Project, linkedProject))).ToList();
    }
    
    /// <summary>
    /// Удаление всех этапов для указанной версии плана.
    /// </summary>
    /// <param name="projectPlanId">Id плана проекта</param>
    /// <param name="numVersion">Номер версии плана</param>
    [Remote]
    public static void DeleteProjectActiviesByNumberVersion(int projectPlanId, int numVersion)
    {
      var prActs = ProjectActivities.GetAll(x => x.NumberVersion.Value == numVersion && x.ProjectPlan.Id == projectPlanId).ToList();
      var activitiesForBatchSaving = new List<DirRX.ProjectPlanner.IProjectActivity>(prActs.Count);
      foreach (var act in prActs)
      {
        if (act.Predecessors != null)
          act.Predecessors.Clear();
        act.LeadingActivity = null;
        activitiesForBatchSaving.Add(act);
      }
      
      BatchSave(activitiesForBatchSaving);
      
      foreach (var act in prActs)
      {
        ProjectActivities.Delete(act);
      }
    }
    
    /// <summary>
    /// Удаление всех этапов плана проекта.
    /// </summary>
    [Remote]
    public static void DeleteProjectActivity(IProjectPlanRX projectPlan)
    {
      var linkedProject = Functions.Module.GetLinkedProject(projectPlan);
      
      if (!DirRX.ProjectPlanning.ProjectDocuments.GetAll(x => x.Project.Id == (linkedProject != null ? linkedProject.Id : 0)).Any() || linkedProject == null)
      {
        var prActs = ProjectActivities.GetAll(x => x.ProjectPlan.Id == projectPlan.Id).ToList();
        
        foreach (var act in prActs)
        {
          if (act.Predecessors != null)
            act.Predecessors.Clear();
          act.LeadingActivity = null;
          act.Save();
        }
        
        foreach (var act in prActs)
        {
          ProjectActivities.Delete(act);
        }
      }
    }
    
    /// <summary>
    /// Создание копии версии плана проекта.
    /// </summary>
    /// <param name="projectPlan">План проекта</param>
    /// <param name="sourceNumVersion">Номер исходной версии</param>
    [Remote]
    public static void CreateCopyVersion(IProjectPlanRX projectPlan, int sourceNumVersion)
    {
      var newVersion = projectPlan.Versions.AddNew();
      newVersion.AssociatedApplication = Sungero.Content.AssociatedApplications.GetAll(x => x.Extension == "rxpp").First();
      projectPlan.Save();
      
      var model = string.Empty;
      
      using (var reader = new System.IO.StreamReader(projectPlan.Versions.First(x => x.Number.Value == sourceNumVersion).Body.Read()))
      {
        model = reader.ReadToEnd();
      }
      
      SaveModelFromModelString(model, projectPlan, projectPlan.LastVersion.Number.Value, true, false);
    }
    
    /// <summary>
    /// Создание копии проекта
    /// </summary>
    [Remote]
    public static void CreateCopyProject(IProjectPlanRX projectPlan, int numVersion)
    {
      var model = string.Empty;
      
      using (var reader = new System.IO.StreamReader(projectPlan.Versions.First(x => x.Number.Value == numVersion).Body.Read()))
      {
        model = reader.ReadToEnd();
      }
      
      SaveModelFromModelString(model, projectPlan, projectPlan.LastVersion.Number.Value, false, true);
    }
    
    [Public(WebApiRequestType = RequestType.Post), Remote]
    public static void WriteJsonBodyToProjectVersion(int projectPlanId, int numVersion, bool writeToPublicBody)
    {
      var projectPlan = ProjectPlanRXes.Get(projectPlanId);
      WriteJsonBodyToProjectVersion(projectPlan, numVersion, writeToPublicBody);
    }
    
    /// <summary>
    /// Записать тело Json-модели в свойство проекта "ModelBody".
    /// </summary>
    /// <param name="projectPlan">Ссылка на план проекта.</param>
    /// <param name="numVersion">Порядковый номер версии.</param>
    /// <param name="writeToPublicBody">Записать в Read-only тело.</param>
    [Public, Remote]
    public static void WriteJsonBodyToProjectVersion(IProjectPlanRX projectPlan, int numVersion, bool writeToPublicBody)
    {
      if (projectPlan != null)
      {
        try
        {
          string jsonBody = GetModel(projectPlan, numVersion);
          
          using (var stream = new System.IO.MemoryStream())
          {
            var bytes = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(jsonBody);
            stream.Write(bytes, 0, bytes.Length);
            
            if (numVersion == 0)
            {
              if (projectPlan.HasVersions)
              {
                var lastVersion = projectPlan.LastVersion;
                if (writeToPublicBody)
                {
                  lastVersion.PublicBody.Write(stream);
                }
                else
                {
                  lastVersion.Body.Write(stream);
                }
              }
              else
              {
                projectPlan.CreateVersionFrom(stream, "rxpp");
              }
              
            }
            else
            {
              var version = projectPlan.Versions.FirstOrDefault(x => x.Number.Value == numVersion);
              if (version != null)
              {
                if (writeToPublicBody)
                {
                  version.PublicBody.Write(stream);
                }
                else
                {
                  version.Body.Write(stream);
                }
              }
              ///HACK Urmanov_AR: Фикс ошибки с ресурсами при создании плана из файла.
              else
              {
                projectPlan.CreateVersionFrom(stream, "rxpp");
              }
            }
            projectPlan.IsCopy = false;
            projectPlan.Save();
            //HACK несмотря на using в коробке принято явно закрывать стрим
            stream.Close();
          }
          
        }
        catch(Exception ex)
        {
          Logger.DebugFormat("При сохранении плана проекта ID {0} произошла ошибка. {1}", projectPlan.Id, ex.Message+ex.StackTrace);
        }
      }
    }
    
    /// <summary>
    /// Запросить модель из сервиса хранилищ по ссылке.
    /// </summary>
    /// <param name="uriModel">Ссылка</param>
    [Public, Remote]
    public static string GetJsonStringByUri(string uriModel)
    {
      var client = new System.Net.WebClient();
      var bytesModel = client.DownloadData(uriModel);
      var stringModel = System.Text.Encoding.UTF8.GetString(bytesModel);
      
      return stringModel;
    }
    
    /// <summary>
    /// Создаёт активити из модели.
    /// </summary>
    /// <param name="activityModel">Модель активити.</param>
    /// <param name="projectPlan">План проекта.</param>
    /// <param name="numVersion">Номер версии.</param>
    /// <returns></returns>
    private static ProjectPlanner.IProjectActivity CreateActivityFromModelString(DirRX.Planner.Model.Activity activityModel, IProjectPlanRX projectPlan, DirRX.ProjectPlanning.IProject linkedProject, Nullable<int> numVersion)
    {
      var newActivity = ProjectPlanner.ProjectActivities.Create();

      newActivity.Name = activityModel.Name;
      if (string.IsNullOrWhiteSpace(newActivity.Name))
      {
        return null;
      }
      
      newActivity.IsCreatedFromCopy = true;
      newActivity.Number = activityModel.CurrentNumber;
      newActivity.StartDate = activityModel.StartDate;
      newActivity.EndDate = activityModel.EndDate;
      if (!newActivity.StartDate.HasValue || !newActivity.EndDate.HasValue)
      {
        return null;
      }
      newActivity.ProjectPlan = projectPlan;
      
      newActivity.Duration = ProjectPlanner.Functions.Module.GetWorkiningDaysInPeriod(newActivity.StartDate.Value, newActivity.EndDate.Value).ToString();
      newActivity.BaselineWork = activityModel.BaselineWork;
      newActivity.ExecutionPercent = activityModel.ExecutionPercent;
      newActivity.Note = activityModel.Note;
      newActivity.SortIndex = activityModel.SortIndex;
      newActivity.Priority = activityModel.Priority;
      newActivity.FactualCosts = activityModel.FactualCosts;
      newActivity.PlannedCosts = activityModel.PlannedCosts;
      newActivity.NumberVersion = numVersion;
          
      if (activityModel.Status != null)
      {
        var status = newActivity.StatusAllowedItems.Where(s => activityModel.Status.EnumValue == s.Value).FirstOrDefault();
          
        if (status != null)
        {
          newActivity.Status = status;
        }
      }
      
      newActivity.TypeActivity = DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Task;
      
      var type = newActivity.TypeActivityAllowedItems.Where(t => t != null && t.Value == activityModel.TypeActivity).FirstOrDefault();
      
      if (type != null && type.Value != null)
      {
        newActivity.TypeActivity = type;
      }
      
      if (activityModel.ResponsibleId.HasValue)
      {
        var responsible = Sungero.Company.Employees.GetAll(x => x.Id == activityModel.ResponsibleId.Value).FirstOrDefault();
      
        if (responsible == null)
        {
          Logger.Debug(DirRX.ProjectPlanner.Resources.EmployeeForActivityNotFoundFormat(activityModel.ResponsibleId.Value, activityModel.Name));
        }
        else
        {
          newActivity.Responsible = responsible;
        
          if (!projectPlan.TeamMembers.Any(x => Recipients.Equals(x.Member, newActivity.Responsible)))
          {
            var newMember = projectPlan.TeamMembers.AddNew();
            newMember.Member = newActivity.Responsible;
            newMember.Group = DirRX.ProjectPlanner.ProjectPlanRXTeamMembers.Group.Change;
          }
          
          if (linkedProject != null)
          {
            if (!linkedProject.TeamMembers.Any(x => x.Member.Id == newActivity.Responsible.Id))
            {
              var newMember = linkedProject.TeamMembers.AddNew();
              newMember.Member = newActivity.Responsible;
              newMember.Group = DirRX.ProjectPlanner.ProjectPlanRXTeamMembers.Group.Change;
            }
          }
        }
      }
      
      return newActivity;
    }
    
    public static void UpdatePredeccessorsFromModel(DirRX.Planner.Model.Model model, List<Structures.Module.ModelActivity> modelActivitiesList)
    {
      foreach (var activityApp in model.Activities.Where(x => x.Predecessors != null))
      {
        var oldActivityModel = modelActivitiesList.FirstOrDefault(a => a.ModelActivitylId == activityApp.Id);
        if (oldActivityModel == null)
        {
          continue;
        }
        
        var activity = ProjectPlanner.ProjectActivities.Get(oldActivityModel.ID);
        foreach (var itemAct in activityApp.Predecessors)
        {
          var idActivityPredec = modelActivitiesList.FirstOrDefault(a => a.ModelActivitylId == itemAct.Id);
          if (idActivityPredec == null)
          {
            continue;
          }
          
          var predecessor = ProjectPlanner.ProjectActivities.Get(idActivityPredec.ID);
          var item  = activity.Predecessors.AddNew();
          item.Activity = predecessor;
          item.LinkType = itemAct.LinkType;
          item.Lag = itemAct.Lag;
        }
        activity.Save();
      }
    }
    
    private static void SetLeadActivities(List<Structures.Module.ModelActivity> modelActivitiesList, List<Structures.Module.LeadActivities> leadActivities)
    {
      foreach (var leadActivityStruct in leadActivities)
      {
        var leadActivity = modelActivitiesList.FirstOrDefault(a => a.ModelActivitylId == leadActivityStruct.LeadActivityId);
        if (leadActivity == null)
        {
          continue;
        }
        
        leadActivityStruct.Activity.LeadingActivity = ProjectPlanner.ProjectActivities.Get(leadActivity.ID);
        leadActivityStruct.Activity.Save();
      }
    }
    
    public static System.Func<System.Threading.Tasks.Task> SaveActivities(
      System.Collections.Generic.List<DirRX.Planner.Model.Activity> activities,
      int numVersion,
      DirRX.ProjectPlanning.IProject linkedProject,
      IProjectPlanRX projectPlan,
      List<Structures.Module.LeadActivities> leadActivities,
      bool isCopyVersion,
      List<int> activityResourcesForDeleting,
      System.Collections.Generic.Dictionary<DirRX.Planner.Model.Activity, int> activityModelDict,
      List<Structures.Module.ModelActivity> modelActivitiesList)
    {
      var activitiesForBatchSaving = new List<DirRX.ProjectPlanner.IProjectActivity>(activities.Count);
      
      foreach (var activityModel in activities)
        {
          var newActivity = CreateActivityFromModelString(activityModel, projectPlan, linkedProject, numVersion);

          if (newActivity == null)
          {
            continue;
          }
          modelActivitiesList.Add(Structures.Module.ModelActivity.Create(activityModel.Id.Value, newActivity.Id));
          
          if (activityModel.LeadActivityId.HasValue)
          {
            leadActivities.Add(Structures.Module.LeadActivities.Create(newActivity, activityModel.LeadActivityId.Value));
          }
          
          RefreshCapacityInfo(activityModel.Resources, newActivity);
          
          activitiesForBatchSaving.Add(newActivity);
          
          if (isCopyVersion)
          {
            activityResourcesForDeleting.Add(activityModel.Id.Value);
          }
          else
          {
            activityResourcesForDeleting.Add(newActivity.Id);
          }
          
          activityModelDict.Add(activityModel, newActivity.Id);
        }
        
        BatchSave(activitiesForBatchSaving.Where(a => a.State.IsChanged || a.State.IsInserted));
        return null;
    }
    
    /// <summary>
    /// Разбор json модели плана проекта.
    /// </summary>
    [Public, Remote]
    public static List<string> SaveModelFromModelString(string modelJson, IProjectPlanRX projectPlan, int numVersion, bool isCopyVersion, bool isCopyProject)
    {
      using (var connection = CreateDBConnection())
      {
        if (!isCopyVersion)
          DeleteProjectActiviesByNumberVersion(projectPlan.Id, numVersion);

        var model = Newtonsoft.Json.JsonConvert.DeserializeObject<DirRX.Planner.Model.Model>(modelJson);
        
        var leadActivities = new List<Structures.Module.LeadActivities>();
        var modelActivitiesList = new List<Structures.Module.ModelActivity>();
        var notFoundEmployeeIds = new List<string>();
        
        var activityResourcesForDeleting = new List<int>();
        var activityModelDict = new Dictionary<DirRX.Planner.Model.Activity, int>();
        var linkedProject = DirRX.ProjectPlanner.Functions.Module.GetLinkedProject(projectPlan);
  
        Sungero.Domain.ParallelHelper.TaskRun(
              SaveActivities(
                model.Activities,
                numVersion,
                linkedProject,
                projectPlan,
                leadActivities,
                isCopyVersion,
                activityResourcesForDeleting,
                activityModelDict,
                modelActivitiesList)
             );
        
        RemoveActivityResources(activityResourcesForDeleting, connection);
        
        foreach (var activityModel in model.Activities)
        {
          var newactivityId = activityModelDict[activityModel];
          SaveResources(activityModel.Resources, newactivityId, activityModel.StartDate.Value, activityModel.EndDate.Value, connection);
        }
        
        UpdatePredeccessorsFromModel(model, modelActivitiesList);
        
        SetLeadActivities(modelActivitiesList, leadActivities);
        
        WriteJsonBodyToProjectVersion(projectPlan, numVersion, false);
        
        return notFoundEmployeeIds;
      }
    }
    
    private static void InsertResourceLink(int activityId, int resourceId, double busy, System.Data.IDbConnection connection)
    {
      using (var command = connection.CreateCommand())
        {
          command.CommandText = "insert into ResourceLinks values(@project_activity_id, @resource_id, @average_busy)";
          SQL.AddParameter(command, "@project_activity_id", activityId, System.Data.DbType.Int32);
          SQL.AddParameter(command, "@resource_id", resourceId, System.Data.DbType.Int32);
          SQL.AddParameter(command, "@average_busy", busy, System.Data.DbType.Double);
          command.ExecuteScalar();
        }
    }
    
    [Public]
    public static System.Data.IDbConnection CreateDBConnectionPublic()
    {
      return CreateDBConnection();
    }
    
    [Public]
    public static void SaveResources(int resourceId, int activityId, int busy, System.Data.IDbConnection connection)
    {
      InsertResourceLink(activityId, resourceId, busy, connection);
    }
    
    /// <summary>
    /// Сохранение изменений в ресурсах проекта.
    /// </summary>
    public static void SaveResources(List<Planner.Model.ResourcesWorkload> resourcesWorkload, int activityId, DateTime startDate, DateTime endDate, System.Data.IDbConnection connection)
    {
      foreach(var resourceWorkload in resourcesWorkload)
      {
        var resource = ProjectsResources.GetAll(x => x.Id == resourceWorkload.ResourceId).SingleOrDefault();
        if (resource == null)
        {
          Logger.Debug(DirRX.ProjectPlanner.Resources.ResourceNotFoundFormat(resourceWorkload.ResourceId));
          continue;
        }
        
        //TODO Urmanov: вот тут была важная проверка на то что ресурсов не найдено, надо сделать с новыми ресурсами
        var activityLengthInDays = WorkingTime.GetDurationInWorkingDays(startDate, endDate, resource) - 1;
        var busy = resourceWorkload.Value;
        
        if (activityLengthInDays > 1)
          busy = resourceWorkload.Value / activityLengthInDays;
        
        InsertResourceLink(activityId, resourceWorkload.ResourceId, busy, connection);
      }
    }
    
    private static System.Data.IDbConnection CreateDBConnection()
    {
      System.Data.IDbConnection connection;
      
      connection = SQL.CreateConnection();
      
      if (connection.State != System.Data.ConnectionState.Open)
      {
        connection.Open();
      }
      
      return connection;
    }
    
    
    /// <summary>
    /// Удаление упоминаний активити в ресурсах.
    /// </summary>
    public static void RemoveActivityResources(System.Collections.Generic.List<int> activityIds, System.Data.IDbConnection connection)
    {
      using (var command = connection.CreateCommand())
      {
        var activityIdsParameter = activityIds.Count == 0 ? "0,0" : string.Join(",", activityIds.ToArray());
        //HACK: через AddArrayParameter работает нестабильно, пришлось сделать replace в запросе.
        //HACK: для того чтобы параметр IN(@resourceIds) в результате не привел к строке IN()
        command.CommandText = "delete from ResourceLinks where project_activity_id  IN(@project_activity_ids)"
          .Replace("@project_activity_ids", activityIdsParameter);
        
        command.ExecuteScalar();
      }
    }
    
    /// <summary>
    /// Удаление упоминаний активити в ресурсах.
    /// </summary>
    public static void RemoveActivityResources(int activityId, System.Data.IDbConnection connection)
    {
      using (var command = connection.CreateCommand())
      {
        command.CommandText = "delete from ResourceLinks where project_activity_id = @project_activity_id";
        SQL.AddParameter(command, "@project_activity_id", activityId, System.Data.DbType.Int32);
        command.ExecuteScalar();
      }
      
    }
    
    /// <summary>
    /// Обновление информации о трудоемкости.
    /// </summary>
    public static void RefreshCapacityInfo(List<DirRX.Planner.Model.ResourcesWorkload> resources, IProjectActivity activity)
    {
      var deletingCapasities = new List<IProjectActivityResourcesCapacity>();
      foreach(var capacity in activity.ResourcesCapacity)
      {
        if(!resources.Any(x => x.ResourceId == capacity.ResourceId))
        {
          deletingCapasities.Add(capacity);
        }
      }

      foreach(var deletingCapacity in deletingCapasities)
        activity.ResourcesCapacity.Remove(deletingCapacity);
      
      foreach(var resourceCapacity in resources)
      {
        IProjectActivityResourcesCapacity capacity = new ProjectActivityResourcesCapacity();
        capacity = activity.ResourcesCapacity.Where(x => x.ResourceId == resourceCapacity.ResourceId).FirstOrDefault();
        if (capacity == null)
        {
          capacity = activity.ResourcesCapacity.AddNew();
        }
        ChangePropertyIfDifferentValue(capacity, "ResourceId", resourceCapacity.ResourceId);
        ChangePropertyIfDifferentValue(capacity, "Capacity", resourceCapacity.Value);
      }
    }
    
    private void UpdateProjectFromGanttService(DirRX.ProjectPlanner.Structures.Module.IProjectDto projectModel, DirRX.ProjectPlanner.IProjectPlanRX plan)
    {
      var projectLockInfo = Locks.GetLockInfo(plan);
      if (projectLockInfo.IsLockedByOther)
      {
        Logger.DebugFormat("Не удалось сохранить проект. Проект заблокирован пользователем {0}.", projectLockInfo.OwnerName);
        return;
      }

      if (!plan.AccessRights.CanUpdate())
      {
        throw new Sungero.Domain.Shared.Exceptions.SecuritySystemException(false, Resources.ExceprionMessage);
      }
      
      plan.Name = projectModel.Name;
      plan.StartDate = projectModel.StartDate;
      plan.EndDate = projectModel.EndDate;
      plan.BaselineWork = projectModel.BaselineWork;
      plan.ExecutionPercent = projectModel.ExecutionPercent;
      plan.Note = projectModel.Note;
      
      //Раньше было в конце SaveModelFromGanttService, прямо перед сохранением, возможно изменит поведение.
      plan.PlannedCosts = projectModel.PlannedCosts;
      plan.FactualCosts = projectModel.FactualCosts;
    }
    
    private void UpdateLinkedProjectFromGanttService(
      DirRX.ProjectPlanning.IProject linkedProject,
      DirRX.ProjectPlanner.Structures.Module.IProjectDto projectModel,
      DirRX.ProjectPlanner.IProjectPlanRX plan)
    {
      linkedProject.StartDate = plan.StartDate;
      linkedProject.EndDate = plan.EndDate;
      linkedProject.BaselineWork = plan.BaselineWork;
      linkedProject.ExecutionPercent = plan.ExecutionPercent;

      if (projectModel.ManagerId.HasValue && linkedProject.Manager != null && projectModel.ManagerId != linkedProject.Manager.Id)
      {
        linkedProject.Manager = Sungero.Company.Employees.Get(projectModel.ManagerId.Value);
      }
    }
    
    private void DeleteActivitiesFromGanttService(
      DirRX.ProjectPlanner.IProjectPlanRX plan,
      DirRX.ProjectPlanner.Structures.Module.IGanttProjectPlanDto model,
      int lastId
     )
    {
      var actListId = ProjectPlanner.ProjectActivities
        .GetAll(p => 
                ProjectPlanning.Projects.Equals(plan, p.ProjectPlan)
                && p.NumberVersion.Value == model.NumberVersion)
        .Select(i => i.Id)
        .ToList();
      var actAppListId = model.Activities.Where(i => i.Id <= lastId).Select(i => i.Id).ToList();
      var actRemoveList = actListId.Where(a => !actAppListId.Contains(a)).ToList();
      
      // TODO Нужно рефакторить -- многократные поиски подчиненных этапов.
      int curIndex = 0;
      
      while (actRemoveList.Count > 0)
      {
        int curId = actRemoveList[curIndex];
        var act = ProjectActivities.Get(curId);
        if (!Functions.ProjectActivity.GetChildActivities(act).Any())
        {
          // Удаляемый этап является предшественником для любого другого этапа.
          foreach (var item in Functions.ProjectActivity.GetActivities(plan).Where(x => x.Predecessors.Any(y => y.Activity.Equals(act))))
          {
            item.Predecessors.Remove(item.Predecessors.First(x => x.Activity.Equals(act)));
          }
          try
          {
            //HACK: вложения не резолвятся через hibernate, приходится сначала материализовать
            var tasks = TeamsCommonAPI.TeamsTasks.GetAll().ToList().Where(t => t.Attachments.Any(a => a.Id == act.Id)); //FIXME: отладочный код, надо исправить - может остановить другую таску, если будет такой же ID вложения (надо сравнивать EntityInfo)
            
            foreach (var task in tasks)
            {
              task.Abort();
            }
            
            ProjectPlanner.ProjectActivities.Delete(act);
            actRemoveList.RemoveAt(curIndex);
          }
          catch(Exception ex)
          {
            var errorMessage = string.Format("Невозможно удалить активити с id {0}, Error: {1}", act.Id, ex.Message + Environment.NewLine + ex.StackTrace);
            Logger.Error(errorMessage);
            throw new Exception(errorMessage);
          }
        }
        else
          curIndex++;
        
        if (curIndex == actRemoveList.Count)
          curIndex = 0;
      }
    }
    
    private void UpdateActivityDataFromGanttService(
      DirRX.ProjectPlanner.Structures.Module.IGanttProjectPlanDto model,
      DirRX.ProjectPlanner.IProjectActivity activity,
      DirRX.ProjectPlanner.Structures.Module.IActivityDto activityModel)
    {
      ChangePropertyIfDifferentValue(activity, "Name", activityModel.Name);
      ChangePropertyIfDifferentValue(activity, "Number", activityModel.CurrentNumber);
      ChangePropertyIfDifferentValue(activity, "StartDate", activityModel.StartDate);
      ChangePropertyIfDifferentValue(activity, "EndDate", activityModel.EndDate);
      // Длительность в рабочих днях.
      ChangePropertyIfDifferentValue(activity, "Duration", ProjectPlanner.Functions.Module.GetWorkiningDaysInPeriod(activity.StartDate.Value, activity.EndDate.Value).ToString());
      ChangePropertyIfDifferentValue(activity, "BaselineWork", activityModel.BaselineWork);
      ChangePropertyIfDifferentValue(activity, "ExecutionPercent", activityModel.ExecutionPercent);
      ChangePropertyIfDifferentValue(activity, "Note", activityModel.Note);
      ChangePropertyIfDifferentValue(activity, "SortIndex", activityModel.SortIndex);
      ChangePropertyIfDifferentValue(activity, "Priority", activityModel.Priority);
      ChangePropertyIfDifferentValue(activity, "FactualCosts", activityModel.FactualCosts);
      ChangePropertyIfDifferentValue(activity, "PlannedCosts", activityModel.PlannedCosts);
      ChangePropertyIfDifferentValue(activity, "NumberVersion", model.NumberVersion);
    }
    
    private static bool ChangePropertyIfDifferentValue(object propertySource, string propertyName, object newValue)
    {
      var isChanged = false;
      var previousValue = propertySource.GetType().GetProperty(propertyName).GetValue(propertySource);
      
      if (newValue == previousValue)
      {
        return isChanged;
      }
      
      if (newValue is System.DateTime && previousValue is System.DateTime)
      {
        var castedNewValue = newValue as System.Nullable<System.DateTime>;
        var castedPreviousValue = previousValue as System.Nullable<System.DateTime>;
        if (castedNewValue != null && castedPreviousValue != null && castedNewValue.Value.Date == castedPreviousValue.Value.Date)
        {
          return isChanged;
        }
      }
      
      if ((previousValue == null && newValue != null) || (newValue == null && previousValue != null) || !previousValue.Equals(newValue))
      {
        propertySource.GetType().GetProperty(propertyName).SetValue(propertySource, newValue);
        isChanged = true;
      }
      
      return isChanged;
    }
    
    private System.Func<System.Threading.Tasks.Task> UpdateActivitiesFromGanttService(
      DirRX.ProjectPlanner.Structures.Module.ActivityDto[] activities,
      DirRX.ProjectPlanner.Structures.Module.IGanttProjectPlanDto model,
      int lastId,
      DirRX.ProjectPlanner.IProjectPlanRX plan,
      List<DirRX.ProjectPlanner.Structures.Module.ModelActivity> updatedActivityIds,
      System.Nullable<int> totalExPersent,
      List<DirRX.ProjectPlanner.Structures.Module.LeadActivities> leadActivities)
    {
      var activitiesForBatchSaving = new List<DirRX.ProjectPlanner.IProjectActivity>(model.Activities.Count);
      var activityResourcesForDeleting = new List<int>();
      var activityModelDict = new Dictionary<DirRX.ProjectPlanner.Structures.Module.ActivityDto, int>();
        
      using (var connection = CreateDBConnection())
      {
        var notNullActivities = activities.Where(a => a != null).ToList();
        var notNullActivitiesIds = notNullActivities.Select(act => act.Id).ToList();
        var existsActivities = ProjectPlanner.ProjectActivities.GetAll(a => notNullActivitiesIds.Contains(a.Id)).ToList();
        //4. Обновить или создать этапы.
        foreach (var activityModel in notNullActivities)
        {
          DirRX.ProjectPlanner.IProjectActivity activity;
          
          // Создать новый этап.
          if (activityModel.Id > lastId)
          {
            activity = ProjectPlanner.ProjectActivities.Create();
            activity.ProjectPlan = plan;
            
            var idActivity = Structures.Module.ModelActivity.Create(activityModel.Id, activity.Id);
            
            updatedActivityIds.Add(idActivity);
            activityModel.Id = idActivity.ID;
          }
          else
          {
            activity = existsActivities.FirstOrDefault(a => a.Id == activityModel.Id);
          }
          
          UpdateActivityDataFromGanttService(model, activity, activityModel);
          totalExPersent += activity.ExecutionPercent;
          
          //TODO URMANOV_AR: Поиск статуса через foreach - надо исправить в будущем
          if (activityModel.Status != null)
          {
            ChangePropertyIfDifferentValue(activity, "Status", activity.StatusAllowedItems.FirstOrDefault(s => s.ToString() == activityModel.Status.EnumValue));
          }
          
          //TODO URMANOV_AR: Поиск статуса через foreach - надо исправить в будущем.
          foreach (var type in activity.TypeActivityAllowedItems.Where(t => t.ToString() == activityModel.TypeActivity))
          {
            ChangePropertyIfDifferentValue(activity, "TypeActivity", type);
          }
          
          //TODO URMANOV_AR: Get запрос в foreach. - надо исправить в будущем.
          if (activityModel.ResponsibleId.HasValue)
          {
            var responsibleEmployee = Sungero.Company.Employees.Get(activityModel.ResponsibleId.Value);
            var isChanged = ChangePropertyIfDifferentValue(activity, "Responsible", responsibleEmployee);
            
            if(isChanged)
            {
              // не выполняю AccessRights.Save(), так как дальше идет сохранение сущности плана
              if(!plan.AccessRights.CanRead(responsibleEmployee))
              {
                plan.AccessRights.Grant(responsibleEmployee, DefaultAccessRightsTypes.Read);
              }
            }
          }
          else
          {
            ChangePropertyIfDifferentValue(activity, "Responsible", null);
          }
          
          // Подобрать ведущий этап.
          if (activityModel.LeadActivityId.HasValue)
          {
            if (activityModel.LeadActivityId.Value <= lastId)
            {
              ChangePropertyIfDifferentValue(activity, "LeadingActivity", ProjectPlanner.ProjectActivities.Get(activityModel.LeadActivityId.Value));
            }
            else
            {
              var leadActivity = Structures.Module.LeadActivities.Create(activity, activityModel.LeadActivityId.Value);
              leadActivities.Add(leadActivity);
            }
          }
          else
          {
            // Очистить ведущий этап.
            ChangePropertyIfDifferentValue(activity, "LeadingActivity", null);
          }
          
          RefreshCapacityInfo(activityModel.Resources.Select(r => new DirRX.Planner.Model.ResourcesWorkload() 
                                                           {
                                                             ResourceId = r.ResourceId,
                                                             Value = r.Value
                                                           }).ToList(), activity);
          
          if (activity.State.IsChanged || activity.State.IsInserted)
          {
            activitiesForBatchSaving.Add(activity);
            activityResourcesForDeleting.Add(activity.Id);
            activityModelDict.Add(activityModel, activity.Id);
          }
        }
        
        BatchSave(activitiesForBatchSaving.Where(a => a.State.IsChanged));
        
        RemoveActivityResources(activityResourcesForDeleting, connection);
          
        foreach (var activityModel in activityModelDict.Keys)
        {
          var newactivityId = activityModelDict[activityModel as DirRX.ProjectPlanner.Structures.Module.ActivityDto];
          SaveResources(
            activityModel.Resources.Select(r => new DirRX.Planner.Model.ResourcesWorkload()
                                           {
                                             ResourceId = r.ResourceId,
                                             Value = r.Value
                                           }).ToList(),
            newactivityId,
            activityModel.StartDate,
            activityModel.EndDate.Value,
            connection);
        }
      }
      
      return null;
    }
    
    /// <summary>
    /// Сохраняет изменения по проекту и этапам.
    /// </summary>
    /// <param name="model">Dto - модель плана проекта.</param>
    [Remote, Public(WebApiRequestType = RequestType.Post)]
    public void SaveModelFromGanttService(DirRX.ProjectPlanner.Structures.Module.IGanttProjectPlanDto model)
    {
      //1.найти проект и засинхронить с тем что пришло с веба.
      var projectModel = model.Project;
      var plan = ProjectPlanRXes.Get(model.ProjectPlanId);
      
      UpdateProjectFromGanttService(projectModel, plan);
      
      //2. синхронизация проекта с планом проекта.
      
      var linkedProject = DirRX.ProjectPlanning.Projects.GetAll(x => ProjectPlanRXes.Equals(plan, x.ProjectPlanDirRX)).FirstOrDefault();
      // Zheleznov_AV TODO уточнить во время рефакторинга, надо ли менять linkedProject. Если надо, то что именно менять, а что нет.
      // linkedProject меняется, но сохранение вызывается в одном из обработчиков событий, хрупкая логика.
      if (linkedProject != null)
      {
        UpdateLinkedProjectFromGanttService(linkedProject, projectModel, plan);
      }
      
      var lastId = model.LastActivityId;
      
      //3. Удалить этапы, которых нет в модели с веба.
      DeleteActivitiesFromGanttService(plan, model, lastId);
      
      var updatedActivityIds = new List<Structures.Module.ModelActivity>();
      var leadActivities = new List<Structures.Module.LeadActivities>();
      
      //Zheleznov_AV надо обновить все этапы. Операция обновления одного этапа стоит дорого. Поэтому будем сохранять все этапы за раз.
      var activitiesForBatchSaving = new List<DirRX.ProjectPlanner.IProjectActivity>(model.Activities.Count);
      
      int? totalExPersent = 0;
      
      Sungero.Domain.ParallelHelper.TaskRun(
        UpdateActivitiesFromGanttService(
          model.Activities.Select(a => (DirRX.ProjectPlanner.Structures.Module.ActivityDto)a).ToArray(),
          model,
          lastId,
          plan,
          updatedActivityIds,
          totalExPersent,
          leadActivities
         )
       );
        
      plan.ExecutionPercent = totalExPersent / (model.Activities.Count > 0 ? model.Activities.Count : 1);
      
      //5. Дозаполнить ведущие этапы.
      foreach (var actStructure in leadActivities)
      {
        var updatedIdItem = updatedActivityIds.FirstOrDefault(a => a.ModelActivitylId == actStructure.LeadActivityId);
        if (updatedIdItem == null)
        {
          throw new Exception(string.Format("не удалось найти обновленный id для активити с id={0}", actStructure.LeadActivityId));
        }
        ChangePropertyIfDifferentValue(actStructure.Activity, "LeadingActivity", ProjectPlanner.ProjectActivities.Get(updatedIdItem.ID));
        if (actStructure.Activity.State.IsChanged)
        {
          activitiesForBatchSaving.Add(actStructure.Activity);
        }
      }
      BatchSave(activitiesForBatchSaving.Where(a => a.State.IsChanged));
      activitiesForBatchSaving.Clear();
      
      //6. Обновить предшественников для этапов и ведущие этапы для сохраняемой в Storage модели.
      this.UpdateActivityRelation(model.Activities, updatedActivityIds, lastId);
      
      plan.Save();
      
      //7. Записать изменения в ФХ, подозреваю, что тоже требует оптимизации.
      WriteJsonBodyToProjectVersion(plan, model.NumberVersion, false);
    }
    
    private void UpdateActivityRelation(List<DirRX.ProjectPlanner.Structures.Module.IActivityDto> sourceActivities,
      List<DirRX.ProjectPlanner.Structures.Module.ModelActivity> updatedActivityIds,
      int lastActivityId)
    {
      var activitiesForBatchSaving = new List<DirRX.ProjectPlanner.IProjectActivity>(sourceActivities.Count);
      
      foreach (var sourceActivity in sourceActivities)
      {
        var targetActivity = ProjectPlanner.ProjectActivities.Get(sourceActivity.Id);
        
        this.UpdatePredecessor(sourceActivity, targetActivity, updatedActivityIds, lastActivityId);
        this.UpdateLeadActivity(sourceActivity, updatedActivityIds, lastActivityId);
        
        activitiesForBatchSaving.Add(targetActivity);
      }
      
      BatchSave(activitiesForBatchSaving.Where(a => a.State.IsChanged));
    }
    
    private void UpdatePredecessor(DirRX.ProjectPlanner.Structures.Module.IActivityDto sourceActivity,
      DirRX.ProjectPlanner.IProjectActivity targetActivity,
      List<DirRX.ProjectPlanner.Structures.Module.ModelActivity> updatedActivityIds,
      int lastActivityId)
    {
      if (sourceActivity.Predecessors != null && sourceActivity.Predecessors.Count > 0)
      {
        targetActivity.Predecessors.Clear();
        
        foreach (var predecessor in sourceActivity.Predecessors)
        {
          int predecessorId = predecessor.Id.Value;
          
          if (predecessor.Id.Value > lastActivityId)
          {
            var updatedPredeccessorId = updatedActivityIds.FirstOrDefault(x => x.ModelActivitylId == predecessor.Id);
            if (updatedPredeccessorId == null)
            {
              throw new Exception(DirRX.ProjectPlanner.Resources.FailToFindUpdatedIdExceptionTextFormat(predecessor.Id));
            }
            predecessorId = updatedPredeccessorId.ID;
          }
          
          var activityPredecessor  = targetActivity.Predecessors.AddNew();
          activityPredecessor.Activity = ProjectPlanner.ProjectActivities.Get(predecessorId);
          activityPredecessor.LinkType = predecessor.LinkType;
          activityPredecessor.Lag = predecessor.Lag;
          predecessor.Id = targetActivity.Id;
        }
      }
      else
      {
        targetActivity.Predecessors.Clear();
      }
    }
    
    /// <summary>
    /// Массовое сохранение сущностей в отдельной сессии БД.
    /// </summary>
    /// <param name="activities">Сущности для сохранения.</param>
    private static void BatchSave(System.Collections.Generic.IEnumerable<object> entities)
    {
      if (!entities.Any())
        return;
      
      using (var session = Sungero.Domain.Session.CreateIndependentSession())
      {
        foreach (var entity in entities)
        {
          session.Update(entity);
        }
        
        session.SubmitChanges();
      }
    }
    
    private static List<int> GetProjectResources(int projectPlanId, int numberVersion)
    {
      using (var connection = CreateDBConnection())
      {
        var resourcesList = new List<int>();
         
        using (var command = connection.CreateCommand())
        {
          //получить ресурсы по активити
          command.CommandText = "select distinct resource_id from ResourceLinks rl "+
                                "join DirRX_Projec1_PrjctActivity activities on rl.project_activity_id = activities.id " +
                                "where (activities.ProjectPlan = @projectPlanId AND activities.NumberVersion = @VersionNum)";
          SQL.AddParameter(command, "@projectPlanId", projectPlanId, System.Data.DbType.Int32);
          SQL.AddParameter(command, "@VersionNum", numberVersion, System.Data.DbType.Int32);
          using (var reader = command.ExecuteReader())
          while (reader.Read())
          {
            int val;
            if (int.TryParse(reader[0].ToString(), out val))
            {
              resourcesList.Add(val);            
            }
          }
        }
        return resourcesList;
      }
    }
    
    /// <summary>
    /// 
    /// </summary>
    private static Structures.Module.IResourcesData HandleUpdateResourcesDataDto(int projectPlanId, int numberVersion)
    {
      var projectPlan = ProjectPlanRXes.GetAll(x => x.Id == projectPlanId).FirstOrDefault();
      if (projectPlan == null)
      {
        throw new Exception(DirRX.ProjectPlanner.Resources.NonExistentPlanIdExeptionFormat(projectPlanId));
      }
      
      var planActivities = ProjectActivities.GetAll(a => ProjectPlanRXes.Equals(a.ProjectPlan, projectPlan) && a.NumberVersion.Value == numberVersion).ToList();
      var planDates = GetProjectPlanDates(projectPlan, planActivities);
      
      //users и resources в dictionary
      
      var users = new Dictionary<int, Structures.Module.IUser>();
      var projectManager = TryGetProjectManager(projectPlan);
      if (projectManager != null)
      {
        users.Add(projectManager.Id, projectManager);
      }
      
      var resources = new Dictionary<int, Structures.Module.IResource>();
      var resourceIdList = GetProjectResources(projectPlanId, numberVersion);
      
      FillUsersFromResponsibles(planActivities, users);
      AddEmployeeResources(resourceIdList, users, resources);
      return new Structures.Module.ResourcesData
      {
        Users = users.Values.ToList(),
        Resources = resources.Values.ToList(),
        Capacity = GetCapacity(resourceIdList, planDates.Start, planDates.End, projectPlanId, numberVersion),
        MaterialResources = GetMaterialResources(),
        ResourceTypes = GetResourcesTypes(),
        WorkingTimeCalendars = GetWorkingTimeCalendars(resourceIdList, planDates.Start, planDates.End)
      };
    }
    
    /// <summary>
    /// Выбирает актуальные даты начала и окончания плана проекта. В карточке плана находятся даты для последней версии плана.
    /// Если надо открыть не последнюю версию плана, то актуальные даты могут отличаться. Поэтому дополнительно ищем даты в списке активити.
    /// </summary>
    /// <param name="projectPlan">План проекта</param>
    /// <param name="activities">Список активити</param>
    /// <returns>Структура с актуальными датами начала и окончания плана</returns>
    private static Structures.Module.ProjectPlanDates GetProjectPlanDates(DirRX.ProjectPlanner.IProjectPlanRX projectPlan, List<DirRX.ProjectPlanner.IProjectActivity> activities)
    {
      var startDateFromCard = Convert.ToDateTime(projectPlan.StartDate);
      //Kiselev HACK в карточке, для удобства отображения, endDate уменьшается на день. Добавляем к endDate день.
      var endDateFromCard = Convert.ToDateTime(projectPlan.EndDate).AddDays(1);
      
      if (startDateFromCard >= endDateFromCard)
      {
        endDateFromCard = startDateFromCard.AddDays(1);
      }
      
      var startDates = new List<DateTime>{startDateFromCard};
      var endDates = new List<DateTime>{endDateFromCard};
      
      startDates.AddRange(activities.Where(a => a.StartDate.HasValue).Select(b => b.StartDate.Value));
      endDates.AddRange(activities.Where(a => a.EndDate.HasValue).Select(b => b.EndDate.Value));
      
      return new Structures.Module.ProjectPlanDates
      {
        Start = startDates.Min(),
        End = endDates.Max()
      };
    }
    
    private static string GetPlanBaselineWorkType(int projectPlanId)
    {
      const string baselineWorkTypeNotSet = "baselineWorkTypeNotSet";
      var projectPlan = ProjectPlanRXes.GetAll(x => x.Id == projectPlanId).FirstOrDefault();
      if (projectPlan == null)
      {
        throw new Exception(DirRX.ProjectPlanner.Resources.NonExistentPlanIdExeptionFormat(projectPlanId));
      }
      return projectPlan.BaselineWorkType.HasValue ? projectPlan.BaselineWorkType.Value.ToString() : baselineWorkTypeNotSet;
    }
    
    /// <summary>
    /// Получаем выходные дни - исключения (выходные в ПН-ПТ).
    /// </summary>
    /// <param name="calendar">Календарь рабочего времени.</param>
    /// <returns>Дополнительные выходные дни.</returns>
    private static List<DateTime> GetExtraFreeDays(IEnumerable<Sungero.CoreEntities.IWorkingTimeCalendar> calendars)
    {
      //Zheleznov_AV HACK предварительная оптимизация работы с календарями. При получении дат нам надо проверять является ли день рабочим,
      //с учетом частного календаря. Раньше мы использовали метод `d.Day.IsWorkingDay(employee)`. Но это было медленно. Сейчас мы смотрим на d.Duration .
      //
      //Скорее всего есть способ дальнейшей оптимизации. Вроде того, что бы сразу из БД получать нужные данные, a не фильтровать их на уровни прикладной.
      //
      //Данный hack используется еще в методах GetExtraWorkingDays и GetCapacity.
      return calendars.SelectMany(c => c.Day).Where(d =>
                                (d.Day.DayOfWeek != DayOfWeek.Saturday && d.Day.DayOfWeek != DayOfWeek.Sunday)
                                && d.Duration == 0)
        .Select(d => d.Day)
        .ToList();
    }
    
    /// <summary>
    /// Получаем рабочие дни - исключения (рабочие в СБ-ВС).
    /// Если суббота или воскресенье становится рабочим днем, то считаем что продолжительность для будет 8 часов.
    /// </summary>
    /// <param name="calendar">Календарь рабочего времени.</param>
    /// <returns>Дополнительные рабочие дни.</returns>
    private static List<Structures.Module.IWorkDay> GetExtraWorkingDays(IEnumerable<Sungero.CoreEntities.IWorkingTimeCalendar> calendars)
    {
      const int defaultWorkDayDuration = 8;
      return calendars.SelectMany(c => c.Day).Where(d => (d.Day.DayOfWeek == DayOfWeek.Saturday || d.Day.DayOfWeek == DayOfWeek.Sunday) && d.Duration > 0)
        .Select(d => new Structures.Module.WorkDay
                {
                  Date = d.Day,
                  Duration = defaultWorkDayDuration
                } as Structures.Module.IWorkDay)
        .ToList();
    }
    
    #region методы, вынесенные из GetModel()
    
    /// <summary>
    /// Создает модель проекта
    /// </summary>
    /// <param name="projectPlan">Проект</param>
    /// <returns>Модель</returns>
    private static DirRX.Planner.Model.Project CreateProjectAppModel(IProjectPlanRX projectPlan)
    {
      var projectApp = new DirRX.Planner.Model.Project();
      projectApp.Id = projectPlan.Id;
      projectApp.Name = projectPlan.Name;
      projectApp.Stage = Sungero.Projects.Projects.Info.Properties.Stage.GetLocalizedValue(projectPlan.Stage);
      projectApp.StartDate = projectPlan.StartDate;
      projectApp.EndDate = projectPlan.EndDate;
      projectApp.BaselineWork = projectPlan.BaselineWork;
      projectApp.ExecutionPercent = projectPlan.ExecutionPercent;
      projectApp.FactualCosts = projectPlan.FactualCosts;
      projectApp.PlannedCosts = projectPlan.PlannedCosts;
      projectApp.Note = projectPlan.Note;
     
      return projectApp;
    }
    
    [Remote]
    public static DirRX.ProjectPlanning.IProject GetLinkedProject(IProjectPlanRX plan)
    {
     return DirRX.ProjectPlanning.Projects.GetAll(x => x.ProjectPlanDirRX != null && plan.Id == x.ProjectPlanDirRX.Id).FirstOrDefault();
    }
    
    /// <summary>
    /// Добавить менеджера проекта.
    /// </summary>
    /// <param name="projectApp">Проект.</param>
    /// <param name="users">Список пользователей.</param>
    private static void AddManager(DirRX.Planner.Model.Project projectApp, List<DirRX.Planner.Model.Users> users, DirRX.ProjectPlanning.IProject linkedProject)
    {
     if (projectApp.ManagerId == null)
     {
       return;
     }
     
     var managerInfo = new DirRX.Planner.Model.Users();
     managerInfo.Id = linkedProject.Manager.Id;
     managerInfo.Name = linkedProject.Manager.Person.FirstName;
     managerInfo.Surname = linkedProject.Manager.Person.LastName;
     users.Add(managerInfo);
    }
    
    /// <summary>
    /// Создать модель активити.
    /// </summary>
    /// <returns>Модель.</returns>
    private static DirRX.Planner.Model.Activity CreateActivity(IProjectActivity activity, List<DirRX.Planner.Model.Users> users)
    {
      var item = new DirRX.Planner.Model.Activity();
      item.BaselineWork = activity.BaselineWork;
      item.LeadActivityId = activity.LeadingActivity != null ? activity.LeadingActivity.Id : (int?)null;
      item.Id = activity.Id;
      item.StartDate = activity.StartDate;
      item.EndDate = activity.EndDate;
      item.Name = activity.Name;
      item.ExecutionPercent = activity.ExecutionPercent;
      item.Note = activity.Note;
      item.SortIndex = activity.SortIndex.HasValue ? activity.SortIndex.Value : 1;

      item.Predecessors = activity.Predecessors.Where(x => x.Activity != null).Select(x => 
                                                                                      new DirRX.Planner.Model.Predecessor 
                                                                                      {
                                                                                        Id = x.Activity.Id,
                                                                                        LinkType = x.LinkType,
                                                                                        Lag = x.Lag ?? 0
                                                                                      }).ToList();

      item.Priority = activity.Priority;
      item.PlannedCosts = activity.PlannedCosts;
      item.FactualCosts = activity.FactualCosts;
      item.TypeActivity = activity.TypeActivity.ToString();
      item.Status = new DirRX.Planner.Model.ActivityStatus 
      {
        EnumValue = activity.Status.ToString(),
        LocalizeValue = activity.Info.Properties.Status.GetLocalizedValue(activity.Status)
      };
     
      FillResources(activity, item);
      
      AddResponsible(activity, users, item);
      
      return item;
    }
    
    /// <summary>
    /// Заполнить активити ресурсами.
    /// </summary>
    /// <param name="activity">Активити.</param>
    /// <param name="item">Модель активити.</param>
    private static void FillResources(IProjectActivity activity, DirRX.Planner.Model.Activity item)
    {
      foreach(var resource in activity.ResourcesCapacity)
      {
        var resourceData = new DirRX.Planner.Model.ResourcesWorkload();
        resourceData.ResourceId = resource.ResourceId.Value;
        resourceData.Value = resource.Capacity.Value;
        item.Resources.Add(resourceData);
      }
    }
    /// <summary>
    /// Добавить ответственных.
    /// </summary>
    /// <param name="activity">Активити.</param>
    /// <param name="users">Список ответственных.</param>
    private static void AddResponsible(IProjectActivity activity, List<DirRX.Planner.Model.Users> users, DirRX.Planner.Model.Activity item)
    {
      var user = new DirRX.Planner.Model.Users();
      if (activity.Responsible != null)
      {
        item.ResponsibleId = activity.Responsible.Id;
        
        user.Id = activity.Responsible.Id;
        user.Name = activity.Responsible.Person.FirstName;
        user.Surname = activity.Responsible.Person.LastName;
        
        if (!users.Any(x => x.Id == user.Id))
          users.Add(user);
      }
    }
    
    /// <summary>
    /// Заполнить типы ресурсов.
    /// </summary>
    /// <returns>Типы ресурсов.</returns>
    private static List<DirRX.Planner.Model.ResourceTypes> FillResourceTypes()
    {
      var resourceTypes = new List<DirRX.Planner.Model.ResourceTypes>();
      foreach(var type in ProjectResourceTypes.GetAll())
      {
        var resourceType = new DirRX.Planner.Model.ResourceTypes();
        resourceType.Id = type.Id;
        resourceType.Name = type.Name;
        resourceType.SectionName = type.ServiceName;
        resourceTypes.Add(resourceType);
      }
      return resourceTypes;
    }
    
    /// <summary>
    /// Найти человекоресурсы. наверное объединять с следующим методом надо.
    /// </summary>
    /// <param name="projActs">Активити.</param>
    /// <param name="users">Пользователи проекта.</param>
    /// <param name="resources">Ресурсы.</param>
    private static void FindEmployeeResources(List<IProjectActivity> projActs, List<DirRX.Planner.Model.Users> users, List<DirRX.Planner.Model.Resources> resources)
    {
      Logger.Debug("Find employee Resource.");
      
      var firstAct = projActs.FirstOrDefault();
      if (firstAct == null)
      {
        return;
      }
      
      var resourcesList = GetProjectResources(firstAct.ProjectPlan.Id, firstAct.NumberVersion.Value);
      //TODO URMANOV: тут такой же запрос как в UpdateActivityResources на получение ид ресурсов в проекте
      foreach (var employeeResource in ProjectsResources.GetAll(x => x.Type.ServiceName == Constants.Module.ResourceTypes.Users
                                                                && resourcesList.Contains(x.Id)))
      {
        Logger.Debug("Finded employee Resource.");
        var user = new DirRX.Planner.Model.Users();
        var resourceLink = new DirRX.Planner.Model.Resources();
        
        user.Id = employeeResource.Employee.Id;
        user.Name = employeeResource.Employee.Person != null ? employeeResource.Employee.Person.FirstName : employeeResource.Employee.Name;
        user.Surname = employeeResource.Employee.Person != null ? employeeResource.Employee.Person.LastName : string.Empty;
        resourceLink.Id = employeeResource.Id;
        resourceLink.EntityTypeId = employeeResource.Type.Id;
        resourceLink.EntityId = employeeResource.Employee.Id;
        resourceLink.UnitLabel = employeeResource.Type.MeasureUnit;
        
        if(!users.Any(x => x.Id == user.Id))
          users.Add(user);
        
        resources.Add(resourceLink);
        Logger.DebugFormat("employee {0} Resource {1} added.", user.Id, resourceLink.Id);
      }
    }
    
    /// <summary>
    /// Найти материальные ресурсы.
    /// </summary>
    /// <param name="projActs">Активити.</param>
    /// <param name="resources">Ресурсы.</param>
    /// <returns></returns>
    private static List<DirRX.Planner.Model.MaterialResources> FindMaterialResources(List<IProjectActivity> projActs, List<DirRX.Planner.Model.Resources> resources)
    {
      var firstAct = projActs.FirstOrDefault();
      if (firstAct == null)
      {
        return null;
      }
      
      var resourcesList = GetProjectResources(firstAct.ProjectPlan.Id, firstAct.NumberVersion.Value);
      
      var materialResources = new List<DirRX.Planner.Model.MaterialResources>();
      Logger.Debug("Find material Resource.");
      foreach (var resource in ProjectsResources.GetAll(x => x.Type.ServiceName == Constants.Module.ResourceTypes.MaterialResources
                                                        && resourcesList.Contains(x.Id)))
      {
        Logger.Debug("Finded material Resource.");
        var materialResource = new DirRX.Planner.Model.MaterialResources();
        var resourceLink = new DirRX.Planner.Model.Resources();
        
        materialResource.Id = resource.Id;
        materialResource.Name = resource.Name;
        
        resourceLink.Id = resource.Id;
        resourceLink.EntityTypeId = resource.Type.Id;
        resourceLink.EntityId = resource.Id;
        resourceLink.UnitLabel = resource.Type.MeasureUnit;
        
        materialResources.Add(materialResource);
        resources.Add(resourceLink);
        Logger.DebugFormat("resource {0} Resource {1} added.", materialResource.Id, resourceLink.Id);
      }
      return materialResources;
    }
    #endregion
    
    private static int GetLastActivityId(List<IProjectActivity> projActs)
    {
      //Zheleznov_AV HACK изначально тут был метод, получающий lastActivtyId через анализ истории сущностей.
      //Он работал медленно. В рамках оптимизации изменил метод на "однострочник". Не делаю inline-ing данного метода,
      //что бы не менять текущий API.
      return projActs.Any() ? projActs.Max(x => x.Id) : 0;
    }
    
    public static string UpdateModel(IProjectPlanRX project, int numberVersion, DirRX.Planner.Model.Model model)
    {
      var prActs = ProjectActivities.Create();
      var listStatuses = new List<DirRX.Planner.Model.ActivityStatus>();
      foreach (Enumeration i in prActs.StatusAllowedItems)
      {
        listStatuses.Add(new DirRX.Planner.Model.ActivityStatus {EnumValue = i.ToString(), LocalizeValue = prActs.Info.Properties.Status.GetLocalizedValue(i)});
      }
     
      model.ActivityStatuses = listStatuses;
      
      var projActs = ProjectActivities.GetAll(a => ProjectPlanRXes.Equals(a.ProjectPlan, project) && a.NumberVersion.Value == numberVersion).ToList();
      model.LastActivityId = GetLastActivityId(projActs);

      var modelJson = Newtonsoft.Json.JsonConvert.SerializeObject(model);
      
      return modelJson;
    }
    
    [Remote, Public(WebApiRequestType = RequestType.Get)]
    public static string GetModel(int projectPlanId, int numberVersion)
    {
      var projectPlan = ProjectPlanRXes.Get(projectPlanId);
      return GetModel(projectPlan, numberVersion);
    }
    
    /// <summary>
    /// Передает данные по проекту в модель Json.
    /// </summary>
    /// <param name="projectId">ИД проекта.</param>
    /// <returns>Сериализованная строка с данными по проекту.</returns>
    [Remote, Public]
    public static string GetModel(IProjectPlanRX project, int numberVersion)
    {
      var model = new DirRX.Planner.Model.Model();
      var projActs = ProjectActivities.GetAll(a => ProjectPlanRXes.Equals(a.ProjectPlan, project) && a.NumberVersion.Value == numberVersion).ToList();
      
      var activitiesApp = new List<DirRX.Planner.Model.Activity>();
      var resources = new List<DirRX.Planner.Model.Resources>();
      var users = new List<DirRX.Planner.Model.Users>();
      
      AccessRights.AllowRead(() =>
                             {
                               var projectApp = CreateProjectAppModel(project);
                               projectApp.Note = project.Note;
                               
                               foreach (var activity in projActs)
                               {
                                 var item = CreateActivity(activity, users);
                                 
                                 activitiesApp.Add(item);
                               }
                               var resourceTypes = FillResourceTypes();
                              
                               var linkedProject = GetLinkedProject(project);
                               
                               if (linkedProject != null)
                               {
                                 projectApp.ManagerId = linkedProject.Manager.Id;
                                 projectApp.UseBaseLineWorkInHours = linkedProject.BaselineWorkType != DirRX.ProjectPlanning.Project.BaselineWorkType.Money;
                               }
                               
                               AddManager(projectApp, users, linkedProject);
                               
                               FindEmployeeResources(projActs, users, resources);
                               
                               var materialResources = FindMaterialResources(projActs, resources);
                               
                               List<DirRX.Planner.Model.Capacity> capacities = new List<DirRX.Planner.Model.Capacity>();
                               
                               model.LastActivityId = GetLastActivityId(projActs);
                               
                               var prAct = ProjectActivities.Create();
                               var listStatuses = new List<DirRX.Planner.Model.ActivityStatus>();
                               foreach (Enumeration i in prAct.StatusAllowedItems)
                               {
                                 listStatuses.Add(new DirRX.Planner.Model.ActivityStatus {EnumValue = i.ToString(), LocalizeValue = prAct.Info.Properties.Status.GetLocalizedValue(i)});
                               }
                               
                               model.ActivityStatuses = listStatuses;
                               model.Project = projectApp;
                               model.Activities = activitiesApp.ToList();
                               Logger.Debug("Filling resourcesData.");
                               var resourcesData = new DirRX.Planner.Model.ResourcesData();
                               resourcesData.MaterialResources = materialResources;
                               resourcesData.Resources = resources;
                               resourcesData.ResourceTypes = resourceTypes;
                               resourcesData.Users = users;
                               model.ResourcesData = resourcesData;
                             });
      
      var modelJson = Newtonsoft.Json.JsonConvert.SerializeObject(model);
      
      return modelJson;
    }
    
    /// <summary>
    /// Отправка задачи по этапу проекта.
    /// </summary>
    /// <param name="activityId">Id этапа</param>
    /// <returns>Id задачи</returns>
    [Public(WebApiRequestType = RequestType.Post), Remote]
    public static int CreateTask(int activityId)
    {
      var activity = ProjectPlanner.ProjectActivities.Get(activityId);
      var subject = string.Format(Resources.SendTaskSubject, activity.Name);
      var newTask = ProjectActivityTasks.Create();
      if(activity.Performers != null && activity.Performers.Count > 0)
      {
        foreach (var performer in activity.Performers)
        {
          var newResponsible = newTask.Responsibles.AddNew();
          newResponsible.Responsible = performer.Performer;
        }
      }
      
      if (activity.Responsible != null)
      {
          var newResponsible = newTask.Responsibles.AddNew();
          newResponsible.Responsible = activity.Responsible;
      }
      
      newTask.Subject = subject;
      
      //Отнимаем один календарный день, потому что дата окончания активити в коде равна 00:00 следующего дня, 
      //что не соответствует визуальному отображению в клиенте.
      var activityEndDate = (activity.EndDate ?? activity.StartDate ?? Calendar.Today).AddDays(-1);
      //Для того чтобы точно быть уверенными, что мы берем дату без времени - берем свойство Date у активити,
      //не смотря на то, что в нормальных ситуациях там будет DateTime с временем 00:00.
      newTask.MaxDeadline = activityEndDate.Date;
      newTask.ActiveText = Resources.SendTaskActiveText;
      newTask.ProjectActivity = activity;
      newTask.ProjectPlan = activity.ProjectPlan;
      newTask.Save();
      
      return newTask.Id;
      
    }
    
    /// <summary>
    /// Возвращает список задач по их идентификаторам.
    /// </summary>
    /// <param name="tasksIds">Список ИД задач.</param>
    /// <returns>Список задач.</returns>
    [Remote(IsPure = true), Public]
    public static IQueryable<Sungero.Workflow.ISimpleTask> GetTasksByIds(List<int> tasksIds)
    {
      return Sungero.Workflow.SimpleTasks.GetAll().Where(c => tasksIds.Contains(c.Id));
    }
    
    [Remote(IsPure = true)]
    public static bool VersionApproved(Sungero.Content.IElectronicDocument project, int numVersion)
    {
      return Signatures.Get(project.Versions.First(x => x.Number.Value == numVersion)).Where(s => s.SignatureType == SignatureType.Approval).Any();
    }
    
    [Remote, Public]
    public static string GetWebSite()
    {
      var result = string.Empty;
      AccessRights.AllowRead(() =>
                             {
                               try
                               {
                                 var webAdressGetter = new ConfigWrapperV1_0_0.WebClientAddressGetter();
                                 result = webAdressGetter.GetContentFullAddress(new Uri(Constants.Module.DefaultClientAddress, UriKind.Relative)).ToString();
                               }
                               catch (ConfigWrapperV1_0_0.Exceptions.SungeroConfigSettingsException ex)
                               {
                                 throw new Exception(DirRX.ProjectPlanner.Resources.ErrorWhileTryOpenWebClient, ex);
                               }
                               
                             });
      return result;
    }
    
    
    /// <summary>
    /// Проверить наличие у участника прав на сущность.
    /// </summary>
    /// <param name="entity">Сущность.</param>
    /// <param name="member">Участник.</param>
    /// <param name="accessRightsType">Тип прав.</param>
    /// <returns>True - если права есть, иначе - false.</returns>
    public static bool CheckGrantedRights(Sungero.Domain.Shared.IEntity entity, IRecipient member, Guid accessRightsType)
    {
      if (accessRightsType == DefaultAccessRightsTypes.Change)
        return entity.AccessRights.IsGrantedDirectly(accessRightsType, member) ||
          entity.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.FullAccess, member);
      
      if (accessRightsType == Constants.Module.ChangeContent)
        return entity.AccessRights.IsGrantedDirectly(accessRightsType, member) ||
          entity.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Change, member) ||
          entity.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.FullAccess, member);
      
      if (accessRightsType == DefaultAccessRightsTypes.Read)
        return entity.AccessRights.IsGrantedDirectly(accessRightsType, member) ||
          entity.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.FullAccess, member) ||
          entity.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Change, member) ||
          entity.AccessRights.IsGrantedDirectly(Constants.Module.ChangeContent, member);
      
      return entity.AccessRights.IsGrantedDirectly(accessRightsType, member);
    }
    
    /// <summary>
    /// 
    /// </summary>
    public static List<Structures.Module.ICapacity> GetCapacity(List<int> resourceIds, DateTime startDate, DateTime endDate, int planId, int planVersion)
    {
      var calendars = GetPrivateOrPublicCalendars(startDate, endDate, resourceIds);
      
      var capacities = new Dictionary<int, List<DirRX.ProjectPlanner.Structures.Module.ICapacityValue>>();
      using (var connection = CreateDBConnection())
      {
        using (var command = connection.CreateCommand())
        {
          command.CommandText = Queries.Module.GetCapacity;
          SQL.AddParameter(command, "@startDate", startDate, System.Data.DbType.Date);
          //HACK: -1 день т.к. активити на самом деле длятся минимум 2 дня, отображаются на 1 день меньше.
          SQL.AddParameter(command, "@endDate", endDate, System.Data.DbType.Date);
          SQL.AddParameter(command, "@projectplan_id", planId, System.Data.DbType.Int32);
          SQL.AddParameter(command, "@projectplan_version", planVersion, System.Data.DbType.Int32);
          
          //HACK: через AddArrayParameter работает нестабильно, пришлось сделать replace в запросе.
          //HACK: для того чтобы параметр IN(@resourceIds) в результате не привел к строке IN()
          var resourceIdsParameter = resourceIds.Count == 0 ? "0,0" : string.Join(",", resourceIds.ToArray());
          command.CommandText = Queries.Module.GetCapacity.Replace(
                                                          "@resource_ids", 
                                                          resourceIdsParameter);
          
          using (var reader = command.ExecuteReader())
          {
            var calendarErrors = new Dictionary<int, HashSet<int>>();
            while (reader.Read())
            {
              var resourceId = (int)reader[0];
              var date = (DateTime)reader[2];
              var Busy = double.Parse(reader[1].ToString());
              
              var calendar = calendars[resourceId].FirstOrDefault(c => c.Year == date.Year);
              if (calendar == null) 
              {
                if (!calendarErrors.ContainsKey(resourceId))
                {
                  calendarErrors.Add(resourceId, new HashSet<int>() {date.Year});
                }
                else
                {
                  if (!calendarErrors[resourceId].Contains(date.Year))
                  {
                    calendarErrors[resourceId].Add(date.Year);
                  }
                }
                continue;
              }
              
              if (!capacities.ContainsKey(resourceId)) {
                capacities.Add(resourceId, new List<DirRX.ProjectPlanner.Structures.Module.ICapacityValue>());
              }
              
              var calendarDay = calendar.Day.FirstOrDefault(d => ((DateTime)reader[2]).Date == d.Day.Date);
              
              if (calendarDay != null && calendarDay.Kind == null)
              {
                capacities[resourceId].Add(new DirRX.ProjectPlanner.Structures.Module.CapacityValue() 
                                  {
                                    Date = (DateTime)reader[2],
                                    Busy = double.Parse(reader[1].ToString())
                                   });
              }
              
            }
            
            foreach (var resId in calendarErrors)
            {
              foreach (var year in resId.Value)
                Logger.Error(DirRX.ProjectPlanner.Resources.ColdNotFindCalendarWithResourceIdFormat(year, resId.Key));
            }
            
          }
        }
      }
      var result = new List<Structures.Module.ICapacity>();
      
      result.AddRange(
        capacities.Select(x => 
                          new DirRX.ProjectPlanner.Structures.Module.Capacity
                          {
                            ResourceId = x.Key,
                            Values = x.Value
                          }
                         )
       );
      
      return result;
    }
    
    private static double GetDailyAverageBusy(IEnumerable<Structures.Module.AverageBusy> busyes, int resourceId, DateTime date)
    {
      return busyes.Where(b => b.EndDate > date && b.StartDate <= date).Sum(b => b.AvgBusy);
    }
    
    private static List<Structures.Module.AverageBusy> GetResourceAverageBusy(int planId, int numberVersion, DateTime startDate, DateTime endDate, System.Data.IDbConnection connection)
    {
      List<Structures.Module.AverageBusy> result = new List<DirRX.ProjectPlanner.Structures.Module.AverageBusy>();
      
      //получить список активити (дата начала, конца, загрузка) в выбранный период для ресурса
      using (var command = connection.CreateCommand())
      {
        command.CommandText = Queries.Module.GetResourceAverageBusy;
        SQL.AddParameter(command, "@projectPlanId", planId, System.Data.DbType.Int32);
        SQL.AddParameter(command, "@VersionNum", numberVersion, System.Data.DbType.Int32);
        SQL.AddParameter(command, "@StartDate", startDate, System.Data.DbType.DateTime);
        SQL.AddParameter(command, "@EndDate", (endDate == default) ? startDate : endDate , System.Data.DbType.DateTime);
        
        using (var reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            var avgBusy = new Structures.Module.AverageBusy();
            avgBusy.AvgBusy = (float)reader[0];
            avgBusy.ResourceId = (int)reader[3];
            avgBusy.StartDate = (DateTime)reader[1];
            avgBusy.EndDate = (DateTime)reader[2];
            result.Add(avgBusy);
          }
        }
      }
      return result;
    }
    
    /// <summary>
    /// 
    /// </summary>
    public static List<Structures.Module.IWorkingTimeCalendar> GetWorkingTimeCalendars(List<int> resourceIds, DateTime startDate, DateTime endDate)
    {
      var workingTimeCalendar = new List<Structures.Module.IWorkingTimeCalendar>();
      var calendars = GetPrivateOrPublicCalendars(startDate, endDate, resourceIds);
      
      var calendarsDto = resourceIds.Select(resId => 
                                          new Structures.Module.WorkingTimeCalendar() 
                                          {
                                            ResourcesIds = new List<int>(){resId},
                                            FreeDays = GetExtraFreeDays(calendars[resId]),
                                            WorkDays = GetExtraWorkingDays(calendars[resId])
                                          });
      workingTimeCalendar.AddRange(calendarsDto);
      return workingTimeCalendar;
    }
    
    /// <summary>
    /// Получить календарь
    /// </summary>
    /// <param name="startDate">Дата начала.</param>
    /// <param name="endDate">Дата окончания.</param>
    /// <param name="resourceId">id ресурса</param>
    /// <returns>Календарь рабочего времени.</returns>
    public static System.Collections.Generic.Dictionary<int, List<Sungero.CoreEntities.IWorkingTimeCalendar>> GetPrivateOrPublicCalendars(DateTime startDate, DateTime endDate, List<int> resourceIds)
    {
      var resources = ProjectsResources.GetAll(r => resourceIds.Contains(r.Id)).ToArray();
      var calendars = new Dictionary<int, List<Sungero.CoreEntities.IWorkingTimeCalendar>>();
      foreach (var resource in resources)
      {
        if (!calendars.ContainsKey(resource.Id)) {
                calendars.Add(resource.Id, new List<Sungero.CoreEntities.IWorkingTimeCalendar>());
              }
        
        Sungero.CoreEntities.Shared.CalendarCache.Restrict(resource.Employee, startDate.Year, endDate.Year);
        
        var calendarsCached = Sungero.CoreEntities.Shared.CalendarCache.GetCached();
        calendars[resource.Id].AddRange(
          calendarsCached
          .Where(c => 
                 PrivateWorkingTimeCalendars.Is(c) && (PrivateWorkingTimeCalendars.As(c).Recipients.Select(r => r.Recipient.Id)
                                                                                                           .Contains(resource.Employee.Id)
                                                       || (resource.Employee.Department != null 
                                                           && PrivateWorkingTimeCalendars.As(c).Recipients.Select(r => r.Recipient.Id)
                                                                                                           .Contains(resource.Employee.Department.Id))
                                                       || PrivateWorkingTimeCalendars.As(c).Recipients.Any(r => 
                                                                                                           Groups.Is(r.Recipient) 
                                                                                                           && resource.Employee.IncludedIn(Groups.As(r.Recipient)))
                                                      )));
          
          calendars[resource.Id].AddRange(
          calendarsCached
          .Where(c => 
                 !PrivateWorkingTimeCalendars.Is(c)
                 && !calendars[resource.Id]
                              .Any(cc => cc.Year == c.Year)
                             ));
          
      }
      return calendars;
    }
    
    /// <summary>
    /// Получить календарь
    /// </summary>
    /// <param name="startDate">Дата начала.</param>
    /// <param name="endDate">Дата окончания.</param>
    /// <param name="resourceId">id ресурса</param>
    /// <returns>Календарь рабочего времени.</returns>
    public static List<IWorkingTimeCalendar> GetPrivateOrPublicCalendars(DateTime startDate, DateTime endDate, int resourceId)
    {
      var resource = ProjectsResources.Get(resourceId);
      Sungero.CoreEntities.Shared.CalendarCache.Restrict(resource.Employee, startDate.Year, endDate.Year);
      return Sungero.CoreEntities.Shared.CalendarCache.GetCached(true).ToList();
    }
    
    
    /// <summary>
    /// [Для демо - генератора] Добавление ресурса. 
    /// </summary>
    /// <param name="activityId">Идентификатор этапа. </param>
    /// <param name="resourceId">Идентификатор ресурса. </param>
    /// <param name="workload">Трудоемкость. </param>
    /// <param name="projectPlanId">Идентификатор плана. </param>
    [Public(WebApiRequestType = RequestType.Post), Remote]
    public void AddResourcesDemo(int activityId, int resourceId, int workload, int projectPlanId)
    {
      var activity = ProjectActivities.GetAll(a => a.Id == activityId).FirstOrDefault();
      var resource = ProjectsResources.GetAll(r => r.Id == resourceId).FirstOrDefault();
      var projectPlan = ProjectPlanRXes.GetAll(p => p.Id == projectPlanId).FirstOrDefault();
      
      if (resource == null || activity == null)
      {
        return;
      }
      
      var res1 = activity.ResourcesCapacity.AddNew();
      res1.Capacity = workload;
      res1.ResourceId = resource.Id;
      
      using (var connection = CreateDBConnection())
      {
        var act = new DirRX.Planner.Model.Activity()
        {
          Resources = new List<DirRX.Planner.Model.ResourcesWorkload>(),
          StartDate = activity.StartDate,
          EndDate = activity.EndDate,
          Id = activity.Id
        };
        act.Resources.Add(new DirRX.Planner.Model.ResourcesWorkload()
                          {
                            ResourceId = resource.Id,
                            Value = workload
                          });
        SaveResources(act.Resources, act.Id.Value, act.StartDate.Value, act.EndDate.Value, connection);
        activity.Save();
        connection.Close();
      }
      DirRX.ProjectPlanner.PublicFunctions.Module.Remote.WriteJsonBodyToProjectVersion(projectPlan, projectPlan.LastVersion.Number.Value, false);
    }
    
  }
}