﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using DirRX.TeamsCommonAPI.NotifyEventType;

namespace DirRX.ProjectPlanner.Server
{
  public class ModuleJobs
  {
    
    private RNDNoticesUtils.Structures.NoticeItem GetNoticeItem(DirRX.TeamsCommonAPI.ITeamsNoticesSettings settingItem, DateTime taskDeadline, object projectPlan = null, object projectActivity = null)
    {
      var setting = DirRX.TeamsCommonAPI.TeamsNoticesSettingses.As(settingItem);
      var attachments = new List<Sungero.Domain.Entity>();
      
      Sungero.Domain.Entity mainEntity = null;
      
      string firstTextParam = null;
      string secondaryTextParam = null;
      
      if (projectPlan != null)
      {
        attachments.Add((Sungero.Domain.Entity) projectPlan);
        mainEntity = (Sungero.Domain.Entity) projectPlan;
        
        firstTextParam = Hyperlinks.Get((Sungero.Domain.Entity) projectPlan);
      }
      
      if (projectActivity != null)
      {
        attachments.Add((Sungero.Domain.Entity) projectActivity);
        
        if (mainEntity == null)
        {
          mainEntity = (Sungero.Domain.Entity) projectActivity;
        }
        
        if (firstTextParam == null)
        {
          firstTextParam = Hyperlinks.Get((Sungero.Domain.Entity) projectActivity);
        }
        else
        {
          secondaryTextParam = Hyperlinks.Get((Sungero.Domain.Entity) projectActivity);
        }
      }
      
      var clientWebsite = DirRX.ProjectPlanner.PublicFunctions.Module.Remote.GetWebSite();
      
      var noticeItem = new RNDNoticesUtils.Structures.NoticeItem(setting, attachments, clientWebsite, mainEntity, firstTextParam, secondaryTextParam);
      
      noticeItem.TaskDeadLine = taskDeadline;
      noticeItem.SolutionGuid = "f2894e4d-a950-497f-a311-1fd7e9665d28";
      
      return noticeItem;
    }
    
    private IProjectPlanRXPlanDateNotices GetSettingByPlanAndEventType(IProjectPlanRX plan, Sungero.Core.Enumeration eventType)
    {
      var setting = plan?.PlanDateNotices?.Where(s => s.EventType.EventType.HasValue && s.EventType.EventType == eventType).FirstOrDefault();
      if (setting == null)
      {
        Logger.DebugFormat("NoticeTrigger. Не найдена соответствующая eventType {0} настройка для плана с id {1}", eventType.Value, plan.Id);
      }
      
      return setting;
    }
    
    public virtual void NoticeTriggerer()
    {
      this.NotifyAboutPlansWillStart();
      this.NotifyAboutPlansWillEnd();
      this.NotifyAboutOverdatedPlans();
      this.NoyifyAboutActivitiesWillStart();
      this.NotifyAboutActivitiesWillEnd();
      this.NotifyAboutOverdatedActivities();
    }
    
    private void NotifyAboutPlansWillStart()
    {
      var plansWillStart = ProjectPlanRXes.GetAll(p => p.StartDate.HasValue).ToList()
        .Where(p => (p.StartDate.Value - Calendar.Today) <= TimeSpan.FromDays(3) && (p.StartDate.Value - Calendar.Today >= TimeSpan.FromMinutes(1)));
      IProjectPlanRXPlanDateNotices setting;
      
      foreach (var plan in plansWillStart)
      {
        setting = GetSettingByPlanAndEventType(plan, EventType.PlanMustStarted);
        if (setting == null)
        {
          continue;
        }
        
        DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, plan.StartDate ?? Calendar.Today, plan));
      }
    }
    
    private void NotifyAboutPlansWillEnd()
    {
      var plansWillEnd = ProjectPlanRXes.GetAll(p => p.EndDate.HasValue).ToList()
        .Where(p => (p.EndDate.Value - Calendar.Today) <= TimeSpan.FromDays(3) && (p.EndDate.Value - Calendar.Today >= TimeSpan.FromMinutes(1)));
      IProjectPlanRXPlanDateNotices setting;
      
      foreach (var plan in plansWillEnd)
      {
        setting = GetSettingByPlanAndEventType(plan, EventType.PlanMustEnded);
        if (setting == null)
        {
          continue;
        }
        
        DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, plan.EndDate ?? Calendar.Today, plan));
      }
    }
    
    private void NotifyAboutOverdatedPlans()
    {
      var overdatedPlans = ProjectPlanRXes.GetAll(p => p.EndDate.HasValue && p.Status.HasValue).ToList()
        .Where(p => (p.EndDate.Value - Calendar.Today) <= TimeSpan.FromMinutes(1) && p.Status == DirRX.ProjectPlanner.ProjectPlanRX.Status.Active);
      IProjectPlanRXPlanDateNotices setting;
      
      foreach (var plan in overdatedPlans)
      {
        setting = GetSettingByPlanAndEventType(plan, EventType.PlanOverdated);
        if (setting == null)
        {
          continue;
        }
        
        DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, Calendar.Today, plan));
      }
    }
    
    private void NoyifyAboutActivitiesWillStart()
    {
      var activitiesWillStart = ProjectActivities.GetAll(p => p.StartDate.HasValue && p.TypeActivity.HasValue && p.ProjectPlan != null).ToList()
        .Where(p => (p.StartDate.Value - Calendar.Today) <= TimeSpan.FromDays(3) && (p.StartDate.Value - Calendar.Today >= TimeSpan.FromMinutes(1)));
      IProjectPlanRXPlanDateNotices setting;
      
      foreach (var activiy in activitiesWillStart)
      {
        if (activiy.TypeActivity == DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Task)
        {
          setting = GetSettingByPlanAndEventType(activiy.ProjectPlan, EventType.ActMustStarted);
          if (setting == null)
          {
            continue;
          }
          
          DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, activiy.StartDate ?? Calendar.Today, null, activiy));
          continue;
        }
        
        if (activiy.TypeActivity == DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Section)
        {
          setting = GetSettingByPlanAndEventType(activiy.ProjectPlan, EventType.SectMustStarted);
          if (setting == null)
          {
            continue;
          }
          
          DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, activiy.StartDate ?? Calendar.Today, null, activiy));
          continue;
        }
        
        setting = GetSettingByPlanAndEventType(activiy.ProjectPlan, EventType.MilestMustReach);
        if (setting == null)
        {
          continue;
        }
        
        DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, activiy.StartDate ?? Calendar.Today, null, activiy));
      }
    }
    
    private void NotifyAboutActivitiesWillEnd()
    {
      var activitiesWillEnd = ProjectActivities.GetAll(p => p.EndDate.HasValue && p.TypeActivity.HasValue && p.ProjectPlan != null).ToList()
        .Where(p => (p.EndDate.Value - Calendar.Today) <= TimeSpan.FromDays(3) && (p.EndDate.Value - Calendar.Today >= TimeSpan.FromMinutes(1)));
      IProjectPlanRXPlanDateNotices setting;
      
      foreach (var activity in activitiesWillEnd)
      {
        if (activity.TypeActivity == DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Task)
        {
          setting = GetSettingByPlanAndEventType(activity.ProjectPlan, EventType.ActMustEnded);
          if (setting == null)
          {
            continue;
          }
          
          DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, activity.EndDate ?? Calendar.Today, null, activity));
          continue;
        }
        
        if (activity.TypeActivity == DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Section)
        {
          setting = GetSettingByPlanAndEventType(activity.ProjectPlan, EventType.SectMustEnded);
          if (setting == null)
          {
            continue;
          }
          
          DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, activity.EndDate ?? Calendar.Today, null, activity));
          continue;
        }
      }
    }
    
    private void NotifyAboutOverdatedActivities()
    {
      var overdatedActivities = ProjectActivities.GetAll(p => p.EndDate.HasValue && p.Status.HasValue && p.TypeActivity.HasValue && p.ProjectPlan != null).ToList()
        .Where(p => (p.EndDate.Value - Calendar.Today) <= TimeSpan.FromDays(0) && p.Status == DirRX.ProjectPlanner.ProjectPlanRX.Status.Active);
      IProjectPlanRXPlanDateNotices setting;
      
      foreach (var activity in overdatedActivities)
      {
        if (activity.TypeActivity == DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Task)
        {
          setting = GetSettingByPlanAndEventType(activity.ProjectPlan, EventType.ActOverdated);
          if (setting == null)
          {
            continue;
          }
          
          DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, Calendar.Today, null, activity));
          continue;
        }
        
        if (activity.TypeActivity == DirRX.ProjectPlanner.ProjectActivity.TypeActivity.Section)
        {
          setting = GetSettingByPlanAndEventType(activity.ProjectPlan, EventType.SectOverdated);
          if (setting == null)
          {
            continue;
          }
          
          DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, Calendar.Today, null, activity));
        }
        
        setting = GetSettingByPlanAndEventType(activity.ProjectPlan, EventType.MilestOverdated);
        if (setting == null)
        {
          continue;
        }
        
        DirRX.TeamsCommonAPI.PublicFunctions.Module.RaiseNotify(GetNoticeItem(setting.LinkedSetting, Calendar.Today, null, activity));
      }
    }
    
    public virtual void SyncAccessRightsProjectPlanAndProject()
    {
      var needUpdateLastRunDate = true;
      var previousRun = GetLastSyncAccessRightsDate();
      var startDate = Calendar.Now;
      
      var projects = DirRX.ProjectPlanning.Projects.GetAll(x => x.ProjectPlanDirRX != null &&
                                                           x.Modified >= previousRun &&
                                                           x.Modified < startDate);
      
      
      foreach (var project in projects)
      {
        try
        {
          var projectPlan = project.ProjectPlanDirRX;
          if (projectPlan != null)
          {
            var oldRights = projectPlan.AccessRights.Current;
            var newRights = project.AccessRights.Current;

            
            foreach (var oldRight in oldRights)
            {
              if(!newRights.Contains(oldRight))
              {
                if (!Locks.GetLockInfo(projectPlan).IsLockedByOther)
                {
                  projectPlan.AccessRights.Revoke(oldRight.Recipient, oldRight.AccessRightsType);
                  projectPlan.AccessRights.Save();
                }
              }
            }
            
            foreach (var newRight in newRights)
            {
              if(!oldRights.Contains(newRight))
              {
                if (!Locks.GetLockInfo(projectPlan).IsLockedByOther)
                {
                  projectPlan.AccessRights.Grant(newRight.Recipient, newRight.AccessRightsType);
                  projectPlan.AccessRights.Save();
                }
              }
            }
          }
        }

        catch (Exception ex)
        {
          Logger.DebugFormat("Произошли ошибка в ФП SyncAccessRightsProjectPlanAndProject при обработке проекта {0} - {1}: {2}", project.Id, project.DisplayValue, ex.Message);
          needUpdateLastRunDate = false;
        }
        
      }
      
      if (needUpdateLastRunDate == true)
        UpdateLastSyncAccessRightsDate(startDate);
    }
    

    
    /// <summary>
    /// Получить дату последней рассылки уведомлений.
    /// </summary>
    /// <returns>Дата последней рассылки.</returns>
    public static DateTime GetLastSyncAccessRightsDate()
    {
      var key = "LastSyncAccessRightsOfProject";
      var command = string.Format(Queries.Module.SelectDocflowParamsValue, key);
      try
      {
        var executionResult = Sungero.Docflow.PublicFunctions.Module.ExecuteScalarSQLCommand(command);
        var date = string.Empty;
        if (!(executionResult is DBNull) && executionResult != null)
          date = executionResult.ToString();
        else
          return Calendar.Today;
        
        Logger.DebugFormat("Last sync date in DB is {0} (UTC)", date);
        
        DateTime result = Calendar.FromUtcTime(DateTime.Parse(date, null, System.Globalization.DateTimeStyles.AdjustToUniversal));
        return result;
      }
      catch (Exception ex)
      {
        Logger.Error("Error while getting last sync date", ex);
        return Calendar.Today;
      }
    }
    
    /// <summary>
    /// Обновить дату последней рассылки уведомлений.
    /// </summary>
    /// <param name="notificationDate">Дата рассылки уведомлений.</param>
    public static void UpdateLastSyncAccessRightsDate(DateTime notificationDate)
    {
      var key = "LastSyncAccessRightsOfProject";
      
      var newDate = notificationDate.Add(-Calendar.UtcOffset).ToString("yyyy-MM-ddTHH:mm:ss.ffff+0");
      Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommandFormat(Queries.Module.InsertOrUpdateDocflowParamsValue, new[] { key, newDate });
      Logger.DebugFormat("Last sync date is set to {0} (UTC)", newDate);
    }

  }
}