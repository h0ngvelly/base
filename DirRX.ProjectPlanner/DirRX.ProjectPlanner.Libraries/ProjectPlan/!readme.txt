В данной папке подключена библиотека mpxj - http://www.mpxj.org/ . Эта библиотека используется для чтения mpp файлов.
Что бы обеспечить работу на разных версия .NET (core и 4.5) приходится использовать hack. 

Суть hack-а.
Библиотека подключается два раза. Первый раз - это версия для .net 4.5, эти файлы лежат на том же уровне, что и данный readme файл.
Второй раз библиотека подключается в папке "core", там находится версия для .net core. Имя папки менять нельзя, это magic константа для sds.

При такой конфигурации в зависимости от того, где идет сборка и использование, берется соответствующая версия библиотеки. При обновлении надо обновлять обе версии.


Библиотеку брать официальную, в ней сборки в папке lib. Ещё необходимо удалить mpxj.dll и mpxj-vb.dll, оставив только mpxj-csharp.dll

Брал библиотеку отсюда - https://www.mpxj.org/howto-dotnet/, там ссылка на гитхаб, оттуда zip и в папке \mpxj\src.net\lib лежат для 4.5 и для core
